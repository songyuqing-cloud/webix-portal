
ad_library {
    APM callback procedures.

    @creation-date 2021-06-21
    @author malte.sussdorff@cognovis.de
}

namespace eval webix-portal::apm {}

ad_proc -public webix-portal::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.0.4.1 5.0.0.4.2 {
                # Grant permission to all freelancer on their own assignments
                # Grant permissions on project folders
                db_foreach assignment "select project_id, assignee_id, assignment_id, fa.freelance_package_id 
                    from im_freelance_assignments fa, im_freelance_packages fp where fp.freelance_package_id = fa.freelance_package_id" {
                    set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
                    if {$project_folder_id ne ""} {
                        permission::grant -party_id $assignee_id -object_id $project_folder_id -privilege "read"
                    }
                    permission::grant -party_id $assignee_id -object_id $freelance_package_id -privilege "read"
                    permission::grant -party_id $assignee_id -object_id $assignment_id -privilege "read"
                }
            }
        }
}
