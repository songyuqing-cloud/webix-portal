ad_library {
    Handling of notifications system for webix
    @author malte.sussdorff@cognovis.de
}

ad_proc webix_notification_type_assignment_overdue {} {} {return 4260}
ad_proc webix_notification_type_assignment_delivered {} {} {return 4261}
ad_proc webix_notification_type_assignment_accepted {} {} {return 4262}
ad_proc webix_notification_type_assignment_denied {} {} {return 4263}
ad_proc webix_notification_type_assignment_request_out {} {} {return 4264}
ad_proc webix_notification_type_project_overdue {} {} {return 4265}
ad_proc webix_notification_type_quote_accepted {} {} {return 4266}
ad_proc webix_notification_type_quote_denied {} {} {return 4267}
ad_proc webix_notification_type_project_delivered {} {} {return 4268}
ad_proc webix_notification_type_package_file_missing {} {} {return 4270}
ad_proc webix_notification_type_assignment_deadline_change {} {} {return 4271}
ad_proc webix_notification_type_default {} {} {return 4280}

ad_proc webix_notification_status_default {} {} {return 4240}
ad_proc webix_notification_status_warning {} {} {return 4241}
ad_proc webix_notification_status_important {} {} {return 4242}


ad_proc webix_notification_action_type_mark_read {} {} {return 4300}
ad_proc webix_notification_action_type_upload_files {} {} {return 4301}
ad_proc webix_notification_action_type_send_email {} {} {return 4302}
ad_proc webix_notification_action_type_open_object {} {} {return 4303}
ad_proc webix_notification_action_type_open_company {} {} {return 4304}
ad_proc webix_notification_action_type_open_contact {} {} {return 4305}
ad_proc webix_notification_action_type_freelancer_selection {} {} {return 4306}
ad_proc webix_notification_action_type_download_files {} {} {return 4307}
ad_proc webix_notification_action_type_cr_direct_download {} {} {return 4308}

namespace eval cog_rest::json_object {
    ad_proc -public webix_notification {} {
        @return notification object webix_notification The notification object, especially the ID is of note here
        @return recipient object person Who is receiving the notification
        @return notification_type category "Webix Notification Type" Type of notification we are looking at
        @return notification_status category "Webix Notification Status" Status of the notification we are looking at
        @return notification_color string Color of the notification. By default derived from the status
        @return viewed_date date-time When was the notification viewed (if at all)
        @return message string Text we are displaying for the notification. Default comes form the notification_type
        @return context json_object cognovis_object Object which caused the notification (context)
        @return actions json_array webix_notification_action Actions for  the notification
    } -

    ad_proc -public webix_notification_body {} {
        @param notification_type_id category "Webix Notification Type" Type of notification we are looking at
        @param notification_status_id category "Webix Notification Status" Status of the notification we are looking at
        @param message string Text we are displaying for the notification. Default comes form the notification_type
        @param context_id integer Object which caused the notification (context)
        @param recipient_id integer Recipient of the notification
    } -

    ad_proc -public webix_notification_action {} {
        @return action_type category "Webix Notification Action Type" Type of action we can perform
        @return action_text string Text we want to pass through to the action (e.g. the default text for an email)
        @return action_help string Tooltip text to describe the action based of the action_type
        @return action_icon string Class to use for the icon
        @return action_object json_object cognovis_object Object which the action shall be performed on
    } - 

    ad_proc -public webix_notification_action_body {} {
        @param action_type_id category "Webix Notification Action Type" Type of action we can perform
        @param action_object_id integer Object which the action shall be performed on
        @param action_text string Text we want to pass through to the action (e.g. the default text for an email)
        @param notification_id object webix_notification::write Notification for which we want to add/edit the action
    } - 

}

ad_proc -public cog_rest::get::webix_notification {
    -rest_user_id:required
    { -notification_id ""}
    { -recipient_id ""}
    { -context_id ""}
} {
    @param notification_id object webix_notification::read Notification which we want to get 
    @param recipient_id object person::read User for whom we want to read the notifications
    @param context_id integer Context for which we would like to get a list  of notifications

    @return notifications json_array webix_notification Array of notifications
} {
    set notifications [list]
    set where_clause_list [list]
    if {$notification_id ne ""} {
        lappend where_clause_list "notification_id = :notification_id"
    } 

    if {$recipient_id ne ""} {
        lappend where_clause_list "recipient_id = :recipient_id"
    } else {
        lappend where_clause_list "recipient_id = :rest_user_id"
    }

    if {$context_id ne ""} {
        lappend where_clause_list "n.context_id = :context_id"
    }

    db_foreach notifications "
        select notification_id, recipient_id, n.context_id, notification_status_id, notification_type_id, message, viewed_date
        from webix_notifications n
        where [join $where_clause_list " and "]
    " {        
        set actions [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::webix_notification_action -notification_id $notification_id -rest_user_id $rest_user_id]]
        set notification_color [db_string aux_html2 "select aux_html2 from im_categories where category_id = :notification_status_id" -default ""]
        set context [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $context_id]
		regsub -all {\n} $context {} context
        lappend notifications [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::webix_notification {
    -rest_user_id:required
    { -notification_type_id "" }
    { -notification_status_id ""}
    { -message "" }
    -context_id:required
    -recipient_id:required
} {
    @param webix_notification_body request_body Notification Information

    @return notification json_object webix_notification Object with the created notification
} {
    set notification ""
    set notification_id [webix::notification::create \
        -notification_status_id $notification_status_id \
        -notification_type_id $notification_type_id \
        -context_id $context_id \
        -recipient_id $recipient_id \
        -message $message \
        -creation_user_id $rest_user_id
    ]
    
    set notification [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::webix_notification -notification_id $notification_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::webix_notification {
    -rest_user_id:required
    -notification_id:required
    { -notification_type_id "" }
    { -notification_status_id ""}
    { -message "" }
    -context_id:required
    -recipient_id:required
} {
    @param webix_notification_body request_body Notification Information
    @param notification_id object webix_notification Notification we want to update
    @return notification json_object webix_notification Object with the created notification
} {
    set notification ""
    set notification_id [webix::notification::update \
        -notification_id $notification_id \
        -notification_status_id $notification_status_id \
        -notification_type_id $notification_type_id \
        -context_id $context_id \
        -recipient_id $recipient_id \
        -message $message \
        -creation_user_id $rest_user_id
    ]
    
    set notification [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::webix_notification -notification_id $notification_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::webix_notification {
    -rest_user_id:required
    -notification_id:required
} {
    Deletes a notification
    @param notification_id object webix_notification::write
    @return errors json_array error Array of errors found

} {
    set errors [list]

    if {[webix::notification::delete -notification_id $notification_id] eq "-1"} {
        set err_msg "Could not delete notifiction"
        set parameter "[im_name_from_id $notification_id]"
        set object_id $notification_id
        lappend errors [cog_rest::json_object]
    }
    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::webix_notification_read {
    -rest_user_id:required
    -notification_id:required
} {
    Marks a notification as read

    @param notification_id object webix_notification Notification we want to mark as read

    @return notification json_object webix_notification Object of notification

} {
    webix::notification::record_view -notification_id $notification_id
    set notification [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::webix_notification -notification_id $notification_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::webix_notification_action {
    -notification_id:required
    -rest_user_id:required
} {
    @param notification_id object webix_notification::read Notification which we want to get 
    @return actions json_array webix_notification_action Array of actions we can perform on that notification
} {
    set actions [list]
    db_foreach action "select action_type_id, action_object_id, action_text, action_help, recipient_id, aux_html2 as action_icon
        from webix_notification_actions na, webix_notifications n, im_categories c
        where n.notification_id = na.notification_id
        and c.category_id = na.action_type_id
        and n.notification_id = :notification_id" {
        
        set locale [lang::user::locale -user_id $recipient_id]
        if {$action_object_id ne ""} {
            set action_object [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $action_object_id]
        } else {
            set action_object ""
        }

        lappend actions [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

namespace eval webix::notification {
    ad_proc -public create {
        {-notification_status_id "4240"}
        {-notification_type_id "4280"}
        -context_id:required
        -recipient_id:required
        {-message ""}
        -creation_user_id:required
    } {
        Creates a new notification to be displayed with webix

        @param notification_status_id Status of the notification / severity - defaults to information 4240
        @param notification_type_id Type of the notification - defaults to 4280 (default)
        @param context_id Object for which we create the notification
        @param recipient_id Recipient who is receiving the notification
        @param message Message we want to send in the notification
        @param creation_user_id Who is creating this notification

        @return notification_id Id of the created notification
    } {
        if {$message eq "" && $notification_type_id eq ""} {
            return
        }
        
        if {$message eq ""} {
            set locale [lang::user::locale -user_id $recipient_id]
            set message [im_category_string1 -category_id $notification_type_id -locale $locale]
        }
        # Check if we already have an open notification for this object and recipient
        set notification_id [db_string notif_exists "select notification_id from webix_notifications
            where context_id = :context_id and recipient_id = :recipient_id and notification_type_id = :notification_type_id" -default ""]

        if {$notification_id ne ""} {
            # Check if we just need to update the viewed_date and potentially the status
            webix::notification::update -notification_id $notification_id \
                -notification_status_id $notification_status_id \
                -notification_type_id $notification_type_id \
                -context_id $context_id \
                -recipient_id $recipient_id \
                -message $message \
                -creation_user_id $creation_user_id 
        } else {

            # Create the notification
            set notification_id [db_string notification "select webix_notification__new(
                :creation_user_id,
                null,
                :notification_type_id,
                :notification_status_id,
                :context_id,
                :recipient_id,
                :message) from dual"
            ]
        }

        # Always provide the mark as read action
        webix::notification::action_create -notification_id $notification_id -action_type_id 4300

        # Permissions on the notification
        permission::grant -party_id $recipient_id -object_id $notification_id -privilege "write"

        return $notification_id
    }

    ad_proc -public update {
        -notification_id:required
        { -notification_status_id "" }
        { -notification_type_id "" }
        { -context_id "" }
        { -recipient_id "" }
        { -message ""}
        { -creation_user_id "" }
    } {
        Updates a notification with new data

        @param notification_id Notification we want to update
        @param notification_status_id Status of the notification / severity - defaults to information 4240
        @param notification_type_id Type of the notification - defaults to 4280 (default)
        @param context_id Object for which we create the notification
        @param recipient_id Recipient who is receiving the notification
        @param message Message we want to send in the notification
        @param creation_user_id Who is creating this notification

        @return notification_id Id of the created notification
    } {

        # Check if we already have an open notification for this object and recipient
        set notification_id [db_string notif_exists "select notification_id from webix_notifications
            where notification_id = :notification_id" -default ""]

        if {$notification_id eq ""} {
            return ""
        } else {

            # Update the notification

            set update_sql_list [list "viewed_date = null"]
            foreach key [list notification_status_id notification_type_id context_id recipient_id message] {
                set value [set $key]
                if {$value ne ""} {
                    lappend update_sql_list "$key = :$key"
                }
            }
            if {[llength update_sql_list]>0} {
                db_dml update_notification "update webix_notifications set [join $update_sql_list ", "] where notification_id = :notification_id"
            }

            if {$creation_user_id eq ""} {
                set creation_user_id [auth::get_user_id]
            }
            acs_object::update -object_id $notification_id -user_id $creation_user_id
        }

        # Permissions on the notification
        permission::grant -party_id $recipient_id -object_id $notification_id -privilege "write"

        return $notification_id
    }

    ad_proc -public delete {
        -notification_id:required
    } {
        Deletes a notification alongside it's actions

        @param notification_id Notification we want to delete
    } {
        return [db_string delete "select webix_notification__delete(:notification_id) from dual" -default "-1"]
    }

    ad_proc -public action_create {
        -notification_id:required
        -action_type_id:required
        {-action_object_id ""}
        {-action_text ""}
        {-action_help ""}
    } {
        Adds an action to a notification for the user to execute

        @param notification_id Notification which will have this action attached
        @param action_type_id What kind of action do we want to add.
        @param action_object_id On what object do we want to run the action
        @param action_text Text to pass through to the action (e.g. email text to send)
        @param action_help Help Text to display on mouseover of the action icon
    } {
        set action_exists_p [db_string action_exists "select 1 from webix_notification_actions where notification_id = :notification_id
            and action_type_id = :action_type_id" -default 0]
        
        if {$action_help eq ""} {
            set action_help [im_category_string1 -category_id $action_type_id]
        }
        if {!$action_exists_p} {
            db_dml insert_action "insert into webix_notification_actions 
                (notification_id,action_type_id,action_object_id,action_text, action_help)            
                values
                (:notification_id,:action_type_id,:action_object_id,:action_text, :action_help)"
        } else {
            webix::notification::action_update -notification_id $notification_id -action_type_id $action_type_id -action_object_id $action_object_id -action_text $action_text -action_help $action_help
        }
        return 1
    }


    ad_proc -public action_object {
        -rest_user_id:required
        -action_type_id:required
        {-action_object_id ""}
        {-action_text ""}
        {-action_help ""}
        {-action_icon ""}
        
    } {
        Returns a json_object for the action

        @param rest_user_id User who will see the action
        @param action_type_id What kind of action do we want to add.
        @param action_object_id On what object do we want to run the action
        @param action_text Text to pass through to the action (e.g. email text to send)
        @param action_help Help Text to display on mouseover of the action icon
    } {
        if {$action_help eq ""} {
            set locale [util_memoize [list lang::user::locale -user_id $rest_user_id] 3600]
            set action_help [util_memoize [list im_category_string1 -category_id $action_type_id -locale $locale] 3600]
        }

        if {$action_object_id ne ""} {
            set action_object [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $action_object_id]
        } else {
            set action_object ""
        }
        if {$action_icon eq ""} {
            set action_icon [util_memoize [list db_string action_icon "select aux_html2 as action_icon from im_categories where category_id = $action_type_id" -default ""] 3600]
        }
        return [cog_rest::json_object -object_class "webix_notification_action"]
    }

    ad_proc -public action_update {
        -notification_id:required
        -action_type_id:required
        {-action_object_id ""}
        {-action_text ""}
        {-action_help ""}

    } {
        Updates an action to a notification for the user to execute

        @param notification_id Notification which will have this action attached
        @param action_type_id What kind of action do we want to add.
        @param action_object_id On what object do we want to run the action
        @param action_text Text to pass through to the action (e.g. email text to send)
        @param action_help Help Text to display on mouseover of the action icon

    } {
        set action_exists_p [db_string action_exists "select 1 from webix_notification_actions where notification_id = :notification_id
            and action_type_id = :action_type_id" -default 0]
        
        if {$action_exists_p} {
            db_dml update_action "update webix_notification_actions 
                set action_object_id = :action_object_id,
                action_text = :action_text,
                action_help = :action_help
                where notification_id = :notification_id
                and action_type_id = :action_type_id"                
        } else {
            webix::notification::action_create -notification_id $notification_id -action_type_id $action_type_id -action_object_id $action_object_id -action_text $action_text -action_help $action_help
        }
        return 1
    }

    ad_proc -public action_delete {
        -notification_id:required
        { -action_type_id ""}
    } {
        Deletes an action from a notification

        @param notification_id notification we want to remove the action from
        @param action_type_id Type of action we want to remove - if empty remove all actions from the notification
    } {
        set where_clause_list [list "notification_id = :notification_id"]
        if {$action_type_id ne ""} {
            lappend where_clause_list "action_type_id = :action_type_id"
        }

        db_string delete "delete from webix_notification_actions where [join $where_clause_list " and "]"
    }

    ad_proc -public record_view {
        { -context_id ""}
        { -viewer_id ""}
        { -notification_id ""}
    } {
        Records the viewing of a notification
        @param context_id Object which has the notification
        @param viewer_id For whom do we want to record the viewing of the notification

        @return notification_id in case we found and updated it.
    } {
        if {$notification_id eq ""} {
            set notification_id [db_string notification_id "select notification_id from webix_notifications  where context_id = :context_id and recipient_id = :viewer_id"]
        }

        if {$notification_id ne ""} {
            db_dml viewed "update webix_notifications set viewed_date = now() where notification_id = :notification_id"
        }
        
        return $notification_id
    }

    ad_proc -public create_overdue_projects {} {
        Loop through all projects which are open and where the due date is in the past or upcoming
    } {
        # ---------------------------------------------------------------
        # Due soon Projects
        # ---------------------------------------------------------------
        set open_status_ids [im_sub_categories [im_project_status_open]]
        db_foreach projects_due_soon "select project_id, project_lead_id from im_projects
            where end_date > now()
            and end_date <= now() + interval '1 hour'
            and project_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {
            
            webix::notification::create \
                -context_id $project_id \
                -notification_type_id [webix_notification_type_project_overdue] \
                -notification_status_id [webix_notification_status_warning] \
                -recipient_id $project_lead_id \
                -creation_user_id $assignee_id

        }
        
        # ---------------------------------------------------------------
        # Overdue Projects
        # ---------------------------------------------------------------

        db_foreach projects_overdue "select project_id, project_lead_id from im_projects
            where end_date < now()
            and project_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {

            webix::notification::create \
                -context_id $project_id \
                -notification_type_id [webix_notification_type_project_overdue] \
                -notification_status_id [webix_notification_status_important] \
                -recipient_id $project_lead_id \
                -creation_user_id $project_lead_id
        
        }
    }

    ad_proc -public create_overdue_assignments {} {
        Loop through all assignments which are open and where the due date is in the past or upcoming
    } {
        # ---------------------------------------------------------------
        # Due soon Assignments
        # ---------------------------------------------------------------
        set open_status_ids [list 4222 4224 4229]
        set notification_type_id [webix_notification_type_assignment_overdue]

        db_foreach assignments_due_soon "select p.project_id,assignment_id, assignee_id, project_lead_id,
            from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
            where fa.freelance_package_id = fp.freelance_package_id
            and fa.end_date > now()
            and fa.end_date <= now() + interval '1 hour'
            and p.project_id = fp.project_id
            and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {

            # Check if we already have an open notification for this object and recipient
            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id" -default ""]
            if {$notification_id eq ""} {
                webix::notification::create \
                    -context_id $assignment_id \
                    -notification_type_id $notification_type_id \
                    -notification_status_id [webix_notification_status_default] \
                    -recipient_id $project_lead_id \
                    -creation_user_id $assignee_id
            }

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :assignee_id and notification_type_id = :notification_type_id" -default ""]
            if {$notification_id eq ""} {
                webix::notification::create \
                    -context_id $assignment_id \
                    -notification_type_id $notification_type_id \
                    -notification_status_id [webix_notification_status_warning] \
                    -recipient_id $assignee_id \
                    -creation_user_id $project_lead_id
            }
        }


        # ---------------------------------------------------------------
        # Overdue Assignments
        # ---------------------------------------------------------------

        db_foreach assignments_overdue "select p.project_id,assignment_id, assignee_id, project_lead_id,
            from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
            where fa.freelance_package_id = fp.freelance_package_id
            and p.project_id = fp.project_id
            and fa.end_date < now()
            and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id and notification_status_id = [webix_notification_status_warning]" -default ""]
            if {$notification_id eq ""} {
                webix::notification::create \
                    -context_id $assignment_id \
                    -notification_type_id [webix_notification_type_assignment_overdue] \
                    -notification_status_id [webix_notification_status_warning] \
                    -recipient_id $project_lead_id \
                    -creation_user_id $assignee_id
            }

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :assignee_id and notification_type_id = :notification_type_id and notification_status_id = [webix_notification_status_important]" -default ""]
            if {$notification_id eq ""} {
                webix::notification::create \
                    -context_id $assignment_id \
                    -notification_type_id [webix_notification_type_assignment_overdue] \
                    -notification_status_id [webix_notification_status_important] \
                    -recipient_id $assignee_id \
                    -creation_user_id $project_lead_id
            }
        }
    }

    ad_proc -public create_unanswered_requested_assignments {} {
        Loop though requested assignments and remind PMs if freelancers haven't reacted
    } {
        db_foreach requested_assignment "select fa.assignment_id, fp.freelance_package_id, p.project_lead_id, p.project_id, fa.assignee_id
            from im_freelance_assignments fa, acs_objects o, im_freelance_packages fp, im_projects p
            where assignment_status_id = [translation_assignment_status_requested]
            and last_modified < now() - interval '2 hours'
            and o.object_id = fa.assignment_id
            and fa.freelance_package_id = fp.freelance_package_id
            and p.project_id = fp.project_id
        " {
            set notification_type_id [webix_notification_type_assignment_request_out]

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id and notification_status_id = [webix_notification_status_warning]" -default ""]

            if {$notification_id eq ""} {
                webix::notification::create \
                    -context_id $assignment_id \
                    -notification_type_id [webix_notification_type_assignment_request_out] \
                    -notification_status_id [webix_notification_status_warning] \
                    -recipient_id $project_lead_id \
                    -message "[im_name_from_id $assignee_id] has not reacted yet to [im_name_from_id $freelance_package_id] in [im_name_from_id $project_id]." \
                    -creation_user_id $project_lead_id
            }
        }
    }
}
