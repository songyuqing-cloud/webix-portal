# check the various steps
namespace eval webix::test::packages {
    ad_proc processing_days {

    } {
        Test the processing_days
    } {
        
        set uom_id 330
        foreach package_type_id [list 4210 4211] {
            db_1row freelance_package "select fp.freelance_package_id, sum(task_units) as workload from im_freelance_packages_trans_tasks fptt, im_trans_tasks tt, im_freelance_packages fp where fp.freelance_package_id = fptt.freelance_package_id and fptt.trans_task_id = tt.task_id and task_uom_id = :uom_id and package_type_id = :package_type_id group by fp.freelance_package_id order by random() desc limit 1"
            switch $package_type_id {
                4211 {
                    set expected_days [format "%.0f" [expr $workload / 3]]
                }
                4210 {
                    set expected_days [format "%.0f" [expr $workload / 1]]
                }
            }

            set work_days [webix::packages::processing_days -freelance_package_id $freelance_package_id]
            if {$expected_days ne $work_days} {
                ns_log Error "$freelance_package_id $expected_days with $workload is not equal to $work_days"
                return 0
            }
        }
        set uom_id 324
        foreach package_type_id [list 4210 4211] {
            db_1row freelance_package "select fp.freelance_package_id, sum(task_units) as workload from im_freelance_packages_trans_tasks fptt, im_trans_tasks tt, im_freelance_packages fp where fp.freelance_package_id = fptt.freelance_package_id and fptt.trans_task_id = tt.task_id and task_uom_id = :uom_id and package_type_id = :package_type_id group by fp.freelance_package_id order by random() desc limit 1"

            set units_per_day [db_string package_type "select aux_int1 from im_freelance_packages fp, im_categories c where fp.freelance_package_id = :freelance_package_id and fp.package_type_id = c.category_id"]
            set expected_days [format "%.0f" [expr $workload / $units_per_day]]

            set work_days [webix::packages::processing_days -freelance_package_id $freelance_package_id]
            if {$expected_days ne $work_days} {
                ns_log Error "$freelance_package_id $expected_days with $workload for $units_per_day is not equal to $work_days"
                return 0
            }
        }
        return 1
    }
}
