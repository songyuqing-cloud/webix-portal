ad_library {

    Callbacks for webix-portal module
    
    @author malte.sussdorff@cognovis.de

}

ad_proc -public -callback webix_trans_task_after_create {
    {-project_id:required}
    {-trans_task_ids:required}
    { -user_id "" }
} {
    After creation of trans tasks in a project

    @param project_id Project in which the trans tasks were created
    @param trans_task_ids Tasks which were created
    @param user_id User which is responsible for the task creation
} - 

ad_proc -public -callback webix_trans_task_after_update {    
    {-trans_task_id:required}
} {
    After update of a trans task
    
    @param trans_task_id Task which was updated
} - 


ad_proc -public -callback webix::trans::project::after_create {
    {-project_id:required}
    { -user_id "" }
} {
    After creation of a translation project

    @param project_id Project which was just created
    @param user_id User which is responsible for the project_creation
} -

#---------------------------------------------------------------
# Trans project after update
#---------------------------------------------------------------

ad_proc -public -callback webix::trans::project::after_update {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After update (primarily status change) callback for translation projects
} - 

ad_proc -public -callback webix::trans::project::after_update -impl "aa_update_object" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After update (primarily status change) callback for translation projects
} {
    db_dml update_assignment_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :project_id"
}

ad_proc -public -callback webix::trans::project::after_update -impl "webix-potential-to-open" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to open from potential

    Will send out created assignments and setting them to requested
} {
    if {$old_project_status_id eq [im_project_status_potential] && $project_status_id eq [im_project_status_open]} {
        set created_assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
            where fp.project_id = :project_id
            and fp.freelance_package_id = fa.freelance_package_id
            and fa.assignment_status_id = 4220"]
        
        foreach assignment_id $created_assignment_ids {
            cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_status_id 4221
        }
    }
}

ad_proc -public -callback webix::trans::project::after_update -impl "webix-open-to-delivered" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to delivered from open

    Close all assignments where work was delivered or accepted
} {
    if {$old_project_status_id eq [im_project_status_open] && $project_status_id eq [im_project_status_delivered]} {
        set assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
            where fp.project_id = :project_id
            and fp.freelance_package_id = fa.freelance_package_id
            and fa.assignment_status_id in (4225,4226)"]
        
        foreach assignment_id $assignment_ids {
            cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_status_id 4231
        }
    }
}


ad_proc -public -callback webix_freelance_packages_after_create {
    {-trans_task_ids:required}
    {-freelance_package_id:required}
} {
    Callback that is executed after freelance packages are generated for trans_task_ids
} -

ad_proc -public -callback webix::assignment::default_filters {
    -project_id:required
    -user_id:required
} {
    Callback to add custom return values
} - 

ad_proc -public -callback webix::assignment::quality_rating_types {
    -assignment_ids:required
    -rest_user_id:required
} {
    Callback to define the quality_type_ids and quality_level_ids
} - 

ad_proc -public -callback webix::packages::after_deadline_calculation {
    -freelance_package_id:required
    -package_deadline:required
} {
    Callback to ammend the deadline after it was auto calculated
} -


#---------------------------------------------------------------
# trans_assignment_after_update
#---------------------------------------------------------------

ad_proc -public -callback webix::assignment::trans_assignment_after_update {
    -assignment_id:required
    -assignment_status_id:required
    -old_assignment_status_id:required
    -old_end_date:required
    -user_id:required
} {
    Callback primarily run to show a change of assignment status
} - 

ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl "aa_update_object" {
    -assignment_id:required
    -assignment_status_id:required
    -old_assignment_status_id:required
    -old_end_date:required
    -user_id:required
} {
    Update object last_modified and user
} {
    db_dml update_assignment_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :assignment_id"
}

ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl status_change {
    -assignment_id:required
    -assignment_status_id:required
    -old_assignment_status_id:required
    -old_end_date:required
    -user_id:required
} {
    After a status change
    * update_other_requests
    * assignment_remind_freelancer
    * remove overdue notifications
} {
    switch $assignment_status_id {
        4221 {
            # If the new status_id is requested, remind the freelancer to accept/deny the project
            if {$old_assignment_status_id ne 4221} {
                cog_rest::post::assignment_remind_freelancer -assignment_id $assignment_id -rest_user_id $user_id
            }
        }
        4222 {
            # If the new status_id is accepted, set all others to assign_others status if not already done so
            if {$old_assignment_status_id ne 4222} {
                # get all assignments with status "requested" for the package_id
                set to_update_status_ids [list 4220 4221] ; # created and requeste

                set to_update_assignment_ids [db_list to_update_assignmente "select assignment_id from im_freelance_assignments 
                    where freelance_package_id = (select freelance_package_id from im_freelance_assignments where assignment_id = :assignment_id)
                    and assignment_status_id in ([template::util::tcl_to_sql_list $to_update_status_ids])"]

                if {[llength $to_update_assignment_ids]>0} {
                    db_dml update_status "update im_freelance_assignments set assignment_status_id = 4228 where assignment_id in ([template::util::tcl_to_sql_list $to_update_assignment_ids])"
                }

                # Remove notifications for request outstanding
                lappend to_update_assignment_ids $assignment_id
                foreach notification_id [db_list notifications "select notification_id from webix_notifications 
                    where context_id in ([template::util::tcl_to_sql_list $to_update_assignment_ids]) 
                    and notification_type_id = [webix_notification_type_assignment_request_out]"] {
                    webix::notification::delete -notification_id $notification_id
                }
            }
        }
        4223 {
            foreach notification_id [db_list notifications "select notification_id from webix_notifications 
                where context_id = :assignment_id
                and notification_type_id in ([webix_notification_type_assignment_request_out], [webix_notification_type_assignment_accepted])"] {
                webix::notification::delete -notification_id $notification_id
            }
        }
        4225 {
            # Remove overdue notifications
            foreach notification_id [db_list notifications "select notification_id from webix_notifications where context_id = :assignment_id and notification_type_id in ([webix_notification_type_assignment_overdue],[webix_notification_type_assignment_accepted])"] {
                webix::notification::delete -notification_id $notification_id
            }
        }
    }
    db_dml update_assignment_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :assignment_id"
}

ad_proc -public -callback cog_rest::cr_file_after_upload -impl package_file_upload {
    -file_item_id
    -file_revision_id
    -context_id
    -context_type
    -user_id
} {
    Callback after a file was uploaded to a package or assignment

    @param file_item_id ItemId of the file we upload
    @param file_revision_id Revision of the file we uploaded
    @param context_id Context which was Provided
    @param context_type Object Type of the context
    @param user_id User who uploaded the package file
} {
    switch $context_type {
        im_freelance_assignment {
            # This must be a return file from the freelancer which is associated with the assignment.
            db_1row assignment_info "select assignment_status_id, assignee_id from im_freelance_assignments where assignment_id = :context_id"
            switch $assignment_status_id {
                4220 - 4221 {
                    # Missing confirmation - should not happend
                }
                4222 - 4224 - 4227 - 4229 {
                    # Work should have been delivered, so update to work delivered
                    permission::grant -party_id [im_profile_employees] -object_id $context_id -privilege "read"
                }
                default {
                    # Do nothing
                }
            }
        }
        im_freelance_package {
            # This must be a file which is uploaded from the PM for freelancers to work on.

            # Check if we have an assignment and set it to package available if it is still in accepted status
            set assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments where freelance_package_id = :context_id"]
            foreach assignment_id $assignment_ids {
                db_1row assignment_info "select assignment_status_id, assignee_id from im_freelance_assignments where assignment_id = :assignment_id"
                switch $assignment_status_id {
                    4220 - 4221 - 4222  {
                        # Created, Requested, Accepted - add permissions to assignee
                        permission::grant -party_id $assignee_id -object_id $context_id -privilege "read"
                    }
                    4223 - 4230 {
                        # Denied, Deleted - do nothing

                    }
                    4224 - 4227 {
                        # In progress - add permission and notify
                        permission::grant -party_id $assignee_id -object_id $context_id -privilege "read"
                    }
                    4225 - 4226 - 4231 {
                        # Work has been delivered already. Add permission and notify the PM
                        permission::grant -party_id $assignee_id -object_id $context_id -privilege "read"
                    }
                }
            }
        }
    }
}

