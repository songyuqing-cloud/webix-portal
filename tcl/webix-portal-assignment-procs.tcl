ad_library {
    Rest Procedures for posting the webix-portal package assignments section
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc assignment {} {
        @return assignment object im_freelance_assignment name and id for the assignment
        @return assignee object user Name and ID of the assignee
        @return freelance_package object im_freelance_package Package/Batch for which the assignment is used for.
        @return assignment_type category "Intranet Trans Task Type" Task Type (like edit / proof / ..) for the assignment
        @return assignment_type_sort_order integer Given the translation workflow, at which step does this assignment_type come in. Typically trans = 1, proof = 2
        @return assignment_status category "Intranet Freelance Assignment Status" Status of the assignment
        @return assignment_units number Number of units for this assignment
        @return rate number Price per unit for the assignment
        @return uom category "Intranet UoM" Unit of measure
        @return purchase_order object im_invoice Purchase Order for this assignment
        @return start_date date-time Time when the assignment will be ready to start working on. Defaults to now
        @return assignment_deadline date-time Deadline until which the assignment needs to be delivered
        @return assignment_comment string Comment / Notes for the assignment
        @return rating_comment string Comment provided with the rating
        @return rating_value number Average Rating for the assignment
        @return rating json_array assignment_rating Detailed Rating information
        @return package_files json_array cr_file Files provided by the PM for this assignment
        @return assignment_files json_array cr_file Files provided by the Freelancer for this assignment
    } -

    ad_proc assignment_body {} {
        @param rate number Rate which we are using for the assignment(s). If empty, get the best rate of the freelancer(s)
        @param assignment_units number Number of units for this package (assignment). If empty, use the units from the tasks in the package
        @param uom_id category "Intranet UoM" Unit of measure for the assignment. If empty, use the latest uom_id from the tasks (we should not have mixed uom_ids in packages)
        @param assignment_deadline date-time When is the assignment due.
        @param start_date date-time Time when the assignment will be ready to start working on. Defaults to now
        @param assignment_comment string Comment / Notes for the assignment
        @param assignment_status_id category "Intranet Freelance Assignment Status" Status of the assignment
    } - 

    ad_proc assignment_rating {} {
        @return quality_type category "Intranet Translation Quality Type" Type of rating
        @return quality_level category "Intranet Quality" Quality Level
        @return quality_value number Value for the quality level
    } -

    ad_proc assignment_rating_body {} {
        @param report_id object im_assignment_quality_report::write Report where we want to store the rating for
        @param quality_type_id category "Intranet Translation Quality Type" Type of rating
        @param quality_level_id category "Intranet Quality" Quality Level
    } - 

    ad_proc assignment_quality_report {} {
        @return report object im_assignment_quality_report Report we return
        @return assignment object im_freelance_assignment name and id for the assignment
        @return assignee object user Name and ID of the assignee being rated
        @return project object im_project Project information which we return
        @return project_nr string Project Nr of the project
        @return task_type category "Intranet Trans Task Type" Task type for which we are looking up the # of worked with. Corresponds to the workflow steps.
        @return rating_comment string Comment provided with the quality report
        @return subject_area category "Intranet Translation Subject Area" Subject Areas for this project 
        @return rating_value number Average Rating for the assignment of the assignee
        @return rating json_array assignment_rating Detailed Rating information        
    } - 

    ad_proc assignment_quality_report_body {} {
        @param rating_comment string Comment provided with the quality report
        @param subject_area_id category "Intranet Translation Subject Area" Subject Areas for this project 
    } - 

    ad_proc freelancer {} {
        @return freelancer object users::read Freelancer whom we can select
        @return freelancer_status_color string Color coding for the status icon
        @return freelancer_status_description string Tooltip to use for this freelancer's status (to explain why the color)
        @return sort_order number Order as configured based on status for the freelancer. Only a suggestion for the frontend, and you have to decide how to sort within the same sort_order
        @return rating number Quality rating for the freelancer
        @return fee json_array freelancer_fee Fee Information
        @return worked_with json_array freelancer_worked_with Detailed information about the freelancer engagement
    } - 

    ad_proc freelancer_worked_with {} {
        @return task_type category "Intranet Trans Task Type" Task type for which we are looking up the # of worked with. Corresponds to the workflow steps.
        @return total number How often did the freelancer execute the workflow step in general
        @return customer number How often did the freelancer work with the (end-)customer in this workflow step
    } -

    ad_proc freelancer_fee {} {
        @return freelancer object person Freelancer for whom to get the fees
        @return rate number Rate of the freelancer
        @return currency string Currency queried
        @return min_price number Minimum Price 
        @return source_language category "Intranet Translation Language" Which source langauge was matched
        @return target_language category "Intranet Translation Language" Which target language was matched
    } - 

    ad_proc freelance_package {} {
        @return freelance_package object im_freelance_package Package/Batch for which the assignment is used for.
        @return package_type category "Intranet Trans Task Type" Trans Task Type (workflow step) of this package
        @return has_assignments boolean Does this package have assignments (so you might want to look them up)
        @return units number Total number of units in this package
        @return uom category "Intranet UoM" Unit of measure. Hopefully a package was not created with different UOM ids...
        @return target_language category "Intranet Translation Language" Target language for which this package is used.
        @return package_deadline date-time End Date when the task needs to be delivered to the customer
        @return package_comment string Comment for the package. Different from the assignment comments     
        @return tasks json_array trans_task Array of tasks included in this package
    } - 

    ad_proc freelance_package_body {} {
        @param trans_task_ids object_array im_trans_task::read List of trans task_ids which we want to generate a package
        @param package_type_id category "Intranet Project Type" Task Type for which we want to generate the package. If empty, generate packages for all types of the project
        @param package_name string Name of the package, will auto generate if not provided
        @param package_comment string Comment for the package
    } -

    ad_proc trans_workflow_step {} {
        @return task_type category "Intranet Trans Task Type" The task type describing the workflow
        @return sort_order integer At which step does this workflow occur. 
    } -

    ad_proc trans_project_filter {} {
        @return source_language category_array "Intranet Translation Language" List of source_language_ids which are valid
        @return target_language category_array "Intranet Translation Language" List of target languages of this project (default filtered)
        @return valid_target_language category_array "Intranet Translation Language" List of valid target languages to choose from 
        @return subject_area category "Intranet Translation Subject Area" Subject Areas for this project 
        @return skill_business_sector category_array "Intranet Skill Business Sector" List of Business Sectors (in addition to subject_area) the freelancer has experience.
    } - 

    ad_proc freelancer_assignment {} {
        @return assignment object im_freelance_assignment name and id for the assignment
        @return assignee object user Name and ID of the assignee
        @return freelance_package object im_freelance_package Package/Batch for which the assignment is used for.
        @return assignment_type category "Intranet Trans Task Type" Task Type (like edit / proof / ..) for the assignment
        @return assignment_status category "Intranet Freelance Assignment Status" Status of the assignment
        @return assignment_units number Number of units for this assignment
        @return rate number Price per unit for the assignment
        @return uom category "Intranet UoM" Unit of measure
        @return purchase_order object im_invoice Purchase Order for this assignment
        @return provider_bill object im_invoice Provider Bill (credit note) for this assignment (usually present once the assignment is finished)
        @return start_date date-time Time when the assignment will be ready to start working on. Defaults to now
        @return assignment_deadline date-time Deadline until which the assignment needs to be delivered
        @return assignment_comment string Comment / Notes for the assignment
        @return package_comment string Comment on the package
        @return project object im_project Project information which we return
        @return project_nr string Project Nr of the project
        @return project_lead named_id Project lead for this project
        @return project_folder object cr_folder Project folder we hopefully have created.
        @return final_company named_id Final Customer for this project - defaults to company if no final customer is defined.
        @return package_files json_array cr_file Files provided by the PM for this assignment
        @return assignment_files json_array cr_file Files provided by the Freelancer for this assignment
        @return source_language category "Intranet Translation Language" Source language for this assignment
        @return target_language category "Intranet Translation Language" Target language of this assignment
        @return subject_area category "Intranet Translation Subject Area" Subject Area for this project 
        @return skill_business_sector category_array "Intranet Skill Business Sector" List of Business Sectors (in addition to subject_area) the freelancer has experience.
    } -
}

ad_proc translation_assignment_status_created {} {} {return 4220}
ad_proc translation_assignment_status_requested {} {} {return 4221}
ad_proc translation_assignment_status_accepted {} {} {return 4222}
ad_proc translation_assignment_status_denied {} {} {return 4223}
ad_proc translation_assignment_status_in_progress {} {} {return 4224}
ad_proc translation_assignment_status_delivered {} {} {return 4225}
ad_proc translation_assignment_status_delivery_accepted {} {} {return 4226}
ad_proc translation_assignment_status_delivery_rejected {} {} {return 4227}
ad_proc translation_assignment_status_assigned_other {} {} {return 4228}
ad_proc translation_assignment_status_package_requested {} {} {return 4229}
ad_proc translation_assignment_status_assignment_deleted {} {} {return 4230}
ad_proc translation_assignment_status_assignment_closed {} {} {return 4231}

#---------------------------------------------------------------
# Translation Assignments
#---------------------------------------------------------------

ad_proc -public cog_rest::get::translation_assignments {
    { -assignment_id "" }
    { -freelance_package_id "" }
    { -assignment_status_id "" }
    { -assignment_type_id "" }
    { -project_id "" }
    { -project_type_id ""}
    -rest_user_id:required
} {
    Returns an array of the current assignments of a project

    @param assignment_id object im_freelance_assignment::read Assignment we want to return (in case we need a single one)
    @param project_id object im_project::read Project for which we want to return the assignments
    @param freelance_package_id object im_freelance_package::read Freelance package we want to view
    @param assignment_status_id category "Intranet Freelance Assignment Status" Status of the assignment (useful for tests)
    @param assignment_type_id category "Intranet Trans Task Type" Type of assignment we look for
    @param project_type_id category "Intranet Project Type" Type of project, needed for testing.

    @return assignments json_array assignment Array of assignments
} {
    set assignments [list]
    set where_clauses [list]

    if {$freelance_package_id ne ""} {
        lappend where_clauses "f.freelance_package_id = :freelance_package_id"
    }

    if {$project_id ne ""} {
        lappend where_clauses "f.project_id = :project_id"
    }

    if {$project_type_id ne ""} {
        lappend where_clauses "f.project_id in (select p.project_id from im_projects p, im_freelance_packages fp where project_type_id = :project_type_id and p.project_id = fp.project_id)"
    }

    if {$assignment_status_id ne ""} {
        lappend where_clauses "a.assignment_status_id = :assignment_status_id"
    } else {
        lappend where_clauses "a.assignment_status_id != 4230"
    }

    if {$assignment_type_id ne ""} {
        lappend where_clauses "a.assignment_type_id = :assignment_type_id"
    }
    
    if {$assignment_id ne ""} {
        # We overwrite if we have a single assignment requested
        set where_clauses [list "a.assignment_id = :assignment_id"]
    }

    if {[llength $where_clauses] eq 0} {
        cog_rest::error -http_status 400 -message "We need either a project_id or freelance_package_id or assignment_id"
    } 

    set sql "select a.assignment_id, a.assignment_name,
        a.assignee_id, a.assignment_type_id, a.assignment_status_id,
        a.purchase_order_id, a.assignment_units, a.uom_id,
        a.rate, a.assignment_comment,
        to_char(a.end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as assignment_deadline,
        to_char(a.start_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as start_date,
        f.freelance_package_id, f.freelance_package_name,
        c.sort_order as assignment_type_sort_order
        from im_freelance_assignments a, im_freelance_packages f, im_categories c
        where f.freelance_package_id = a.freelance_package_id 
        and a.assignment_type_id = c.category_id
        and [join $where_clauses " and "]
        "
    
    db_foreach assignment $sql {
        set assignee_name [im_name_from_id $assignee_id]

        # Ratings
        if {[db_0or1row rating "select report_id, comment as rating_comment from im_assignment_quality_reports where assignment_id = :assignment_id"]} {
            set rating [list]
            db_foreach quality_value "select quality_type_id, quality_level_id, c.sort_order, c2.aux_int1 as quality_value
                from im_assignment_quality_report_ratings qrr, im_categories c, im_categories c2
                where qrr.quality_type_id = c.category_id
                and qrr.quality_level_id = c2.category_id
                and report_id = :report_id" {
                    lappend rating [cog_rest::json_object -object_class "assignment_rating"]
            }
            set rating_value [webix::trans::quality_rating -freelancer_id $assignee_id -assignment_id $assignment_id]
        } else {
            set rating ""
            set rating_comment ""
            set rating_value ""
        }

        set package_files [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::cr_file -parent_id $freelance_package_id -rest_user_id $rest_user_id]]
        set assignment_files [cog_rest::helper::json_array_to_object_list -json_array  [cog_rest::get::cr_file -parent_id $assignment_id -rest_user_id $rest_user_id]]
        
        lappend assignments [cog_rest::json_object]            
    }

    return [cog_rest::json_response]
    
}

ad_proc cog_rest::post::translation_assignments {
    -freelancer_ids:required
    -freelance_package_id:required
    -rest_user_id:required
    { -rate "" }
    { -assignment_units "" }
    { -uom_id "" }
    { -start_date ""}
    { -assignment_deadline "" }
    { -assignment_comment "" }
    { -assignment_status_id "4221" }

} {
    Generate one (or multiple) assignments for a *single* package.

    @param freelancer_ids object_array person Freelancer for whom to create an assignment
    @param freelance_package_id object im_freelance_package::read Package which we want to assign. Only one package at a time.
    @param assignment_body request_body Assignment information we want to created
    
    @return assignments json_array assignment Array of assignments
} {

    # Check sanity on the assignment_status_id
    switch $assignment_status_id {
        4222 {
            if {[llength $freelancer_ids]>1} {
                cog_rest::error -http_status 400 -message "You can't create more than one accepted assignment for a package_id"
                return
            }
        }
        4223 - 4228 - 4230 - 4231 {
            cog_rest::error -http_status 400 -message "Don't create an assignment if you already know it is denied or given to somebody else, deleted or closed."
            return
        }
        4224 - 4225 - 4226 - 4227 - 4229 {
            cog_rest::error -http_status 400 -message "Pick a status for a *new* assignment, not something in between states"
            return
        }
    }

    set assignment_type_id [db_string assignment_type_id "select package_type_id from im_freelance_packages where freelance_package_id =:freelance_package_id"]    
    set new_assignment_ids [list]        
    foreach freelancer_id $freelancer_ids {
        set existing_assignment_id [db_string assignment_exist "select assignment_id from im_freelance_assignments where freelance_package_id =:freelance_package_id and assignee_id =:freelancer_id limit 1" -default 0]
        if {$existing_assignment_id } {
            cog_rest::error -http_status 400 -message "We already have an assignment ($existing_assignment_id) for this user for this package, can't create a second one."
            return
        }
        
        if {$rate ne ""} {
            set assignment_id [webix::assignments::create -freelance_package_id $freelance_package_id -assignee_id $freelancer_id -rate $rate -assignment_units $assignment_units -uom_id $uom_id -end_date $assignment_deadline -user_id $rest_user_id -assignment_status_id $assignment_status_id -ignore_min_price_p 1 -assignment_comment $assignment_comment]
        } else {
            set assignment_id [webix::assignments::create -freelance_package_id $freelance_package_id -assignee_id $freelancer_id -assignment_units $assignment_units -uom_id $uom_id -end_date $assignment_deadline -user_id $rest_user_id -assignment_status_id $assignment_status_id -assignment_comment $assignment_comment]
        }

        lappend new_assignment_ids $assignment_id
    }

    set assignments [list]

    foreach assignment_id $new_assignment_ids {
        lappend assignments [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::translation_assignments -rest_user_id $rest_user_id -assignment_id $assignment_id]]
    }

    return [cog_rest::json_response]
}

ad_proc cog_rest::put::translation_assignments {
    -assignment_id:required
    -rest_user_id:required
    -rate
    -assignment_units
    -uom_id
    -start_date
    -assignment_deadline
    -assignment_comment
    -assignment_status_id

} {
    Update an assignment

    @param assignment_id object im_freelance_assignment::write Assignment which we want to update
    @param assignment_body request_body detailed information of the assignment we want to update

    @return assignments json_object assignment The newly updated assignment
} {

    # Check if we have a purchase order. In that case we can't really change much.
    db_1row assignment "select assignment_status_id as old_assignment_status_id, purchase_order_id, end_date, freelance_package_id, assignment_type_id from im_freelance_assignments where assignment_id = :assignment_id"

    # Don't allow the change if the end_date in some cases
    switch $old_assignment_status_id {
        4220 - 4221 {
            switch $assignment_status_id {
                4221 {
                    # Setting to requested, this is possible . do nothing
                }
                4222 {
                    # Accepted an assignment

                    # Check that the assignment is still available
                    # And no other user has been assigned for tasks in this assignment
                    db_1row assignment_info "select freelance_package_id, assignment_type_id, rate, uom_id,end_date as deadline from im_freelance_assignments where assignment_id = :assignment_id"

                    set existing_assignee_id [db_string assigned_p "select assignee_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
                        and assignment_type_id = :assignment_type_id
                        and assignment_status_id in (4222,4224,4225,4226) limit 1" -default 0]

                    if {$existing_assignee_id >0} {
                        # Check if he is the assigned
                        if {$assignee_id ne $existing_assignee_id} {
                            cog_rest::error -http_status 400 -message "We are sorry to inform you, but the assignment is no longer available."
                            set assignment_status_id 4228
                        }
                    }

                    if {$assignment_status_id eq 4222} {
                        db_dml update_other "update im_freelance_assignments set assignment_status_id = 4228 where assignment_id in (select assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
                            and assignment_type_id = :assignment_type_id
                            and assignment_status_id in (4220,4221))"                    
                    }
                }
                4223 - 4230 - 4231 { 
                    # Denied the assignment
                }
                default {
                    # This is not allowed, keep the old status
                    set assignment_status_id $old_assignment_status_id
                }
            }
        }
        4222 - 4224 {
            switch $assignment_status_id {
                4223 {
                    # Do not allow denying an already accepted assignment
                    cog_rest::error -http_status 400 -message "You can't deny an already accepted assignment"
                    set assignment_status_id $old_assignment_status_id
                }
            }
        }
        4225 - 4226 - 4231 {
            set assignment_deadline $end_date
            switch $assignment_status_id {
                4223 {
                    # Do not allow denying an already accepted assignment
                    cog_rest::error -http_status 400 -message "You can't deny an already accepted assignment"
                    set assignment_status_id $old_assignment_status_id
                }
            }
        }
        default {
            # Do nothing
        }
    }


    set sql "update im_freelance_assignments set"

    foreach optional_var [list start_date assignment_comment assignment_status_id] {
        if {[info exists $optional_var]} {
            append sql " $optional_var=:$optional_var,\n"
        }
    }

    if {[info exists assignment_deadline]} {
        append sql " end_date = :assignment_deadline,"
    }

    if {$purchase_order_id eq ""} {

        foreach financial_var [list uom_id rate assignment_units] {
            if {[info exists $financial_var]} {
                append sql " $financial_var=:$financial_var,\n"
            }
        }
    }

    append sql " assignment_id = :assignment_id where assignment_id = :assignment_id"
    db_dml update_assignment $sql

    callback webix::assignment::trans_assignment_after_update -assignment_id $assignment_id -assignment_status_id $assignment_status_id -old_assignment_status_id $old_assignment_status_id -old_end_date $end_date -user_id $rest_user_id

    set assignments [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::translation_assignments -assignment_id $assignment_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::delete::translation_assignments {
    -assignment_ids:required
    { -purge_p 0 }
    -rest_user_id:required
} {
    Set an assignment to deleted

    @param assignment_ids object_array im_freelance_assignment::write Assignment which we want to delete
    @param purge_p boolean Are we trying to purge the assignment. Successful if the assignment does not have a purchase order

    
    @return errors json_array error Array of errors found
} {
    set errors [list]

    foreach assignment_id $assignment_ids {
        # Check if we have a purchase order. In that case we can't really change much.
        db_1row assignment "select assignment_status_id as old_assignment_status_id, purchase_order_id, end_date from im_freelance_assignments where assignment_id = :assignment_id"

        if {$purchase_order_id eq ""} {
            if {$purge_p} {
                db_exec_plsql delete_package {
                    select im_freelance_assignment__delete(:assignment_id);
                }
            } else {
                set assignment_status_id 4230 ; #deleted
                db_dml set_to_deleted "update im_freelance_assignments set assignment_status_id = :assignment_status_id where assignment_id = :assignment_id"
                callback webix::assignment::trans_assignment_after_update -assignment_id $assignment_id -assignment_status_id $assignment_status_id -old_assignment_status_id $old_assignment_status_id -old_end_date $end_date -user_id $rest_user_id
            }
        } else {
            set err_msg "Purchase order exists, can't delete the assignment $assignment_id"
            set parameter "[im_name_from_id $assignment_id]"
            set object_id $assignment_id
            lappend errors [cog_rest::json_object]
        }
    }
    return [cog_rest::json_response]
}


#---------------------------------------------------------------
# Translation packages
#---------------------------------------------------------------

ad_proc -public cog_rest::get::packages {
    { -project_id ""} 
    -rest_user_id:required
    { -trans_task_id "" }
    { -package_type_id "" }
    { -freelancer_ids "" }
    { -freelance_package_ids "" }
} {
    Returns a list of packages along with their tasks.

    Appends unassigned tasks with an empty freelance_package object..

    @param project_id object im_project::read Project for which we want to return the assignments
    @param package_type_id category "Intranet Trans Task Type" Trans Task Type for which we want to view the packages
    @param trans_task_id object im_trans_task::read Task which we want to get the packages for.
    @param freelancer_ids object_array person::read Freelancer whom we want to get possible packages for.
    @param freelance_package_ids object_array im_freelance_package::read Packages we want to return (by ID)

    @return packages json_array freelance_package Array of the packages

} {
    set packages [list]

    set where_clauses [list]
    set from_clauses [list "im_freelance_packages f"]

    if {$project_id ne ""} {
        lappend where_clauses "f.project_id = :project_id"
    }

    if {$package_type_id ne ""} {
        lappend where_clauses "f.package_type_id = :package_type_id"
    }

    if {$trans_task_id ne ""} {
        lappend where_clauses "fptt.trans_task_id = :trans_task_id"
        
        lappend from_clauses "im_freelance_packages_trans_tasks fptt"
        lappend where_clauses "fptt.freelance_package_id = f.freelance_package_id"
    }

    if {$freelancer_ids ne ""} {
        if {[lsearch $from_clauses "im_freelance_packages_trans_tasks fptt"]<0} {
            lappend from_clauses "im_freelance_packages_trans_tasks fptt"
            lappend where_clauses "fptt.freelance_package_id = f.freelance_package_id"
        }

        # Get target language skills for the freelancer
        set target_language_ids [webix::freelancer::project_target_languages -freelancer_ids $freelancer_ids -project_id $project_id]

        lappend from_clauses "im_trans_tasks tt"
        lappend where_clauses "tt.task_id = fptt.trans_task_id"
        lappend where_clauses "tt.target_language_id in ([template::util::tcl_to_sql_list $target_language_ids])"
    }

    if {$freelance_package_ids ne ""} {
        lappend where_clauses "f.freelance_package_id in ([template::util::tcl_to_sql_list $freelance_package_ids])"
    }

    if {[llength $where_clauses] eq 0} {
        cog_rest::error -http_status 400 -message "You need to provide at least freelance_package_ids, trans_task_id or project_id"
    }
    db_foreach package_tasks "
        select distinct f.freelance_package_id, f.freelance_package_name, f.package_type_id, f.package_comment
        from [join $from_clauses " , "]
        where [join $where_clauses " and "]
    " {
        set tasks [list]
        set has_assignments [db_string assignments_p "select 1 from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id not in (4230,4223,4227,4228) limit 1" -default 0]

        set total_tasks_in_package [db_string count_package_tasks "select count(*) from im_freelance_packages_trans_tasks where freelance_package_id =:freelance_package_id" -default 0]
        if {$total_tasks_in_package > 0} {
            db_foreach package_tasks {
                select tt.task_id as task_id, tt.task_name as task_name, task_units, task_uom_id, target_language_id,
                    to_char(tt.end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as task_deadline, billable_units,task_type_id
                from im_freelance_packages_trans_tasks ftt, im_trans_tasks tt
                where ftt.freelance_package_id = :freelance_package_id
                and ftt.trans_task_id = tt.task_id
            } {
                lappend tasks [cog_rest::json_object -object_class "trans_task"] 
            }
        }

        if {$has_assignments} {
            set package_deadline [db_string min_deadline "select to_char(min(end_date), 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') from im_freelance_assignments where freelance_package_id = :freelance_package_id" -default ""]
        } else {
            # Need to calculate the package deadline
            set package_deadline [webix::packages::calculate_deadline -freelance_package_id $freelance_package_id]
        }

        set uom_id [webix::packages::uom_id -freelance_package_id $freelance_package_id]
        set units [webix::packages::trans_task_units -freelance_package_id $freelance_package_id]

        lappend packages [cog_rest::json_object]
    }

    return [cog_rest::json_response]        
}

ad_proc -public cog_rest::post::packages {
    { -project_id ""}
    { -trans_task_ids "" }
    { -package_type_id "" }
    { -package_name "" }
    -rest_user_id:required
    { -overwrite_p "0" }
} {
    Returns a list of newly packages along with their tasks.

    Appends unassigned tasks with an empty freelance_package object..

    @param project_id object im_project::read Project for which we want to generate packages (all yet unassigned tasks into one package)
    @param overwrite_p boolean if set, we will overwrite existing package assignments (if possible). Otherwise we only generate a package for the not already assigned tasks.
    @param freelance_package_body request_body Package Info for the creation of the pacakge

    @return packages json_array freelance_package Array of the packages
} {

    set packages [list]

    if {$trans_task_ids eq ""} {
        if {$project_id eq ""} {
            cog_rest::error -http_status 400 -message "You need to provide at least tasks or a project"
        } else {
            set trans_task_ids [db_list trans_tasks "select distinct task_id from im_trans_tasks tt, im_freelance_packages fp 
                where fp.project_id = :project_id 
                and fp.project_id = tt.project_id
                and tt.task_id not in (select trans_task_id from im_freelance_packages_trans_tasks fptt where fptt.freelance_package_id = fp.freelance_package_id)"]
        }
    }

    if {$overwrite_p} {
        set new_packages_ids [webix::packages::create -trans_task_ids $trans_task_ids -package_type_id $package_type_id -package_name $package_name -user_id $rest_user_id -overwrite] 
    } else {
        set new_packages_ids [webix::packages::create -trans_task_ids $trans_task_ids -package_type_id $package_type_id -package_name $package_name -user_id $rest_user_id] 
    }
    
    if {[llength $new_packages_ids] > 0} {
        set packages [cog_rest::get::packages -freelance_package_ids $new_packages_ids -rest_user_id $rest_user_id]
    }
    
    return [cog_rest::json_response]

}

ad_proc -public cog_rest::put::packages {
    -freelance_package_id:required
    { -trans_task_ids "" }
    { -package_comment "" }
    { -package_name "" }
    -rest_user_id:required
} {
    Returns a list of newly packages along with their tasks.

    Appends unassigned tasks with an empty freelance_package object..

    @param freelance_package_id object im_freelance_package::write Package we want to update
    @param freelance_package_body request_body Package Info for the creation of the pacakge

    @return packages json_object freelance_package Package we just updated

} {
    set has_assignments [db_string assignments "select 1 from im_freelance_assignments 
        where freelance_package_id = :freelance_package_id
        and assignment_status_id not in (4230,4223,4227,4228) limit 1" -default 0]

    if {$has_assignments eq 0 } {
        # Remove all tasks from that package            
        db_dml delete_mapping "delete from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"        
    }
    
    if {$trans_task_ids ne "" && $has_assignments eq 0 } {
        # try to update the tasks

        # First identify the tasks which are in other packages which we are not allowed to assign to this package
        db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"
        set exclude_package_ids [db_list project_freelance_packages "select freelance_package_id from im_freelance_packages 
            where project_id = :project_id
            and package_type_id = :package_type_id
            and freelance_package_id <> :freelance_package_id"]              
    
        set assignable_task_sql "select task_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $trans_task_ids])"
        if {$exclude_package_ids ne ""} {
            append assignable_task_sql " and task_id not in (
                    select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id in ([template::util::tcl_to_sql_list $exclude_package_ids]))"
        }
        set assignable_task_ids [db_list assignable_task_ids $assignable_task_sql]

        # Run foreach loop to 
        foreach trans_task_id $assignable_task_ids {
            db_dml delete_mapping "delete from im_freelance_packages_trans_tasks 
                where trans_task_id = :trans_task_id 
                and freelance_package_id in (
                    select freelance_package_id from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id
                )"
            db_dml add_mapping "insert into im_freelance_packages_trans_tasks (freelance_package_id,trans_task_id) values (:freelance_package_id, :trans_task_id)"
        }        
    }

    if {$package_name ne ""} {
        db_dml update_name "update im_freelance_packages set freelance_package_name = :package_name where freelance_package_id = :freelance_package_id"
    }
    
    if {$package_comment ne ""} {
        db_dml update_name "update im_freelance_packages set package_comment = :package_comment where freelance_package_id = :freelance_package_id"
    }

    set packages [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::packages -rest_user_id $rest_user_id -freelance_package_ids $freelance_package_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::packages {
    -package_ids:required
} {
    Delete the provided package ids

    Will remove as many as it can. For others we have the errors

    @param package_ids object_array im_freelance_package::write Package we are trying to delete

    @return errors json_array error Array of errors found
} {
    set errors [list]

    # Check for assignments on the package_ids. Generate errors for those
    set packages_with_assignments [db_list packages_with_assignments "
        select distinct freelance_package_id from im_freelance_assignments 
        where freelance_package_id in ([template::util::tcl_to_sql_list $package_ids])"]

    foreach package_id $packages_with_assignments {
        set err_msg "We have at least one existing assignment for the package. Please delete those first"
        set parameter "[im_name_from_id $package_id] - $package_id"
        set object_id $package_id 
        ns_log Warning "$package_id still has assignments, won't delete with cog_rest::delete::packages"
        lappend errors [cog_rest::json_object]
    }

    foreach package_id $package_ids {
        if {[lsearch $packages_with_assignments $package_id]<0} {
            db_exec_plsql delete_package {
                select im_freelance_package__delete(:package_id);
            }
        }
    }

    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Freelancer Ratings
#---------------------------------------------------------------

ad_proc -public cog_rest::get::assignment_quality_rating_types {
    -assignment_ids:required
    -rest_user_id:required
} {
    Provide the quality report rating types which can be used for rating an assignment
    
    @param assignment_ids object_array im_freelance_assignment::* Assignments we would like to rate

    @return report_options json_object Object with the two arrays
    @return_report_options quality_type category_array "Intranet Translation Quality Type" Type of rating
    @return_report_options quality_level category_array "Intranet Quality" Quality Level

} {
    # Initial setup, assume we have all valid ones
    
    set quality_type_ids [db_list quality_types "select category_id from im_categories where category_type = 'Intranet Translation Quality Type'"]
    set quality_level_ids [db_list quality_types "select category_id from im_categories where category_type = 'Intranet Quality'"]

    callback webix::assignment::quality_rating_types -assignment_ids $assignment_ids -rest_user_id $rest_user_id

    set report_options [cog_rest::json_object]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::assignment_quality_report {
    { -assignment_id "" }
    { -assignee_id "" }
    -rest_user_id:required
} {
    Returns the rating information

    @param assignee_id object person::* Assignee for whom we want to get assignments
    @param assignment_id object_array im_freelance_assignment::* Assignment we would like get the report for

    @return report json_array assignment_quality_report

} {
    set report [list]

    if {$assignee_id eq "" && $assignment_id eq ""} {
        cog_rest::error -http_status 400 - message "You need to provide either an assignment or assignee"
    }

    if {$assignee_id eq ""} {
        set assignee_id [db_string assignee "select assignee_id from im_freelance_assignments where assignment_id = [lindex $assignment_id 0]"]
    }

    if {$assignment_id eq ""} {
        set assignment_ids [db_list assignment_ids "select fa.assignment_id
            from im_freelance_assignments fa, im_assignment_quality_reports qr
            where qr.assignment_id = fa.assignment_id
            and fa.assignee_id = :assignee_id"]
    } else {
        set assignment_ids $assignment_id
    }

    if {$assignment_ids ne ""} {
        db_foreach rating "select report_id, comment as rating_comment, subject_area_id, assignment_id from im_assignment_quality_reports 
            where assignment_id in ([template::util::tcl_to_sql_list $assignment_ids])" {
            set rating [list]
            db_foreach quality_value "select quality_type_id, quality_level_id, c.sort_order, c2.aux_int1 as quality_value
                from im_assignment_quality_report_ratings qrr, im_categories c, im_categories c2
                where qrr.quality_type_id = c.category_id
                and qrr.quality_level_id = c2.category_id
                and report_id = :report_id" {
                    lappend rating [cog_rest::json_object -object_class "assignment_rating"]
            }
            set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
            db_0or1row project_info "select project_nr, project_name from im_projects where project_id = :project_id"
            set task_type_id [db_string get_task_type_id "select assignment_type_id from  im_freelance_assignments where assignment_id = :assignment_id"]
            set task_type_name [im_category_from_id $task_type_id ]
            set rating_value [webix::trans::quality_rating -freelancer_id $assignee_id -assignment_id $assignment_id]
            lappend report [cog_rest::json_object]
        }
    }
    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::assignment_quality_report {
    -assignment_id:required
    {-rating_comment ""}
    {-subject_area_id ""}
    -rest_user_id:required
} {
    Create a new assignment_quality_report

    @param assignment_id object im_freelance_assignment Assignment where we want to add the report to
    @param assignment_quality_report_body request_body Report information
    @return report json_object assignment_quality_report
} {

    set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]

    if {$subject_area_id eq ""} {
        set subject_area_id [db_string subject_area_id "select subject_area_id from im_projects where project_id = :project_id"]
    }

    set report_id [db_string new_report_id "select im_assignment_quality_report__new(
        :project_id, :assignment_id, :subject_area_id, :rating_comment, :rest_user_id, null)" -default ""]

    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::assignment_quality_report -rest_user_id $rest_user_id -assignment_id $assignment_id]]
    
    permission::grant -party_id $rest_user_id -object_id $report_id -privilege "write"

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::assignment_quality_report {
    -report_id:required
    {-rating_comment ""}
    {-subject_area_id ""}
    {-assignment_id ""}
    -rest_user_id:required
} {
    Update a quality report
    
    @param report_id object im_assignment_quality_report Report we want to update
    @param assignment_id object im_freelance_assignment Assignment where we want to add the report to

    @param assignment_quality_report_body request_body Report information
    @return report json_object assignment_quality_report
} {

    set update_sql [list "comment = :rating_comment"]
    if {$assignment_id ne ""} {
        lappend update_sql "assignment_id = :assignment_id"
    } else {
        set assignment_id [db_string assignment "select assignment_id from im_assignment_quality_reports where report_id = :report_id"]
    }

    if {$subject_area_id ne ""} {
        lappend update_sql "subject_area_id = :subject_area_id"
    }
    db_dml update_report "update im_assignment_quality_reports set [join $update_sql ", "] where report_id = :report_id"

    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::assignment_quality_report -rest_user_id $rest_user_id -assignment_id $assignment_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::assignment_quality_report {
    -report_id:required
    -rest_user_id:required
} {
    Delete a quality report
    
    @param report_id object im_assignment_quality_report Report we want to update
    @return report json_object assignment_quality_report Any remaining report for this assignment, probably empty
} {
    set assignment_id [db_string assignment "select assignment_id from im_assignment_quality_reports where report_id = :report_id" -default ""]
    if {$assignment_id ne ""} {
        db_dml delete_report_ratings "delete from im_assignment_quality_report_ratings where report_id = :report_id"
        db_dml delete_report "delete from im_assignment_quality_reports where report_id = :report_id"
    }
    return "null"
}


ad_proc -public cog_rest::post::assignment_quality_rating {
    -report_id:required
    -quality_type_id:required
    -quality_level_id:required
    -rest_user_id:required
} {
    Create a new rating within a report

    @param assignment_rating_body request_body Invididual Rating
    @return report json_object assignment_quality_report
} {
    set assignment_id [db_string assignment "select assignment_id from im_assignment_quality_reports where report_id = :report_id"]

    set rating_exists_p [db_string rating_p "select 1 from im_assignment_quality_report_ratings where report_id = :report_id and quality_type_id = :quality_type_id" -default 0]
    if {$rating_exists_p} {
        db_dml update_rating "update im_assignment_quality_report_ratings set quality_level_id = :quality_level_id where report_id = :report_id and quality_type_id = :quality_type_id"
    } else {
        db_dml insert_rating "insert into im_assignment_quality_report_ratings (report_id, quality_type_id, quality_level_id) values (:report_id, :quality_type_id, :quality_level_id)"
    }

    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::assignment_quality_report -rest_user_id $rest_user_id -assignment_id $assignment_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::assignment_quality_rating {
    -report_id:required
    -quality_type_id:required
    -quality_level_id:required
    -rest_user_id:required
} {
    Update a new rating within a report

    @param assignment_rating_body request_body Invididual Rating
    @return report json_object assignment_quality_report
} {
    set assignment_id [db_string assignment "select assignment_id from im_assignment_quality_reports where report_id = :report_id"]
    db_dml update_rating "update im_assignment_quality_report_ratings set quality_level_id = :quality_level_id where report_id = :report_id and quality_type_id = :quality_type_id"
    
    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::assignment_quality_report -rest_user_id $rest_user_id -assignment_id $assignment_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::assignment_quality_rating {
    -report_id:required
    -quality_type_id:required
    -rest_user_id:required
} {
    Update a new rating within a report

    @param report_id object im_assignment_quality_report Report from which we want to delete the rating
    @param quality_type_id category "Intranet Translation Quality Type" Type of rating

    @return report json_object assignment_quality_report
} {
    set assignment_id [db_string assignment "select assignment_id from im_assignment_quality_reports where report_id = :report_id"]
    db_dml delete_rating "delete from  im_assignment_quality_report_ratings where report_id = :report_id and quality_type_id = :quality_type_id"
    
    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::assignment_quality_report -rest_user_id $rest_user_id -assignment_id $assignment_id]]
    return [cog_rest::json_response]
}

#---------------------------------------------------------------
#  GET functions
#---------------------------------------------------------------

namespace eval cog_rest::get {
    ad_proc -public tasks_without_package {
        -project_id:required
        {-target_language_id ""}
        {-package_type_id ""}
        -rest_user_id:required
    } {
        Returns a list of tasks which are not in a package

        @param project_id object im_project::read Project for which we want to return the assignments
        @param target_language_id category "Intranet Translation Language" Target language for which we want to get the tasks

        @return tasks json_array trans_task Array of the tasks which don't have a package
    } {

        # Append all unassigned tasks        
        set tasks [list]
        set where_clause_list [list]
        lappend where_clause_list "task_id not in (select trans_task_id from im_freelance_packages_trans_tasks)"

        if {$project_id ne ""} {
            lappend where_clause_list "project_id = :project_id"
        }

        if {$target_language_id ne ""} {
            lappend where_clause_list "target_language_id = :target_language_id"
        }

        db_foreach unassigned_package_tasks "
            select task_id as task_id, task_name as task_name,task_units, task_uom_id, billable_units, target_language_id,
                to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as deadline
            from im_trans_tasks 
            where [join $where_clause_list " and "]
        " {
            set task_deadline ""
            set task_type_id ""

            lappend tasks  [cog_rest::json_object] 
        }

        return [cog_rest::json_response]

    }

    ad_proc -public freelancer_fee {
        -freelancer_ids:required
        -uom_id:required
        -task_type_id:required
        -source_language_id:required
        -target_language_id:required
        { -subject_area_id "" }
        -rest_user_id:required
    } {
        Returns the fee for each freelancer for the given uom_id and task_type_id


        @param freelancer_ids object_array person Freelancer for whom to get the fees
        @param uom_id category "Intranet UoM" Unit of measure for the assignment. If empty, use the latest uom_id from the tasks (we should not have mixed uom_ids in packages)
        @param task_type_id category "Intranet Trans Task Type" Trans Task Type for which we want to view the fee
        @param source_language_id category "Intranet Translation Language" Source_language_ids to make this more specific
        @param target_language_id category "Intranet Translation Language" Target languages to specify the fee better
        @param subject_area_id category "Intranet Translation Subject Area" Subject area we look for

        @return fees json_array freelancer_fee array of fees
    } {
        set fees [list]

        foreach freelancer_id $freelancer_ids {
            set freelancer_name [im_name_from_id $freelancer_id]

            set price_id [webix::trans::best_price_id \
                -company_id [ im_user_main_company_id -user_id $freelancer_id ] \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -uom_id $uom_id]

            if {$price_id ne ""} {
                db_1row freelancer_price "select price as rate, currency, min_price, source_language_id, target_language_id from im_trans_prices where price_id = :price_id"
                lappend fees [cog_rest::json_object]                
            }
        }

        return [cog_rest::json_response]
    }

    ad_proc -public project_default_filters {
        -project_id:required
        -rest_user_id:required
    } {
        Returns a list of default values for filtering feelancer

        @param project_id object im_project::read project for which we need the filters

        @return filter_set json_object trans_project_filter Set of filters for this project
    } {
        
        
        set source_language_ids [webix::trans::project_valid_language_ids -project_id $project_id]
        set valid_target_language_ids [webix::trans::project_valid_language_ids -project_id $project_id -type "target"]
        set target_language_ids [im_target_language_ids $project_id]    
        set subject_area_id [db_string subject_area "select subject_area_id from im_projects where project_id = :project_id" -default ""]
        set skill_business_sector_ids [db_list business_sector "select skill_id from im_object_freelance_skill_map where skill_type_id = (select skill_type_id from im_freelance_skill_types where skill_type = 'Business Sector') and object_id = :project_id"]

        callback webix::assignment::default_filters -project_id $project_id -user_id $rest_user_id

        set filter_set [cog_rest::json_object]
        return  [cog_rest::json_response]
    }

    ad_proc -public project_workflow_steps {
        -project_id:required
        -rest_user_id:required
    } {
        Provide the list of workflow steps necessary to deliver the tasks in this project

        Works based of im_trans_tasks task_type_id. Not all tasks necessarily have to go through each of the steps, so
        this is only useful for e.g. displaying the columns for the steps in a project.

        @param project_id object im_project::read Project for which we need to retrieve the steps

        @return workflow_steps json_array trans_workflow_step Workflow steps of this project
    } {
        set workflow_steps [list]
        
        set trans_task_ids [db_list tasks "select task_id from im_trans_tasks where project_id = :project_id"]
        set trans_task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]
        set sort_order 1
        foreach task_type_id $trans_task_type_ids {
            lappend workflow_steps [cog_rest::json_object]
            incr sort_order
        }
        return [cog_rest::json_response]
    }

    ad_proc -public freelancers_for_package {
        -freelance_package_id:required
        -rest_user_id:required
    } {
        Returns a list of freelancers who could work in a specific package.

        @param freelance_package_id object im_freelance_package::read Package for which we look for freelancers

        @return freelancers json_array freelancer Array of freelancers for display in the freelancers selection screen
    } {
        set trans_task_id [db_string task "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id limit 1" -default ""]
        
        if {$trans_task_id ne ""} {
            set price_task_type_id [db_string package_type "select package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"]

            db_1row task_info "select t.project_id, t.source_language_id, t.target_language_id, t.task_units, t.task_uom_id, p.subject_area_id
                from im_projects p, im_trans_tasks t
                where t.project_id = p.project_id
                and t.task_id = :trans_task_id"

            return [cog_rest::get::freelancers_for_project \
                -project_id $project_id \
                -rest_user_id $rest_user_id \
                -source_language_ids $source_language_id \
                -target_language_ids $target_language_id \
                -subject_area_ids $subject_area_id \
                -price_task_type_id $price_task_type_id \
                -uom_id $task_uom_id]
        } else {
            cog_rest::error -http_status 400 -message "Could not find tasks for package"
        }
    }

    ad_proc -public freelancers_for_project {
        -project_id:required
        -rest_user_id:required
        { -source_language_ids "" }
        { -target_language_ids "" }
        { -subject_area_ids "" }
        { -skill_role_ids "" }
        { -skill_tm_tool_ids "" }
        { -skill_business_sector_ids ""}
        { -price_task_type_id "" }
        { -uom_id "324" }
    } {
        Returns a list of freelancers who could work in a specific project.

        All filters (everything except the project_id) include sub_categories

        @param project_id object im_project::read Project for which we search freelancers to work on.
        @param source_language_ids category_array "Intranet Translation Language" List of source_language_ids to filter, will not default to anything (so any source language is possible)
        @param target_language_ids category_array "Intranet Translation Language" List of target languages  to filter (will limit to valid languages of the project. Filter out any invalid target_lang)
        @param subject_area_ids category_array "Intranet Translation Subject Area" List of subject areas to filter for
        @param skill_role_ids category_array "Intranet Skill Role" List of Roles the freelancer has experience in. 
        @param skill_business_sector_ids category_array "Intranet Skill Business Sector" List of Business Sectors (in addition to subject_area) the freelancer has experience.
        @param skill_tm_tool_ids category_array "Intranet TM Tool" List of TM Tools the freelancer can work with.
        @param uom_id category "Intranet UoM" Unit of measure for which we want to show the price. Defaults to S-Word

        @return freelancers json_array freelancer Array of freelancers for display in the freelancers selection screen

    } {
        set freelancers [list]
        set from_clauses [list]
        set where_clauses [list]

        # Limit to open/potential freelancers
        set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]

        set source_language_skill_type_id [im_freelance_skill_type_source_language]
        if {$source_language_ids eq ""} {
            set source_language_ids [webix::trans::project_valid_language_ids -project_id $project_id]
        }

        if {[llength $source_language_ids] > 0} {
            lappend from_clauses "(select user_id from im_freelance_skills where skill_type_id = :source_language_skill_type_id and skill_id in ([template::util::tcl_to_sql_list $source_language_ids])) source_lang"
            lappend where_clauses "m.member_id = source_lang.user_id"
        }

        set target_language_skill_type_id [im_freelance_skill_type_target_language]
        set target_language_ids [webix::trans::project_valid_language_ids -project_id $project_id -language_ids $target_language_ids -type "target"]
        if {[llength $target_language_ids] > 0} {
            lappend from_clauses "(select user_id from im_freelance_skills where skill_type_id = :target_language_skill_type_id and skill_id in ([template::util::tcl_to_sql_list $target_language_ids])) target_lang"
		    lappend where_clauses "m.member_id = target_lang.user_id"
        }

        if {$subject_area_ids ne ""} {
            set subject_area_skill_type_id [im_freelance_skill_type_subject_area]
            lappend from_clauses "(select user_id from im_freelance_skills where skill_type_id = :subject_area_skill_type_id and skill_id in ([template::util::tcl_to_sql_list $subject_area_ids])) subject_area"
	    	lappend where_clauses "m.member_id = subject_area.user_id"
        }
        
        if {$skill_role_ids ne ""} {
            set role_skill_type_id [db_string role "select category_id from im_categories where category = 'Role' and category_type = 'Intranet Skill Type'" -default ""]
            if {$role_skill_type_id eq ""} {
                ns_log Error "Somebody is trying to filter by role_ids but the skill type is not configured"
            } else {
                lappend from_clauses "(select user_id from im_freelance_skills where skill_type_id = :role_skill_type_id and skill_id in ([template::util::tcl_to_sql_list $skill_role_ids])) role"
	        	lappend where_clauses "m.member_id = role.user_id"

                # Overwrite the price_task_type based of the skills for the role. 
                if {$price_task_type_id eq ""} {         
                    set skill_role_id [lindex $skill_role_ids 0]
                    set workflow_step [db_string workflow_step "select aux_string1 from im_categories where category_id = :skill_role_id" -default ""]
                    if {$workflow_step ne ""} {
                        set price_task_type_id [db_string price_type_id "select category_id from im_categories where aux_string1 = :workflow_step order by category asc limit 1" -default ""]
                    }
                }
            }
        }

        if {$skill_business_sector_ids ne ""} {
            set business_sector_skill_type_id 2024 ; # fixed number.
            lappend from_clauses "(select user_id from im_freelance_skills where skill_type_id = :business_sector_skill_type_id and skill_id in ([template::util::tcl_to_sql_list $skill_business_sector_ids])) business_sector"
           	lappend where_clauses "m.member_id = business_sector.user_id"
        }

        if {$skill_tm_tool_ids ne ""} {
            set tm_tool_skill_type_id [im_freelance_skill_type_tm_tool]
            lappend from_clauses "(select user_id from im_freelance_skills where skill_type_id = :tm_tool_skill_type_id and skill_id in ([template::util::tcl_to_sql_list $skill_tm_tool_ids])) tm_tool"
           	lappend where_clauses "m.member_id = tm_tool.user_id"
        }

        set customer_id [db_string final_company_or_customer "select coalesce(final_company_id,company_id) from im_projects where project_id = :project_id"]

        set freelance_sql "
		    select distinct
			    m.member_id as freelancer_id,
                im_name_from_id(m.member_id) as freelancer_name
		    from
			    group_member_map m,
			    (select object_id_two, company_status_id 
                    from acs_rels r, im_companies c 
                    where c.company_id = r.object_id_one 
                    and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])
                ) f, 
                [join $from_clauses " , "]
		    where m.group_id = [im_profile_freelancers] 
		    and m.member_id not in (
				-- Exclude banned or deleted users
				select	m.member_id
				from	group_member_map m,
					membership_rels mr
				where	m.rel_id = mr.rel_id and
					m.group_id = acs__magic_object_id('registered_users') and
					m.container_id = m.group_id and
					mr.member_state != 'approved') 
			and f.object_id_two = m.member_id
            and [join $where_clauses " and "]
		"
    
        set trans_task_ids [webix::assignments::remaining_trans_tasks -project_id $project_id]
        set task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]

        db_foreach freelancer $freelance_sql {

            # Status color, description and sort_order
            # Use an empty sort_order to determine if we have found the correct category for the user
            set freelancer_selection_status_id [webix::assignments::freelancer_selection_status_id -project_id $project_id -freelancer_id $freelancer_id]

            if {$freelancer_selection_status_id eq ""} {
                set sort_order 999
                set freelancer_status_color "grey"
                set freelancer_status_description ""
            } else {
                db_1row freelancer_selection_status "select aux_string1 as freelancer_status_color, coalesce(sort_order,99) as sort_order, category_description as freelancer_status_description
                    from im_categories
                    where category_id = :freelancer_selection_status_id"                
            }

            set rating [util_memoize [list webix::trans::quality_rating -freelancer_id $freelancer_id] 600]
            set worked_with [list]
            foreach task_type_id $task_type_ids {
                set total [llength [util_memoize [list webix::assignments::freelancer_assignment_ids -freelancer_id $freelancer_id -task_type_id $task_type_id] 600]]
                set customer [llength [util_memoize [list webix::assignments::freelancer_assignment_ids -freelancer_id $freelancer_id -task_type_id $task_type_id -customer_id $customer_id] 600]]
                lappend worked_with [cog_rest::json_object -object_class "freelancer_worked_with"]
            }

            
            set fee [list]
            if {$price_task_type_id ne ""} {
                set price_id [webix::trans::best_price_id \
                    -company_id [ im_user_main_company_id -user_id $freelancer_id ] \
                    -task_type_id $price_task_type_id \
                    -subject_area_id [lindex $subject_area_ids 0] \
                    -target_language_ids $target_language_ids \
                    -source_language_id [lindex $source_language_ids 0] \
                    -uom_id $uom_id]

                if {$price_id ne ""} {
                    db_1row freelancer_price "select price as rate, currency, min_price, source_language_id, target_language_id from im_trans_prices where price_id = :price_id"
                    lappend fee [cog_rest::json_object -object_class "freelancer_fee"]
                }
            }

            lappend freelancers [cog_rest::json_object]
        }
        return [cog_rest::json_response]
    }

    ad_proc -public freelancer_assignments {
        { -freelancer_id "" }
        { -assignment_id "" }
        -rest_user_id:required
        {-assignment_status_ids ""}
    } {
        Returns the list of assignments of a freelancer used in the freelancer list view or the single assignment view.

        Contains more information about the project than the normal translation_assignment endpoint.

        @param freelancer_id object im_person::read Freelancer for whom we want to get the list of assignments
        @param assignment_id object im_freelance_assignment::read Assignment we want to return (in case we need a single one)
        @param assignment_status_ids category_array "Intranet Freelance Assignment Status" Status of the assignment

        @return assignments json_array freelancer_assignment Assignments of the freelancer
    } {
        set assignments [list]
        set where_clause_list [list]
        lappend where_clause_list "f.project_id = p.project_id"
        lappend where_clause_list "f.freelance_package_id = a.freelance_package_id"
        if {$assignment_status_ids ne ""} {
            lappend where_clause_list "a.assignment_status_id in ([template::util::tcl_to_sql_list $assignment_status_ids])"
        }

        set sql_where_id ""
        if {$freelancer_id ne ""} {
            lappend where_clause_list "a.assignee_id = :freelancer_id"
        }

        if {$assignment_id ne ""} {
            lappend where_clause_list "a.assignment_id =:assignment_id"
        }

        set sql "select a.assignment_id, a.assignment_name,
            a.assignee_id, a.assignment_type_id, a.assignment_status_id,
            a.purchase_order_id, a.assignment_units, a.uom_id,
            a.rate, a.assignment_comment, 
            to_char(a.end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as assignment_deadline,
            to_char(a.start_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as start_date,
            f.freelance_package_id, f.freelance_package_name,f.package_comment,
            p.project_id, p.project_name,p.project_nr,p.project_lead_id,
            p.company_id,p.final_company_id,p.subject_area_id,
            r.object_id_two as provider_bill_id
            from im_freelance_packages f, im_projects p, im_freelance_assignments a
            left outer join acs_rels r on a.purchase_order_id = r.object_id_one
                and r.rel_type = 'im_invoice_invoice_rel'
            where [join $where_clause_list " and "]    
        "

        db_foreach assignment $sql {
            # We need to set source & target langauges to empty strings in case no trans_tasks are created
            # Another option would be to first check if trans_tasks exists, probably something to discuss
            set source_language_id ""
            set target_language_id ""
            set assignee_name "[im_name_from_id $assignee_id]"
            set project_lead_name "[im_name_from_id $project_lead_id]"
            db_0or1row languages "select source_language_id, target_language_id 
                from im_freelance_packages_trans_tasks fptt, im_trans_tasks tt
                where freelance_package_id = :freelance_package_id
                and tt.task_id = fptt.trans_task_id
                limit 1"

            if {$final_company_id eq ""} {
                set final_company_id $company_id
            }
            set skill_business_sector_ids [db_list business_sector "select skill_id from im_object_freelance_skill_map where skill_type_id = (select skill_type_id from im_freelance_skill_types where skill_type = 'Business Sector') and object_id = :project_id"]

            set package_files [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::cr_file -parent_id $freelance_package_id -rest_user_id $rest_user_id]]
            set assignment_files [cog_rest::helper::json_array_to_object_list -json_array  [cog_rest::get::cr_file -parent_id $assignment_id -rest_user_id $rest_user_id]]

            set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
            if {$project_folder_id eq ""} {
                set project_folder_id [intranet_fs::create_project_folder -project_id $project_id -user_id $rest_user_id]
            }

            lappend assignments [cog_rest::json_object]            
        }
        
        return [cog_rest::json_response]

    }

    ad_proc -public previous_assignments {
        -assignment_id:required
        { -assignment_status_ids "" }
        -rest_user_id:required
    } {
        Returns the list of previous assignments which have the same tasks in them. Used for rating freelancers

        @param assignment_id object im_freelance_assignment::read Assignment we want to return the previous assignments for
        @param assignment_status_ids category_array "Intranet Freelance Assignment Status" Status of the previous assignments we look for

        @return assignments json_array assignment Array of assignments
    } {

    
        set assignments [list]
        set freelance_package_id [db_string package_id "select freelance_package_id from im_freelance_assignments where assignment_id = :assignment_id"]
        set previous_package_ids [webix::packages::previous_packages -freelance_package_id $freelance_package_id]

        if {$previous_package_ids ne ""} {
            set where_clause_list [list "freelance_package_id in ([template::util::tcl_to_sql_list $previous_package_ids])"]

            if {$assignment_status_ids ne "" } {
                lappend where_clause_list "assignment_status_id in ([template::util::tcl_to_sql_list $assignment_status_ids])"
            }

            set previous_assignment_ids [db_list assignments "select distinct assignment_id from im_freelance_assignments where [join $where_clause_list " and "] "]

            foreach previous_assignment_id $previous_assignment_ids {
                lappend assignments [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::translation_assignments -rest_user_id $rest_user_id -assignment_id $previous_assignment_id]]
            }
        } 
        return [cog_rest::json_response]   
    }
}

#---------------------------------------------------------------
# POST Functions
#---------------------------------------------------------------
namespace eval cog_rest::post {

    ad_proc -public assignment_remind_freelancer {
        -assignment_id:required
        -rest_user_id
    } {
        Remind the freelancer about the assignment so he / she should accept it. Only works on requested assignments

        @param assignment_id object im_freelance_assignment::read Assignment for which we want to send the reminder

        @return reminder json_object Reminder send
        @return_reminder success boolean was sending the reminder Successful
        @return_reminder message string message to display back to the user
        @return_reminder body string Body of the mail send to the freelancer
    } {
        set assignee_id [db_string requested "select assignee_id from im_freelance_assignments where assignment_id = :assignment_id and assignment_status_id = 4221" -default 0]
        if {$assignee_id ne 0} {
            # Try to send the mail
            set success 1
            set locale [lang::user::locale -user_id $rest_user_id]
            set message [lang::message::lookup $locale sencha-portal.message_sent_message]
            set assignee_locale [lang::user::locale -user_id $assignee_id]
            set salutation_pretty [im_invoice_salutation -person_id $assignee_id]

            db_1row assignment_info "select assignment_name, assignee_id, project_id, fa.rate, fa.uom_id, fa.end_date as deadline, assignment_units, fa.freelance_package_id, assignment_type_id, assignment_status_id
                from im_freelance_assignments fa, im_freelance_packages fp
                where fa.freelance_package_id = fp.freelance_package_id
                and fa.assignment_id = :assignment_id"

            db_1row project_info "
                select im_name_from_id(project_lead_id) as project_manager,
                project_nr, project_type_id, project_lead_id, subject_area_id, im_name_from_id(source_language_id) as source_language
                from im_projects
                where project_id=:project_id"
            

            set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
            
            if {[llength $task_ids] > 0} {
                # Get the messages by language of the user
                db_1row languages "select source_language_id, target_language_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids]) limit 1 "
                
                set source_language [im_category_from_id -current_user_id $assignee_id $source_language_id]
                set target_language [im_category_from_id -current_user_id $assignee_id $target_language_id]

                set task_type_pretty [im_category_from_id -current_user_id $assignee_id $assignment_type_id]
                
                set subject "[lang::message::lookup $assignee_locale webix-portal.lt_enquiry_reminder_subject]"

                set body "[lang::message::lookup $assignee_locale webix-portal.lt_enquiry_reminder_body]"
                

                if {[intranet_chilkat::send_mail \
                    -to_party_id $assignee_id \
                    -from_party_id $rest_user_id \
                    -subject $subject \
                    -body $body \
                    -no_callback] eq ""} {
                    set success 0
                    set message "We could not send the mail due to mail server complications. Contact your sysadmin with the project_nr, freelance and current date/time"
                }
            } else {
                set success 0
                set body ""
                set message "No tasks for this assignment. No need to remind the freelancer"
            }
        } else {
            set success 0
            set message "Can't send a reminder as the assignment is not in requested state anymore"
            set body ""
        }
        
        return [cog_rest::json_object]
    }
}

#---------------------------------------------------------------
# Webix assignments helper procs
#---------------------------------------------------------------

namespace eval webix::assignments {

    ad_proc url {
        -assignment_id:required
    } {
        Returns the deep url to directly access an assignment

        @param assignment_id Id of the assignment we want to reach
    } {
        set portal_url [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
        return [export_vars -base "${portal_url}/#!/assignment-details" -url {assignment_id}]
    }

    ad_proc freelancer_selection_status_id {
        -freelancer_id:required
        -project_id:required
    } {
        Returns the freelancer selection status, acts as a wrapper procedure for custom implementations

        @param freelancer_id Freelancer we are looking at for doing some work on the project
        @param project_id Project for which we select freelancers

        @return freelancer_selection_status_id Based of "Intranet Freelancer Selection Status" category
    } {

        # Deal with the various status of freelancers
        set accepted_assignee_p 0
        set requested_assignee_p 0
        set rejected_assignee_p 0
        set finished_assignee_p 0

        db_foreach assignment {
            select distinct assignment_status_id
            from im_freelance_assignments fa, im_freelance_packages fp
            where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id
            and assignee_id = :freelancer_id
        } {
            switch $assignment_status_id {
                4220 - 4221 {
                    set requested_assignee_p 1
                }
                4222 - 4224 - 4229 {
                    set accepted_assignee_p 1
                }
                4223 - 4227 {
                    set rejected_assignee_p 1
                }
                4225 - 4226 - 4231 {
                    set finished_assignee_p 1
                }
            }
        }

        # We need to loop through the status in the sort order, so we can stop at the "highest" found
        # This allows for changing the sort order of status.

        set status_ids [db_list status_ids "select category_id from im_categories where category_type = 'Intranet Freelancer Selection Status' order by sort_order"] 

        foreach status_id $status_ids {
            switch $status_id {
                4250 {
                    set main_translator_ids [webix::trans::main_translator_ids -project_id 1133488]    
                    if {[lsearch $main_translator_ids $freelancer_id]>-1} {
                        return 4250
                    }            
                }
                4251 {
                    set potential_status_ids [im_sub_categories [im_company_status_potential]]
                    set potential_p [db_string freelancer_company_status "
                        select 1 
                        from acs_rels r, im_companies c 
                        where c.company_id = r.object_id_one 
                        and r.object_id_two = :freelancer_id
                        and company_status_id in ([template::util::tcl_to_sql_list $potential_status_ids])
                        limit 1
                    " -default 0]

                    if {$potential_p} {
                        return 4251
                    }
                }
                4252 {
                    if {$requested_assignee_p} {
                        return 4252
                    }
                }
                4253 {
                    if {$accepted_assignee_p} {
                        return 4253
                    }
                }
                4254 {
                    if {$finished_assignee_p} {
                        return 4254
                    }
                }
                4255 {
                    if {$rejected_assignee_p} {
                        return 4255
                    }
                }
            }
        }

        # No specific status found
        return ""
    }
    

    ad_proc -public trans_task_type_ids {
        -trans_task_ids:required
    } {
        Returns the package_types for the tasks provided

        @param trans_task_ids Task_ids as used in im_trans_tasks

        @return IDs of translation task types, derived from the task_type_id using aux_string1
    } {
        set trans_task_type_ids [list]
        set trans_task_types [list]
        if {[llength $trans_task_ids] eq 0} { return "" }

        # Get all the aux_string1, then loop through all of their positions
        
        set workflows [db_list task_names "
            select distinct aux_string1 from (
                select aux_string1, sort_order
                from im_trans_tasks t, im_categories c
                where t.task_id in ([template::util::tcl_to_sql_list $trans_task_ids])
                and c.category_id = t.task_type_id
                order by sort_order
            ) tc
        "]

        # Get the initial steps from the project workflow
        set trans_task_id  [lindex $trans_task_ids 0]
        set project_id [db_string project_id "select project_id from im_trans_tasks where task_id = :trans_task_id"]
        set project_workflow_steps [db_string steps "select aux_string1 from im_projects p, im_categories c where project_id = :project_id and p.project_type_id = c.category_id"]
        set i 0
        foreach project_step $project_workflow_steps {
            set trans_task_type_id [db_string package_types "select category_id from im_categories where lower(category) = lower(:project_step) and category_type = 'Intranet Trans Task Type'" -default ""]
            set task_type_ids_for_step($i) $trans_task_type_id
            set task_types_for_step($i) $project_step
            incr i
        }

        # How many steps do we have ?
        set no_workflow_steps 0
        foreach workflow $workflows {
            set no_steps [llength $workflow]
            if {$no_steps > $no_workflow_steps} {
                set no_workflow_steps $no_steps
            }
        
            set i 0
            
            foreach task_type $workflow {
                if {![info exists task_types_for_step($i)]} {
                    set trans_task_type_id [db_string package_types "select category_id from im_categories where lower(category) = lower(:task_type) and category_type = 'Intranet Trans Task Type'" -default ""]
                    set task_type_ids_for_step($i) $trans_task_type_id
                    set task_types_for_step($i) $task_type
                } else {
                    if {[lsearch $task_types_for_step($i) $task_type]<0} {
                        set trans_task_type_id [db_string package_types "select category_id from im_categories where lower(category) = lower(:task_type) and category_type = 'Intranet Trans Task Type'" -default ""]
                        lappend task_type_ids_for_step($i) $trans_task_type_id
                        lappend task_types_for_step($i) $task_type
                    }
                }
                incr i
            }
        }

        for {set i 0} {$i<$no_steps} {incr i} {
            foreach task_type_id $task_type_ids_for_step($i) {
                if {[lsearch $trans_task_type_ids $task_type_id]<0} {
                    lappend trans_task_type_ids $task_type_id
                }
            }
        }

        return $trans_task_type_ids
    }


    ad_proc -public remaining_trans_tasks {
        -project_id:required
    } {
        Returns the list of remaining trans tasks

        @param project_id project for which we look for the tasks
    } {
        set freelance_package_ids [db_list project_packages_sql "select freelance_package_id from im_freelance_packages where project_id=:project_id"]
        
        if {$freelance_package_ids eq ""} {
            return ""
        }
        # Remove all freelance_package_ids which have a finished
        set finished_package_ids [db_list finished_assignments "select freelance_package_id from im_freelance_assignments 
            where freelance_package_id in ([template::util::tcl_to_sql_list $freelance_package_ids])
            and assignment_status_id in (4225,4226,4231)
            "]

        foreach finished_package_id $finished_package_ids {
            set freelance_package_ids [lsearch -inline -all -not -exact $freelance_package_ids $finished_package_id]
        }

        if {$freelance_package_ids eq ""} {
            set remaining_task_ids ""
        } else {
            set remaining_task_ids [db_list remaining_trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks 
                where freelance_package_id in ([template::util::tcl_to_sql_list $freelance_package_ids])"]
        }
        
        return $remaining_task_ids
    }

    ad_proc -public freelancer_assignment_ids {
        -freelancer_id:required
        { -customer_id "" }
        { -task_type_id "" }
    } {
        Returns the assignment_ids of the freelancer with the customer or in general for a specific task_type

        @param freelancer_id Freelancer who did the work
        @param task_type_id Type of task the freelancer was assigned to do
        @param customer_id Limit to only assignments for a customer
    } {
    	return [util_memoize [list webix::assignments::freelancer_assignment_ids_helper -freelancer_id $freelancer_id -customer_id $customer_id -task_type_id $task_type_id] 600]
    }

    ad_proc freelancer_assignment_ids_helper {
        -freelancer_id:required
        { -customer_id "" }
        { -task_type_id "" }
    } {
        Returns the assignment_ids of the freelancer with the customer or in general for a specific task_type

        @param freelancer_id Freelancer who did the work
        @param task_type_id Type of task the freelancer was assigned to do
        @param customer_id Limit to only assignments for a customer
    } {
        set where_clauses [list]
        
        if {$task_type_id ne ""} {
            lappend where_clauses "fp.package_type_id = :task_type_id"
        }

        if {$customer_id ne ""} {
            lappend where_clauses "fp.project_id in (select project_id from im_projects where company_id = :customer_id or final_company = :customer_id)"
        }

        set sql "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
            where fa.assignee_id = :freelancer_id
            and fp.freelance_package_id = fa.freelance_package_id
            and fa.assignment_status_id in (4225,4226,4231)"

        if {$where_clauses ne ""} {
            append sql " and [join $where_clauses " and "]"
        }
        set assignment_ids [db_list assignment_ids $sql]
        return $assignment_ids
    }

    ad_proc -public create {
        -freelance_package_id:required
        -assignee_id:required
        -user_id:required
        {-assignment_status_id "4222"}
        {-assignment_units ""}
        {-uom_id ""}
        {-rate ""}
        {-start_date ""}
        {-end_date ""}
        {-ignore_min_price_p 0}
        {-assignment_comment ""}
        
    } {
        Create an assignment for a freelancer
        
        @param freelance_package_id
        @param assignee_id User who will work on this task
        @param assignment_status_id Status of this assignment. Defaults to accepted.
        @param assignment_units How many Hours/Source words etc. are in this assignment. Will be calculated for the freelancer if empty
        @param uom_id Unit of measure for this assignment. Defaults to tasks uom_id
        @param rate Rate for this assignment. Calculate to the default rate for the freelancer if missing
        @param start_date Expected start_date for this assignment in \"YYYY-MM-DD HH24:MI\" format.
        @param end_date Deadline for this assignment in  \"YYYY-MM-DD HH24:MI\" format
        @param assignment_comment e.g. comment, note, additional information

        @return assignment_id Id of the created assignment. Alternatively the assignment_id if it already existed.
    } {
  	    set assignment_id [db_string assignment "select max(assignment_id) from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignee_id = :assignee_id" -default ""]
        if {$assignment_id ne ""} {
            return $assignment_id
        }

        if {$uom_id eq ""} {
            set uom_id [webix::packages::uom_id -freelance_package_id $freelance_package_id]
        }

        if {$assignment_units eq ""} {
            set assignment_units [webix::packages::trans_task_units -freelance_package_id 683116 -object_id $assignee_id]
        }

        set provider_id [im_user_main_company_id -user_id $assignee_id]
        set assignment_type_id [db_string freelance_package_type "select package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"]

        set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
        set task_id [lindex $trans_task_ids 0]

       	db_1row get_task_info "select p.project_id, tt.source_language_id, tt.target_language_id, subject_area_id from im_trans_tasks tt, im_projects p where p.project_id = tt.project_id and task_id = :task_id"
        set material_id [im_trans_material_create_from_parameters -task_type_id $assignment_type_id -source_language_id $source_language_id -target_language_id $target_language_id -task_uom_id $uom_id]

        # Find the currency for the provider by looking at the prices
        set currency ""
        if {[im_table_exists "im_trans_prices"]} {
            db_0or1row currency "select currency, count(*) as num_prices from im_trans_prices where company_id = :provider_id group by currency order by num_prices desc limit 1"
        }
        
        if {$currency eq ""} {
            set currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
        }


        if {$rate eq ""} {
		    set rate_info [im_translation_best_rate \
                -provider_id $provider_id \
                -task_type_id $assignment_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -task_uom_id $uom_id \
                -currency $currency \
                -ignore_min_price_p 1 \
                -task_sum $assignment_units]
            set rate [lindex $rate_info 2]
        }

	    if {!$ignore_min_price_p} {		
            # ---------------------------------------------------------------
            #  Minimum Price handling
            # ---------------------------------------------------------------
            

		    set total_rate_info [im_translation_best_rate \
                -provider_id $provider_id \
                -task_type_id $assignment_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -task_uom_id $uom_id \
                -currency $currency \
                -ignore_min_price_p $ignore_min_price_p \
                -task_sum $assignment_units]
		
            # Split the rate information up into the components
            set total_billable_units [lindex $total_rate_info 0]
            set total_uom_id [lindex $total_rate_info 1]
            set total_rate [lindex $total_rate_info 2]
            
            if {[im_uom_unit] eq $total_uom_id && $uom_id ne [im_uom_unit]} {
                # We found a language where the tasks together are below the
                # Minimum Price.
                
                set assignment_units 1
                set uom_id $total_uom_id
                set material_id [im_trans_material_create_from_parameters -task_type_id $assignment_type_id -source_language_id $source_language_id -target_language_id $target_language_id -task_uom_id $total_uom_id]
                set rate $total_rate
                
            }		
	    }

	    set assignment_name "[im_material_name -material_id $material_id] for [person::name -person_id $assignee_id] "
	    set ip_address [ad_conn peeraddr]
	    set assignment_id [db_string assignment "select im_freelance_assignment__new(
	    	:user_id,		-- creation_user
			:ip_address,		-- creation_ip
			:assignment_name,
			:freelance_package_id,
			:assignee_id,
			:assignment_type_id,
			:assignment_status_id,
			:material_id,
			:assignment_units,
			:uom_id,
			:rate,
			:start_date,
			:end_date,
			:assignment_comment )"]

        permission::grant -party_id $assignee_id -object_id $assignment_id -privilege "read"
        permission::grant -party_id $assignee_id -object_id $freelance_package_id -privilege "read"
        permission::grant -party_id [im_profile_employees] -object_id $assignment_id -privilege "write"
        
        return $assignment_id
    }

    ad_proc -public create_purchase_order {
        -assignment_id:required
        { -user_id "" }
    } {
        Create the purchase order for an assignment. 
        
        @param assignment_id Assignment for which we want to generate the purchase_order
    } {
        db_1row assignment_info "select assignment_type_id, freelance_package_id, assignee_id,
            material_id as assignment_material_id, purchase_order_id, assignment_units,
            uom_id as assignment_uom_id, rate as assignment_rate, start_date, end_date as assignment_end_date,
            im_name_from_id(assignment_type_id) as action
            from im_freelance_assignments fa
            where fa.assignment_id = :assignment_id"

        set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
        db_1row project_info "select project_lead_id, project_cost_center_id from im_projects where project_id = :project_id"
    	
        set locale [lang::user::locale -user_id $assignee_id]

        if {$user_id eq ""} {
            set user_id $project_lead_id
        }

        set invoice_id [cog::invoice::create \
            -provider_contact_id $assignee_id \
            -customer_contact_id $project_lead_id \
            -invoice_status_id [im_cost_status_created] \
            -invoice_type_id [im_cost_type_po] \
            -project_id [im_project_id_from_assignment_id -assignment_id $assignment_id] \
            -delivery_date $assignment_end_date \
            -cost_center_id $project_cost_center_id
        ]
        
        set task_date_pretty [lc_time_fmt $assignment_end_date "%x %X" $locale]
        
        db_1row invoice_info "select currency from im_costs where cost_id = :invoice_id"

        # Total task_units which might be the same as the assignment ones
	    set task_units [db_string task_units "select sum(task_units) from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
            where fptt.trans_task_id = tt.task_id
            and fptt.freelance_package_id = :freelance_package_id" -default 0]

        # Append the Tasks to the provider bill as line items
        db_foreach task_tasks "select source_language_id, target_language_id,
		    task_id, task_name, task_uom_id, task_units as po_billable_units
		    from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
		    where fptt.trans_task_id = tt.task_id
		    and fptt.freelance_package_id = :freelance_package_id
	    " {
		    set item_name "[im_category_from_id -locale $locale $assignment_type_id]: $task_name ([im_category_from_id -locale $locale $source_language_id] -> [im_category_from_id -locale $locale $target_language_id]) Deadline: \"$task_date_pretty CET\""

            # ---------------------------------------------------------------
            # Create the line items
            # ---------------------------------------------------------------

		    # Reset rate if we have a different number of assignment units.
		    if {$assignment_units ne $task_units} {
    			set line_assignment_rate "0"
			    set po_billable_units "0"
		    } else {
    			set line_assignment_rate $assignment_rate
		    }
	
		    set task_material_id [im_trans_material -task_type_id $assignment_type_id \
                -source_language_id $source_language_id \
                -target_language_id $target_language_id \
                -task_uom_id $task_uom_id]

		    incr sort_order
		
            cog_rest::post::invoice_item -invoice_id $invoice_id \
                -item_name $item_name \
                -item_uom_id $task_uom_id \
                -item_units $po_billable_units \
                -price_per_unit $line_assignment_rate \
                -sort_order $sort_order \
                -item_material_id $task_material_id \
                -task_id $task_id \
                -rest_user_id $user_id
	    }
	
	    if {$assignment_units ne $task_units} {

		    # Check if we have only one line item
		    set num_line_items [db_string line_items "select count(*) from im_invoice_items where invoice_id = :invoice_id" -default 0]


            if {$num_line_items > 1} {
                # add a line item with the total price
                set item_name "[im_category_from_id $assignment_type_id]"

                incr sort_order

                cog_rest::post::invoice_item -invoice_id $invoice_id \
                    -item_name $item_name \
                    -item_uom_id $assignment_uom_id \
                    -item_units $assignment_units \
                    -price_per_unit $assignment_rate \
                    -sort_order $sort_order \
                    -item_material_id $assignment_material_id \
                    -rest_user_id $user_id

            } else {
                set line_item_id [db_string line_item "select item_id from im_invoice_items where invoice_id = :invoice_id"]
                db_dml update_line_item "update im_invoice_items set item_units = :assignment_units, item_uom_id = :assignment_uom_id,
                    item_material_id = :assignment_material_id, price_per_unit = :assignment_rate where item_id = :line_item_id"
            }
        }

        # Recalculate and update the invoice amount
        cog::invoice::update_rounded_amount  -invoice_id $invoice_id

        im_audit -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id [im_cost_status_created] -type_id [im_cost_type_po]

        db_dml record_po_assignment "update im_freelance_assignments set purchase_order_id = :invoice_id where assignment_id = :assignment_id"
	
    	return $invoice_id
    }
}


#---------------------------------------------------------------
# Webix packages helper procs
#---------------------------------------------------------------

namespace eval webix::packages {

    ad_proc -public previous_packages {
        -freelance_package_id:required
    } {
        Returns a list of directly previous freelance_package_ids. Who have the same tasks.

        @param freelance_package_id
    } {
        db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"
        set trans_task_ids [db_list tasks "select task_id from im_trans_tasks where project_id = :project_id"]
        set trans_task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]
        
        set workflow_step_nr [lsearch $trans_task_type_ids $package_type_id]
        if {$workflow_step_nr eq 0} {
            # First step, no need to check for previous packages
            return ""
        } else {
            set previous_type_id [lindex $trans_task_type_ids [expr $workflow_step_nr -1]]
            set package_task_ids [db_list package_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
            if {[llength $package_task_ids] > 0} {
                set previous_package_ids [db_list previous_packages "select distinct fp.freelance_package_id 
                    from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt
                    where fp.freelance_package_id = fptt.freelance_package_id
                    and fp.freelance_package_id != :freelance_package_id
                    and fptt.trans_task_id in ([template::util::tcl_to_sql_list $package_task_ids])
                    and fp.package_type_id = :previous_type_id"]
                return $previous_package_ids
            } else {
                return ""
            }
        }
    }


    ad_proc -public calculate_deadline {
        -freelance_package_id:required
        {-start_timestamp ""}
    } {
        Returns the deadline for a freelance_package_id. If it has assignments, use the closest deadline of the assignments.

        @param freelance_package_id PackageID for which to calculate the deadline
        @param start_timestamp Timestamp which we use to calculate the deadline from. Use previous steps end date or project start date if not provided
    } {
        set has_assignments [db_string assignments_p "select 1 from im_freelance_assignments where freelance_package_id = :freelance_package_id limit 1" -default 0]
        if {$has_assignments} {
            set package_deadline [db_string min_deadline "select min(end_date) from im_freelance_assignments where freelance_package_id = :freelance_package_id" -default ""]
        } else {
            # Need to calculate the package deadline
            
            db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"
            set previous_package_ids [webix::packages::previous_packages -freelance_package_id $freelance_package_id]
            if {$start_timestamp eq ""} {
                
                set start_timestamp [db_string start_date "select coalesce(start_date, now()) from im_projects where project_id = :project_id"]

                foreach previous_package_id $previous_package_ids {
                    # Deadline is calculated based of the previous deadlines if they are later than the project start. which they should :)
                    set previous_deadline [webix::packages::calculate_deadline -freelance_package_id $previous_package_id]
                    if {$previous_deadline > $start_timestamp} {
                        set minutes_between_steps [parameter::get_from_package_key -package_key "webix-portal" -parameter "MinutesBetweenTransSteps" -default 0]
                        if {$minutes_between_steps eq 0} {
                            set start_timestamp $previous_deadline
                        } else {
                            set start_timestamp [db_string add_minutes "select timestamp '$previous_deadline' + interval '$minutes_between_steps minutes' from dual" -default $previous_deadline]
                        }
                    }
                }
            }
            
            # Now we know when to start, calculate the days and add them.
            # Then do additional logic.
            set processing_days [webix::packages::processing_days -freelance_package_id $freelance_package_id]
      		set package_deadline "[im_translation_processing_deadline -start_timestamp $start_timestamp -days $processing_days]"
        }

        callback webix::packages::after_deadline_calculation -freelance_package_id $freelance_package_id -package_deadline $package_deadline
        return $package_deadline
    }



    ad_proc -public processing_days {
        -freelance_package_id:required
    } {
	    Returns the calculated duration of working days needed to finish the freelance_package_id

        @param freelance_package_id Package we want to get the uom for
        @return working days Number of working days
    } {
        set uom_id [webix::packages::uom_id -freelance_package_id $freelance_package_id]
        set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
        if {[llength $trans_task_ids] eq 0} {
            # No tasks => no processing_days
            return ""
        }

        switch $uom_id {
            320 {
                # Hour
                set units_per_day [parameter::get_from_package_key -package_key "intranet-timesheet2" -parameter "TimesheetHoursPerDay"]
            }
            321 {
                # Day
                set units_per_day 1
            }
            324 - 325 {
                # Words
                set units_per_day [db_string package_type "select aux_int1 from im_freelance_packages fp, im_categories c where fp.freelance_package_id = :freelance_package_id and fp.package_type_id = c.category_id"]
            }
            328 {
                # Week
                set working_days [parameter::get_from_package_key -package_key "intranet-timesheet2" -parameter "TimesheetWeeklyLoggingDays"]
                set units_per_day [expr 1 / [llength $working_days]]
            }
            330 {
                # Text
                set package_type [db_string package_type "select category from im_freelance_packages fp, im_categories c where fp.freelance_package_id = :freelance_package_id and fp.package_type_id = c.category_id"]
                switch $package_type {
                    trans - copy {
                        set units_per_day 1 
                    }
                    proof - edit {
                        set units_per_day 3
                    }
                    default {
                        set units_per_day ""
                    }
                }
            }
            default {
                # Can't compute
                return ""
            }
        }

        if {$units_per_day ne ""} {
            set days [db_string tu "select ceil(coalesce(sum(task_units),0)/:units_per_day) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $trans_task_ids])" -default 0]
        } else {
            set days "0"
        }

        return [format "%.0f" $days]
    }

    ad_proc -public uom_id {
        -freelance_package_id:required
    } {
        Returns the uom_id for a freelance_package_id based of the trans_tasks

        @param freelance_package_id Package we want to get the uom for
        @return uom_id Unit of Measure category

        @error Returns nothing if we can't find a task or if we have multiple task_uom_id (which should not be the case)
    } {
        set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]

        if {[llength $trans_task_ids] eq 0} {
            return ""
        }

        set task_uom_ids [db_list task_uoms "select distinct task_uom_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $trans_task_ids])"]

        if {[llength $task_uom_ids] eq 1} {
            return $task_uom_ids
        } else {
            return ""
        }

    }
    
    ad_proc -public trans_task_units {
        { -freelance_package_id "" }
        { -trans_task_ids "" }
        { -package_type_id "" }
        { -object_id "" }
    } {
        Return the task_units for a freelance_package_id or bunch of trans_task_ids

        @param freelance_package_id Package for which we need the units
        @param trans_task_ids List of task_ids for which we calculate the units
        @param object_id Object for which we calculate the task_units. Might be the freelancer, the customer or the project
    } {
        if {$freelance_package_id ne ""} {
            set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
            set package_type_id [db_string package_type "select package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"]
        }

        if {$trans_task_ids eq ""} {
            return ""
        }

        set total_units 0
        
        if {$object_id eq "" || $package_type_id eq ""} {
            set total_units [db_string task_units "select sum(task_units) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $trans_task_ids])" -default 0]
        } else {
            set task_type [im_category_from_id -translate_p 0 $package_type_id]

            foreach trans_task_id $trans_task_ids {
                db_1row task_matrix "select match_x,match_rep,match100,match95,match85,match75,match50,match0,
						match_perf,match_cfr,match_f95,match_f85,match_f75,match_f50,locked, task_units
					from im_trans_tasks where task_id = :trans_task_id"

		        set trans_task_units [im_trans_trados_matrix_calculate $object_id \
					$match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
					$match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]

                if {$trans_task_units < 1} {
                    set trans_task_units $task_units
                }
                set total_units [expr $total_units + $trans_task_units]
            }
        }
        return [format "%.0f" $total_units]
    }


    ad_proc -public create {
        -trans_task_ids:required
        { -package_type_id "" }
        { -package_name "" }
        { -user_id "" }
        -overwrite:boolean
    } {
        Generates one (or more) packages for the task_ids if not already assigned to a package.

        Assumes we want to generate one package per target language (for the time being)

        @param trans_task_ids List of trans task_ids which we want to generate a package
        @param package_type_id Task Type for which we want to generate the package. If empty, generate packages for all types of the project
        @param overwrite if set, we will overwrite existing package assignments (if possible). Otherwise we only generate a package for the not already assigned tasks.
        @param user_id Who is generating the packages

        @return package_ids List of package_ids generated
    } {
        # We need to generate the freelance_package
    	set p_task_id [lindex $trans_task_ids 0]
	    set project_id [db_string project_id "select project_id from im_trans_tasks where task_id = :p_task_id" -default ""]
        if {$project_id eq ""} {
            ns_log Error "We can't generate the packages for $trans_task_ids .. could not find a project"
            return ""
        }        

        if {$user_id eq ""} { set user_id [auth::get_user_id]}
        set ip_address [ad_conn peeraddr]

        set package_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]

        if {$package_type_id ne ""} {
            # Check if the package_type_id is valid
            if {[lsearch $package_type_ids $package_type_id]<0} {
                # not a valid package_type, return
                ns_log Error "Attempt to create a package of type $package_type_id for trans_task_id $trans_task_ids, which is not valid"
                return
            } else {
                set package_type_ids $package_type_id
            }
        }

        set package_ids [list]

        foreach package_type_id $package_type_ids {

            # Check that we can actually create a new package as we have tasks to create with left
            
            if {$overwrite_p} {
                # We need to prevent overwriting of packages with PO
                set freelance_packages_with_po  [db_list po_created "select fa.freelance_package_id
                    from im_freelance_assignments fa, im_freelance_packages fp
                    where fa.freelance_package_id = fp.freelance_package_id
                    and fp.project_id = :project_id
                    and fa.assignment_status_id <> 4230
                    and fa.purchase_order_id is not null"]
                
                set exclude_package_ids $freelance_packages_with_po
            } else {
                set project_freelance_packages [db_list project_freelance_packages "select freelance_package_id from im_freelance_packages 
                    where project_id = :project_id
                    and package_type_id = :package_type_id"]
                
                set exclude_package_ids $project_freelance_packages
            }

            set assignable_task_sql "select task_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $trans_task_ids])"
            if {$exclude_package_ids ne ""} {
                append assignable_task_sql " and task_id not in (
                        select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id in ([template::util::tcl_to_sql_list $exclude_package_ids]))"
            }
            set assignable_task_ids [db_list assignable_task_ids $assignable_task_sql]

            if {[llength $assignable_task_ids]>0} {
                
                if {$overwrite_p} {
                    foreach trans_task_id $assignable_task_ids {
                        # Delete first so we don't get the same package_name back, as it would find 
                        # the package name (which exists) for the trans_task_ids
                        db_dml delete_mapping "delete from im_freelance_packages_trans_tasks 
                            where trans_task_id = :trans_task_id 
                            and freelance_package_id in (
                                select freelance_package_id from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id
                            )"
                    }
                }

                if {$package_name eq ""} {
                    set this_package_name [webix::packages::package_name -trans_task_ids $trans_task_ids -package_type_id $package_type_id]
                } else {
                    set this_package_name $package_name
                }

                #---------------------------------------------------------------
                # Assume one package per target language
                #---------------------------------------------------------------
                set target_language_ids [db_list target_languages "select target_language_id from im_trans_tasks 
                    where task_id in ([template::util::tcl_to_sql_list $assignable_task_ids])"]

                foreach target_language_id $target_language_ids {
                    set package_task_ids [db_list target_languages "select task_id from im_trans_tasks 
                        where task_id in ([template::util::tcl_to_sql_list $assignable_task_ids])
                        and target_language_id = :target_language_id"]

                    if {[llength $target_language_ids] >1} {
                        set target_package_name "${this_package_name}_[im_name_from_id $target_language_id]"
                    } else {
                        set target_package_name $this_package_name
                    }

                    set freelance_package_id [db_string assignment "select im_freelance_package__new(
                        :user_id,		-- creation_user
                        :ip_address,		-- creation_ip
                        :package_type_id,
                        null,
                        :project_id,
                        :target_package_name)"]

                    foreach trans_task_id $package_task_ids {
                        # Delete first if overwrite
                        if {$overwrite_p} {
                            db_dml delete_mapping "delete from im_freelance_packages_trans_tasks 
                                where trans_task_id = :trans_task_id 
                                and freelance_package_id in (
                                    select freelance_package_id from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id
                                )"
                        }

                        db_dml add_mapping "insert into im_freelance_packages_trans_tasks (freelance_package_id,trans_task_id) values (:freelance_package_id, :trans_task_id)"
                    }

                    catch {
                        callback webix_freelance_packages_after_create -trans_task_ids $package_task_ids -freelance_package_id $freelance_package_id
                    }

                    lappend package_ids $freelance_package_id
                }
            }
        }

        return $package_ids
    }

    ad_proc -public package_name {
        -trans_task_ids:required
        -package_type_id:required
    } {
        Return the package name for the task_type_ids and the task_type_id


    } {
        # Check if we already have package_names for the tasks.
        set package_names [db_list package_names "select distinct freelance_package_name from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt 
            where fptt.freelance_package_id = fp.freelance_package_id and package_type_id = :package_type_id and fptt.trans_task_id in ([template::util::tcl_to_sql_list $trans_task_ids])"]

        if {[llength $package_names] eq 1} {
            set package_name [lindex $package_names 0]
        } else {
            # We have either no package_name, then we need to generate it.
            # Alternative we have more than one, then we need a new one if we want to assign the tasks in a package
          	set p_task_id [lindex $trans_task_ids 0]
	        set project_id [db_string project_id "select project_id from im_trans_tasks where task_id = :p_task_id" -default ""]
            set counter [db_string counter "select count(*) from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id" -default 0]

            # Check if the counter already exists for this type
            if {![db_0or1row prefix "select aux_string1 as type_prefix, category from im_categories where category_id = :package_type_id and category_type = 'Intranet Trans Task Type'"]} {
                # Invalid package_type
                ns_log Error "Invalid category for trans tasks $package_type_id"
                return ""
            }
            if {$type_prefix eq ""} {
                db_1row project_name_and_language "select project_name, im_name_from_id(target_language_id) as target_language from im_projects p, im_trans_tasks t where task_id = :p_task_id and p.project_id = t.project_id"
                set type_prefix "${project_name}_${target_language}_$category"
            }

            set exists_p 1
            while {$exists_p > 0 } {
                incr counter
                set package_name "${type_prefix}_$counter"
                set exists_p [db_string package_name_exists "select 1 from im_freelance_packages where freelance_package_name = :package_name and project_id = :project_id" -default 0]
            }
        }
        return $package_name
    }
}