## Copyright (c) 2021, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Init and scheduling
}

if {[parameter::get_from_package_key -package_key "webix-portal" -parameter ScanOverdueAssignments] eq "t"} {
    ad_schedule_proc -thread t 301 webix::notifications::create_overdue_assignments
}

if {[parameter::get_from_package_key -package_key "webix-portal" -parameter ScanOverdueProjects] eq "t"} {
    ad_schedule_proc -thread t 300 webix::notifications::create_overdue_projects
}

if {[parameter::get_from_package_key -package_key "webix-portal" -parameter RemindFreelancersAboutRequestedAssignment] eq "t"} {
    ad_schedule_proc -thread t 302 webix::notifications::create_unanswered_requested_assignments
}