ad_library {
    Rest Procedures for posting the webix-portal package translation section
    @author malte.sussdorff@cognovis.de
}

#---------------------------------------------------------------
# Translation Projects
#---------------------------------------------------------------
namespace eval cog_rest::json_object {
    ad_proc trans_project {} {
        @return project named_id Project to look at 
        @return project_nr string ProjectNR for the project
        @return project_status category "Intranet Project Status" Status_id of the project
        @return project_type category "Intranet Project Type" Type_id of the project
        @return company_project_nr string Customer Reference Number
        @return start_date date-time Date when project starts
        @return end_date date-time Date for the deadline
        @return source_language category "Intranet Translation Languages" source language ID of the project
        @return target_language category_array "Intranet Translation Languages" List of target languagages
        @return subject_area category "Intranet Subject Area" Subject Area ID of the project
        @return project_lead json_object user PM running the project
        @return company_contact named_id Company contact for this project
        @return company_contact_email string E-Mail address for the company contact
        @return processing_time integer Time the project usually takes to finish in days
        @return price number Quote Price for the project
        @return invoice_amount number Invoice Amonut for the project
        @return project_folder object cr_folder Project folder we hopefully have created.
        @return company named_id Company for which we run this project. This is the company being billed
        @return final_company named_id Final customer / company of the project. Usually won't be billed directly.
        @return description string description of the project. Usually Customer facing
    } -

    ad_proc trans_project_body {} {
        @param project_name string Name of the project. Defaults to project_nr if not provided.
        @param project_nr string Number of the project. Mostly automatically determined now though
        @param project_status_id category "Intranet Project Status" Status_id of the project
        @param company_id object im_company::read Company in which to create the project
        @param project_type_id category "Intranet Project Type" Project Type of the project to create. Defaults to trans+edit (87)
        @param source_language_id category "Intranet Translation Language" Source Language for the project. Typically the language the source material is in
        @param target_language_ids category_array "Intranet Translation Language" Language into which this task is translated to. Leaving it empty will use the projects target languages
        @param subject_area_id category "Intranet Translation Subject Area" Subject Area of the project (determines pricing among other things)
        @param final_company_id object im_company::read Final company for this translation (determines the reference material and Termbase / Translation Memory to use)
        @param project_lead_id object person::read Project Manager of this project
        @param customer_contact_id object person::read Contact in the company who requested this project
        @param language_experience_level_id category "Intranet Experience Level" Required experience level for this project. Used (previously) to differentiate certified translations
        @param processing_time number How many days will this project take
        @param company_project_nr string Reference number of the client of this project
        @param project_source_id category "Intranet Project Source" Source of the project. 
        @param start_date date-time Date when project starts
        @param end_date date-time Date for the deadline
        @param do_analysis_p boolean Switch which decided if we should execute MemoQ file analysis. 1 = Yes, 0 = No.
        @param description string description of the project. Usually Customer facing
    } -

    ad_proc trans_task {} {
        @return task named_id Task ID and name - typically the name of the file to be translated
        @return task_units number Units for the task
        @return billable_units number Billable Units for the task
        @return task_uom category "Intranet UoM" Unit of measure for the task
        @return target_language category "Intranet Translation Language" Language into which this task is translated to
        @return task_type category "Intranet Project Type" Type of task (by default the one from the project). You can read the steps involved based of the aux_int1 of the category
        @return task_deadline date-time Deadline until when the CUSTOMER wants to have the task delivered back
    } -

    ad_proc trans_task_body {} {
        @param task_name string Name of the task - typically the name of the file to be translated
        @param task_units number Units for the task (used for freelancer assignments)
        @param billable_units number Billable Units for the task (to be used in quotes & invoices) - defaults to task_units
        @param task_uom_id category "Intranet UoM" Unit of measure for the task
        @param target_language_ids category_array "Intranet Translation Language" Language into which this task is translated to. Leaving it empty will use the projects target languages
        @param task_type_id category "Intranet Project Type" Type of task (by default the one from the project). You can read the steps involved based of the aux_int1 of the category
        @param task_deadline date-time Deadline until when the CUSTOMER wants to have the task delivered back
    } - 

    ad_proc tm_tool {} {
        @return url string URL in which to open the tm_tool
        @return download_only boolean Can we only download a file under this url (instead of going to different server)
        @return errors json_array error Array of errors found (if any)
    } -
}

ad_proc -public cog_rest::get::trans_projects {
    { -project_id ""}
    { -project_nr "" }
    { -project_type_id ""}
    { -project_status_id ""}
    { -company_id ""}
    { -pagination "limit,50,start,0,property,end_date,direction,DESC"}
    { -rest_oid ""}
    -rest_user_id:required
} {
    Return Translation projects
        
    @param project_id object im_project::read Project for which to return the tasks.
    @param project_nr string Project Nr we are looking for
    @param company_id object im_company::read Company whos projects we look for 

    @param project_status_id category_array "Intranet Project Status" Limit to projects of a certain status. Exclude deleted status by default.
    @param project_type_id category_array "Intranet Project Type" Limit to projects of a certain type. We will by default exclude Task/Ticket (100/101)

    @param pagination pagination_object Pagination information

    @return projects json_array trans_project
} {
 
    # Handling of rest_oid
    if {$rest_oid ne "" && $project_id eq ""} {
        set project_id $rest_oid
    }

    set additional_wheres [cog_rest::filter::where_clauses]

    if {$project_type_id eq ""} {
        lappend additional_wheres "project_type_id not in (100,101)"
    }

    # Don't allow projects without source language
    lappend additional_wheres "source_language_id is not null"

    # Permission limitations
    if {![im_user_is_pm_p $rest_user_id]&&![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {

        # Check for company. then display projects of the company
        if {[im_user_is_customer_p $rest_user_id]} {
            set contact_company_ids [db_list company_id_sql "select object_id_one from acs_rels where object_id_two =:rest_user_id and rel_type = 'im_company_employee_rel'"]
            lappend additional_wheres "company_id in ([template::util::tcl_to_sql_list $contact_company_ids])"
        } else {
            # Do not return anything.
            lappend additional_wheres "0 = 1"
        }
    } 


    #---------------------------------------------------------------
    # Build the SQL
    #---------------------------------------------------------------

    set projects_sql "select project_id, project_nr, project_name, project_type_id, project_status_id,
        company_project_nr, source_language_id, subject_area_id, company_id, final_company_id,
        project_lead_id, im_name_from_id(project_lead_id) as project_lead_name, description,
        cost_quotes_cache as price, cost_invoices_cache as invoice_amount,
        company_contact_id, processing_time, to_char(start_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as start_date, to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as end_date
        from im_projects where [join $additional_wheres " and "]"

    append projects_sql " [cog_rest::sort_and_paginate -pagination $pagination]"

    set projects [list]

    db_foreach project $projects_sql {
        set target_language_ids [im_target_language_ids $project_id]
        if {$target_language_ids ne ""} {
            set end_date_formatted [lc_time_fmt $end_date "%d.%m.%Y"]
            
            # Find the company contact
            if {$company_contact_id eq ""} {
                # Set to the primary contact of the company
                set company_contact_id [db_string primary_contact "select primary_contact_id from im_companies where company_id = :company_id" -default ""]
            }

            if {$company_contact_id eq ""} {
                # Default to rest user. Something is really off with this project
                set company_contact_id $rest_user_id
            }                  

            if {$price eq ""} {
                array set costs [im_cost_update_project_cost_cache $project_id]
                set price $costs([im_cost_type_quote])
            }
        
            set company_contact_name [im_name_from_id $company_contact_id]
            set company_contact_email [im_email_from_user_id $company_contact_id]
            set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
            if {$project_folder_id eq ""} {
                set project_folder_id [intranet_fs::create_project_folder -project_id $project_id -user_id $rest_user_id]
            }

            if {$project_lead_id ne ""} {
                set project_lead [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $project_lead_id -rest_user_id $rest_user_id]]
            } else {
                set project_lead ""
            }
            
            lappend projects [cog_rest::json_object]
        }
        
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::trans_project {
    -company_id:required
    -source_language_id:required
    -target_language_ids:required
    { -project_type_id "" }
    { -subject_area_id ""}
    { -final_company_id ""}
    { -customer_contact_id ""}
    { -project_lead_id ""}
    { -project_name ""}
    { -project_nr ""}
    { -processing_time "" }
    { -company_project_nr ""}
    { -project_source_id "" }
    { -start_date ""}
    { -end_date ""}
    -rest_user_id:required
    { -do_analysis_p 0 }
    { -description ""}
} {
    Create a translation project. In comparison to normal project creation this supports source and target languages as well as skills.
    Might be used for "normal" project creation as well though.

    Handler for POST calls on the project.

    For variables starting with "skill_", these will be added as freelancer skills to the project. So are source_language_id, target_language_ids, subject_area_id and
    language_experience_level_id (the level for the skills in languages).

    @param trans_project_body request_body Single project which can be created

    @return project json_object trans_project Translation Project we just created
} {

    if {$project_lead_id eq ""} {
        set project_lead_id $rest_user_id
    }
        
    set project_id [webix::trans::project::new \
            -company_id $company_id \
            -project_name $project_name \
            -project_nr $project_nr \
            -processing_time $processing_time \
            -project_type_id $project_type_id \
            -project_lead_id $project_lead_id \
            -source_language_id $source_language_id \
            -target_language_ids $target_language_ids \
            -subject_area_id $subject_area_id \
            -final_company_id $final_company_id \
            -project_source_id $project_source_id \
            -creation_user $rest_user_id \
            -customer_contact_id $customer_contact_id \
            -description $description]

    ns_log Notice "Project created... $project_id"
    
    if {$project_id eq 0} {
       # There was  an error creating the project, try to find out if it is because it was a duplicate
       set project_id [db_string project_id "select project_id from im_projects where
            (   upper(trim(project_name)) = upper(trim(:project_name)) OR
                upper(trim(project_nr)) = upper(trim(:project_nr)) OR
                upper(trim(project_path)) = upper(trim(:project_name))
            )" -default 0]
       
        if {$project_id eq 0} {
            cog_rest::error -http_status 400 -message "Failed to create project. We sadly don't know why"
        } else {
            cog_rest::error -http_status 400 -message "Found project already existing"
        }

    } 


    # Calculate the end date
    set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
    im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp

    # Grant permission to company contact
    if {$customer_contact_id ne ""} {
        permission::grant -party_id $customer_contact_id -object_id $project_id -privilege "write"
    }

    #---------------------------------------------------------------
    # Return the created project as it is in the DB
    #---------------------------------------------------------------
    set project [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::trans_projects -project_id $project_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}



ad_proc -public cog_rest::put::trans_project {
    -project_id:required
    -company_id
    -source_language_id
    { -target_language_ids "" }
    -project_type_id
    -project_status_id
    -subject_area_id
    -final_company_id
    -customer_contact_id
    -project_name
    -project_nr
    -processing_time
    -company_project_nr
    -project_source_id
    -start_date
    -end_date
    -description
    -rest_user_id:required
    { -do_analysis_p 0 }
} {
    Modify a translation project. In comparison to normal project creation this supports source and target languages as well as skills.
    Might be used for "normal" project creation as well though.

    Handler for PUT calls on the project.

    For variables starting with "skill_", these will be added as freelancer skills to the project. So are source_language_id, target_language_ids, subject_area_id and
    language_experience_level_id (the level for the skills in languages).

    @param trans_project_body request_body Single project which can be created
    @param project_id object im_project::write Project which we want to update

    @return project json_object trans_project Translation Project we just created
} {

    set old_project_status_id [db_string previous_status "select project_status_id from im_projects where project_id = :project_id"]

    set sql "update im_projects set"

    set vars_to_retrieve [list]
    foreach mandatory_var [list final_company_id description company_contact_id  processing_time company_project_nr start_date end_date company_id project_type_id project_status_id project_name project_nr subject_area_id source_language_id] {
        if {[exists_and_not_null $mandatory_var]} {
            append sql " $mandatory_var=:$mandatory_var,\n"
        } else {
            lappend vars_to_retrieve $mandatory_var
        }
    }
        
    append sql " project_id=:project_id where project_id=:project_id"
    
    db_dml update_im_projects $sql
    
    if {$target_language_ids ne ""} {
        db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
        foreach target_language_id $target_language_ids {
            db_dml insert_im_target_language "insert into im_target_languages values ($project_id, $target_language_id)"
        }
    } else {
        set target_language_ids [im_target_language_ids $project_id]
    }

    #---------------------------------------------------------------
    # Retrieve vars for the skills
    #---------------------------------------------------------------
    db_1row retrieve_vars "select [join $vars_to_retrieve ","] from im_projects where project_id = :project_id"

    db_dml delete_exising_skills "delete from im_object_freelance_skill_map where object_id =:project_id"
    
    im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
    im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
    foreach target_language_id $target_language_ids {
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $target_language_id
    }

    # Calculate the end date
    set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
    im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp

    callback webix::trans::project::after_update -project_id $project_id -user_id $rest_user_id -project_status_id $project_status_id -old_project_status_id $old_project_status_id

    
    #---------------------------------------------------------------
    # Return the created project as it is in the DB
    #---------------------------------------------------------------
    set project [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::trans_projects -project_id $project_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::trans_project {
    -project_id:required
    -rest_user_id:required
} {
    Delete a project from the Database. Can only be done by admins

    @param project_id object im_project::read Project we want to delete.
    @return errors json_array error Array of errors found
} {
    set errors [list]
    set task_ids [im_project_subproject_ids -project_id $project_id -type "task"]
    set project_ids [im_project_subproject_ids -project_id $project_id]
    foreach pid [concat $task_ids $project_ids] {
        im_project_permissions $rest_user_id $pid view read write admin
        set object_id $pid
        if {!$admin} {
            set err_msg "You need to have administration rights for the project."
            set parameter "[im_name_from_id $pid] - $pid"
            lappend errors [cog_rest::json_object]
        } else {
            # delete the packages first
            foreach package_id [db_list package_id "select freelance_package_id from im_freelance_packages where project_id = :pid"] {
                foreach assignment_id [db_list assignments "select assignment_id from im_freelance_assignments where freelance_package_id = :package_id"] {
                    db_string delete_assignment "select im_freelance_assignment__delete(:assignment_id) from dual"
                }
                db_string delete_package "select im_freelance_package__delete(:package_id) from dual"                
            }
            
            # Delete trans_price_history

            set result [im_project_nuke -current_user_id $rest_user_id $pid ]
            if {$result ne ""} {
                set err_msg "$result"
                set parameter "[im_name_from_id $pid] - $pid"
                lappend errors [cog_rest::json_object]
            }
        }
    }
    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Translation Tasks
#---------------------------------------------------------------

ad_proc -public cog_rest::get::trans_tasks {
    {-project_id ""}
    {-task_id ""}
    { -task_name "" }
    -rest_user_id
    {-rest_oid ""}
} {
    Provide the trans tasks from ]project-open[ for a specific project

    @param project_id object im_project::read Project for which to return the tasks.
    @param task_id object im_trans_task::read Task which we want to get
    @param task_name string name of the task to delete in the project

    @return trans_tasks json_array trans_task Translation task to be returned
} {

    # Handling of rest_oid
    if {$rest_oid ne "" && $task_id eq ""} {
        set task_id $rest_oid
    }
    
    if {$task_id ne ""} {
        set existing_project_tasks_sql "select task_id, task_name, task_units, task_uom_id, target_language_id,task_type_id, to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as task_deadline, billable_units from im_trans_tasks where task_id = :task_id"
    } else {
        set existing_project_tasks_sql "select task_id, task_name, task_units, task_uom_id, target_language_id,task_type_id, to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as task_deadline, billable_units from im_trans_tasks where project_id =:project_id and task_status_id <> 372"
        if {$task_name ne ""} {
            append existing_project_tasks_sql " and task_name = :task_name"
        }
    }

    set trans_tasks [list]

    # Check if we have the correct project_id
    set project_exists_p [db_string project_exists "select 1 from im_projects where project_id = :project_id" -default 0]
    if {!$project_exists_p} {
        return [cog_rest::error -http_status 400 -message "Project does not exist"]
    }

    # Check permissions
    im_project_permissions $rest_user_id $project_id view read write admin
    if {!$view} {
        return [cog_rest::error -http_status 403 -message "User is not allowed to see the project"]
    }

    db_foreach existing_tasks $existing_project_tasks_sql {
        lappend trans_tasks [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::trans_tasks {
    -project_id:required
    -task_name:required
    { -target_language_ids ""}
    -task_type_id:required
    { -task_units "" }
    { -billable_units "" }
    -task_uom_id:required
    { -task_deadline ""}
    -rest_user_id:required
} {
    Create a new trans task for the provided project_id

    @param project_id object im_project::write Project in which to create the task
    @param trans_task_body request_body Single trans task which can be created

    @return trans_tasks json_array trans_task Task which were created

} {

    set trans_tasks [list]
    if {$target_language_ids eq ""} {
        # getting all possible project target languages ids
        set target_language_ids [im_target_language_ids $project_id]
    } 

    set new_task_ids [webix::trans::task_new \
        -project_id $project_id \
        -task_name $task_name \
        -task_units $task_units \
        -task_uom_id $task_uom_id \
        -task_type_id $task_type_id \
        -target_language_ids $target_language_ids \
        -billable_units $billable_units \
        -end_date $task_deadline \
        -user_id $rest_user_id]

    callback webix_trans_task_after_create -trans_task_ids $new_task_ids -project_id $project_id -user_id $rest_user_id

    set trans_task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $new_task_ids]
    foreach package_type_id $trans_task_type_ids {
        set freelance_package_id [db_string unassigned_package "select min(freelance_package_id) 
            from im_freelance_packages fp
            where fp.package_type_id = :package_type_id
            and fp.project_id = :project_id
            and freelance_package_id not in (select fp.freelance_package_id from im_freelance_assignments fa, im_freelance_packages fp where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id)
            " -default ""]
                        
        if {$freelance_package_id eq ""} {
            webix::packages::create -trans_task_ids $new_task_ids -package_type_id $package_type_id -user_id $rest_user_id
        } else {
            foreach trans_task_id $new_task_ids {
                db_dml insert_task "insert into im_freelance_packages_trans_tasks (freelance_package_id, trans_task_id) values (:freelance_package_id, :trans_task_id)"
            }
        }
    }

    foreach task_id $new_task_ids {
        db_1row task_info "select task_id, task_name, task_units, task_uom_id, target_language_id,task_type_id, to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as task_deadline, billable_units from im_trans_tasks where task_id = :task_id"

        lappend trans_tasks [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::trans_tasks {
    -task_id:required
    -task_name:required
    {-target_language_ids ""}
    -task_type_id:required
    { -task_units "" }
    { -billable_units "" }
    -task_uom_id:required
    { -task_deadline ""}
    -rest_user_id:required
} {
    Create a new trans task for the provided project_id

    @param task_id object im_trans_task::write TaskID we want to overwrite (if provided)
    @param trans_task_body request_body Single trans task which can be created

    @return trans_tasks json_object trans_task Task which was updated
} {

    set trans_tasks [list]


    # Check if we can update the task
    if {[webix::trans::task_removeable_p -task_id $task_id]} {
        if {$target_language_ids eq ""} {
            set target_language_id [db_string target_language_id "select target_language_id from im_trans_tasks where task_id = :task_id"]
        } else {
            set target_language_id [lindex $target_language_ids 0]
        }

        webix::trans::task_update \
            -task_id $task_id \
            -task_name $task_name \
            -task_units $task_units \
            -task_uom_id $task_uom_id \
            -task_type_id $task_type_id \
            -target_language_id $target_language_id \
            -billable_units $billable_units \
            -end_date $task_deadline

        callback webix_trans_task_after_update -trans_task_id $task_id
    } else {
        # Name can always be edited if not duplicate for the target language
        # Get the target language from the current task, not the form
        set target_language_id [db_string target_language_id "select target_language_id from im_trans_tasks where task_id = :task_id"]
        if {![webix::trans::task_name_exists_p -task_id $task_id -task_name $task_name -target_language_id $target_language_id]} {
            db_dml update_task_name "update im_trans_tasks set task_name = :task_name where task_id = :task_id"
        }

        # Deadline can be changed if  the last assignment deadline for the task is earlier than the new one - Otherwise you get an error. 
        if {$task_deadline ne ""} {
            set deadline_update_p [db_string deadline "select 1 from dual where :task_deadline > (select max(end_date) from im_freelance_assignments fa, im_freelance_packages_trans_tasks fptt where fptt.freelance_package_id = fa.freelance_package_id and fptt.trans_task_id = :task_id)" -default 0]
            if {!$deadline_update_p} {
                cog_rest::error -http_status 400 -message "Deadline is before a freelancer assignment's deadline for this task. Please ammend the freelancer assignments first if you want to change the deadline"
            } else {
                db_dml update_deadline "update im_trans_tasks set end_date = :task_deadline where task_id = :task_id"
            }
        }
    }

    db_1row task_info "select task_id, task_name, task_units, task_uom_id, target_language_id,task_type_id, to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as task_deadline, billable_units from im_trans_tasks where task_id = :task_id"

    lappend trans_tasks [cog_rest::json_object]

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::trans_tasks {
    -task_ids:required
} {
    Delete the provided task ids if it is possible to remove them

    Will only remove all or none. If there are no errors, then the removal was successful.

    @param task_ids object_array im_trans_task::write Trans Task we are trying to delete

    @return errors json_array error Array of errors found
} {
    set errors [list]

    foreach task_id $task_ids {
        # Check if we have some Assignment
        if {![webix::trans::task_removeable_p -task_id $task_id]} {
            set err_msg "We have at least one existing assignment for the task. Please delete those first"
            set parameter "[im_name_from_id $task_id] - $task_id"
            set object_id $task_id
            lappend errors [cog_rest::json_object]
        }
    }

    if {[llength $errors] eq 0} {
        # No error, save to delete
        foreach task_id $task_ids {
            db_exec_plsql delete_trans_task {
                select im_trans_task__delete(:task_id);
            }
        }
    }

    return [cog_rest::json_response]
}


#---------------------------------------------------------------
# Translation GET endpoints
#---------------------------------------------------------------

namespace eval cog_rest::get {


    ad_proc -public suggested_task_names {
        { -task_name_stub ""}
        -rest_user_id:required
    } {
        Handler for GET rest calls which return list of previously used task_names

        @param task_name_stub string first three letters of task_name

        @return suggested_task_names json_array array of task_names
        @return_suggested_task_names task object im_trans_task task name
        
    } {

        set suggested_task_names [list]

        set name_filter ""
        if {$task_name_stub ne ""} {
            set name_filter "AND lower(tt.task_name) like lower('%$task_name_stub%')"
        }

        set suggested_task_names_sql "
            SELECT task_id, task_name
            FROM im_trans_tasks tt, acs_objects obj
            WHERE tt.task_id = obj.object_id
            $name_filter
            AND obj.creation_user = :rest_user_id
            AND obj.object_type = 'im_trans_task'
            AND obj.creation_date > current_date - interval '6 months'
        "

        db_foreach suggested_task_name $suggested_task_names_sql {
            lappend suggested_task_names [cog_rest::json_object]
        }

        return [cog_rest::json_response]
    }

   ad_proc -public not_removable_trans_tasks {
        -trans_tasks
        -rest_user_id
    } {
        Return not removable tasks for given project_id.
        Task can be removed if has not yet been assigned

        @param trans_tasks object_array im_trans_tasks::read Object ID of translation tasks which we check if they are removable
        
        @return not_removable_trans_tasks json_array trans_task Translation task that are not removable
    } {

        set not_removable_task_ids [list]

        foreach task_id $trans_tasks  {
            if {![webix::trans::task_removeable_p -task_id $task_id]} {
                lappend not_removable_task_ids $task_id
            }
        }
        
        set not_removable_sql "select distinct tt.task_id, tt.task_name from im_trans_tasks tt
            where tt.task_id in ([template::util::tcl_to_sql_list $not_removable_task_ids])"

        db_foreach not_removable $not_removable_sql {
            lappend not_removable_trans_tasks [cog_rest::json_object]
        }

        return [cog_rest::json_response]

    }

    ad_proc -public tm_tool_url {
        -project_id:required
        -tm_tool_id:required
    } {
        Returns the URL to open the project in the TM Tool - Might just provide the download of a project folder for this project

        @param project_id object im_project::read Project for which we want the TM Tool link
        @param tm_tool_id category "Intranet TM Tool" TM Tool for which we want to get the link

        @return tm_tools json_object tm_tool Information about the TM Tool
    } {
        set errors [list]
        set download_only false
        set url ""

        set project_type_id [db_string project_type "select project_type_id from im_projects where project_id = :project_id" -default ""]
        
    	if {[lsearch [im_sub_categories [im_project_type_translation]] $project_type_id]<0} {
            set err_msg "[im_name_from_id $project_id] is not a translation project."
            set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
            set object_id $project_id
            lappend errors [cog_rest::json_object -object_class "error"]	
    	} else {

            switch $tm_tool_id {
                10000534 - 10000535 - 10000763 - 10000379 {
                    # Trados
                    db_1row project_info "select p.project_nr, c.company_path
                        from im_projects p, im_companies c
                        where p.project_id=:project_id and p.company_id=c.company_id"

                    set project_dir [im_filestorage_project_path $project_id]
#                	if {![file exists ${project_dir}/Trados]} {
			            # Seems it is not setup. Create it now
            			im_trans_trados_setup_directory -project_id $project_id
 #               	}

                    if {[im_trans_trados_project_p -project_id $project_id]} {
                        set url "file:///T:/Projekte/${company_path}/${project_nr}/Trados/${project_nr}.sdlproj"
                    } else {
                        set err_msg "$project_nr is not a Trados project. Can't help"
                        set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
                        set object_id $project_id
                        lappend errors [cog_rest::json_object -object_class "error"]	
                    }
                }
                10000308 {
                    # MemoQ
                    if {[im_trans_memoq_configured_p]} {
                        set memoq_guid [db_string memoq "select memoq_guid from im_projects where project_id = :project_id" -default ""] 
                        if {$memoq_guid eq ""} {
                           set memoq_guid [im_trans_memoq_create_project -project_id $project_id]
                           im_trans_memoq_file_upload_source_files -project_id $project_id
                           im_trans_memoq_project_analysis -project_id $project_id
                        } 
                        set memoq_server [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQServer"]
                        set memoq_port [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQPort"]
                        set url [export_vars -base "${memoq_server}:${memoq_port}/memoq/pm/projectoverview/index/$memoq_guid" -url {{nocache True}}]
                    } else {
                        set err_msg "MemoQ is not configured, can't help you"
                        set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
                        set object_id $project_id
                        lappend errors [cog_rest::json_object -object_class "error"]	
                    }
                }
                default {
                    set err_msg "We don't support your tm tool [im_name_from_id $tm_tool_id] yet"
                    set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
                    set object_id $project_id
                    lappend errors [cog_rest::json_object -object_class "error"]	
                }
            }
        }
        set tm_tools [cog_rest::json_object]
        return [cog_rest::json_response]
    }
}


#---------------------------------------------------------------
# POST Endpoints (for storing)
#---------------------------------------------------------------

namespace eval cog_rest::post {

    ad_proc -public trans_tasks_from_file {
        -filename:required
        -path:required
        -project_id:required
        -rest_user_id
    } {
        Creates tasks from single uploaded CSV/XLS file

        @param filename string Name of the file (original filename)
        @param path string Path to the file as returned by upload_fs_file endpoint
        @param project_id object im_project::write Project in which to create the task - needs write permission

        @return trans_tasks json_array trans_task Task which were created
    } {

        set trans_tasks [list]
        set new_task_ids [list]

        set task_csv [new_CkCsv]
        CkCsv_put_HasColumnNames $task_csv 1

        # In here we previosly had a loop, but now we upload only single file so it wasn't needed anymore
        # We still might have loop here in future

        # Extract file exetension and proceed only if its not empty
        set extension [file extension $filename]
        if {$extension ne ""} {
           switch $extension {
                ".xls" - ".xlsx" {
                    # Konvert the XLS to CSV for impport
                    set xls_filename "[file rootname $path]$extension"
                    file rename $path $xls_filename
                    set csv_file [intranet_oo::convert_to -oo_file $xls_filename -convert_to "csv"]
                    file delete $xls_filename
                }
                ".csv" {
                    set csv_file $path
                }
            }

            set success [CkCsv_LoadFile $task_csv $csv_file]
            if {$success != 1} then {
                ns_log Error [CkCsv_lastErrorText $task_csv]
                delete_CkCsv $task_csv
                cog_rest::error -http_status 400 -message "Could not load the provided file $filename for Parsing"
                return
            }
 
            set n [CkCsv_get_NumRows $task_csv]

            # Check the column names
            set numCols [CkCsv_get_NumColumns $task_csv]
            set column_names [list]
            for {set i 0} {$i < $numCols} {incr i} {
                set column_name [CkCsv_getColumnName $task_csv $i]
                lappend column_names $column_name
            }

            # Check we have the mandatory columns
            foreach mandatory_column [list task_name task_units task_uom] {
                if {[lsearch $column_names $mandatory_column]<0} {
                    cog_rest::error -http_status 400 -message "Mandatory column $mandatory_column missing in CSV"
                }
            }

            set project_target_language_ids [im_target_language_ids $project_id]
            set project_task_type_id [db_string project_task_type "select project_type_id from im_projects where project_id = :project_id"]

            # Get a list of batch names to use later for batch generation
            set batch_names [list]

            for {set row 0} {$row <= [expr $n - 1]} {incr row} {
                set task_name [CkCsv_getCellByName $task_csv $row "task_name"]
                if {[string trim $task_name] eq ""} {
                    continue
                }

                set task_uom [CkCsv_getCellByName $task_csv $row "task_uom"]
                set task_uom_id [im_category_from_category -category $task_uom]
                if {$task_uom_id eq ""} {
                    set task_uom_id [im_category_from_category -category "S-Word"]
                }

                set task_units [CkCsv_getCellByName $task_csv $row "task_units"]

                set target_language [CkCsv_getCellByName $task_csv $row "target_language"]
                if {$target_language eq ""} {
                    set target_language_ids $project_target_language_ids
                } else {
                    set target_language_ids [im_category_from_category -category "$target_language"]
                }

                set task_type [CkCsv_getCellByName $task_csv $row "task_type"]
                if {$task_type eq ""} {
                    set task_type_id $project_task_type_id
                } else {
                    set task_type_id [im_category_from_category -category "$task_type"]
                }            

                set task_deadline [CkCsv_getCellByName $task_csv $row "task_deadline"]


                set billable_units $task_units
                set new_task_ids [webix::trans::task_new \
                    -project_id $project_id \
                    -task_name $task_name \
                    -task_units $task_units \
                    -task_uom_id $task_uom_id \
                    -task_type_id $task_type_id \
                    -target_language_ids $target_language_ids \
                    -billable_units $billable_units \
                    -user_id $rest_user_id \
                    -end_date $task_deadline]

                set batch_name [CkCsv_getCellByName $task_csv $row "batch_name"]
                if {$batch_name eq ""} {
                    set batch_name "default"
                } 

                if {[lsearch $batch_names $batch_name]<0} {
                    lappend batch_names $batch_name
                }
                
                if {[info exists tasks_in_batch($batch_name)]} {
                    set tasks_in_batch($batch_name) [list {*}$tasks_in_batch($batch_name) {*}$new_task_ids]
                } else {
                    set tasks_in_batch($batch_name) $new_task_ids
                }
                callback webix_trans_task_after_create -trans_task_ids $new_task_ids -project_id $project_id
                
            }
        }

        if {[llength $new_task_ids] > 0} {

            # Create packages for the batches
            foreach package_name $batch_names {
                if {$package_name eq "default"} {
                    webix::packages::create -trans_task_ids $tasks_in_batch($package_name) -user_id $rest_user_id
                } else {
                    webix::packages::create -trans_task_ids $tasks_in_batch($package_name) -package_name $package_name -user_id $rest_user_id
                }
            }
            
            # Prepare the return object
            foreach new_task_id $new_task_ids {
                db_1row get_single_tasK_data "select * from im_trans_tasks where task_id =:new_task_id"

                lappend trans_tasks [cog_rest::json_object]
            }
        }

        return [cog_rest::json_response]
    }

    ad_proc -public trans_tasks_from_trados {
        -project_id:required
        { -only_new "0"}
        -rest_user_id:required
    } {
        Uploads the files from the analysis in the "Trados/Reports" folder of the project

        @param project_id object im_project::write Project in which to create the task - needs write permission
        @param only_new boolean Include only new tasks, do not overwrite existing ones

        @return trans_tasks json_array trans_task Tasks which were created
    } {
        # ---------------------------------------------------------------
        # Analyse the trados XML File and create the tasks
        # ---------------------------------------------------------------
        set project_dir [im_filestorage_project_path $project_id]

        # ---------------------------------------------------------------
        # Load the results fo the analysis
        # ---------------------------------------------------------------

        db_1row project_info "select project_name, source_language_id from im_projects where project_id = :project_id"
        set source_language [im_name_from_id $source_language_id]

        set created_task_ids [list]

        # Loop through all the target langauges and try to find the wordcount file.
        foreach target_language [im_target_languages $project_id] {

            # Get the latest version of the wordcount analysis
            set wc_xml_file ""
            set modification_date ""

            if {[catch {glob -directory "${project_dir}/Trados/Reports" *${source_language}_${target_language}*} wc_files]} {
                set wc_files [list]
            }

            foreach wc_file $wc_files {
                set mod [file mtime $wc_file]
                if {$mod>$modification_date} {
                    set modification_date $mod
                    set wc_xml_file $wc_file
                }
            }

            if {$wc_xml_file ne ""} {
                set target_language_id [cog_category_id -category $target_language -category_type "Intranet Translation Language"]
                set wc_task_ids [webix::trans::trados_tasks_new \
                    -project_id $project_id \
                    -trados_analysis_xml $wc_xml_file \
                    -target_language_id $target_language_id \
                    -user_id $rest_user_id \
                    -only_new_p $only_new]
                set created_task_ids [concat $created_task_ids $wc_task_ids]

            }
        } 

        #---------------------------------------------------------------
        # Return the task information
        #--------------------------------------------------------------- 

        foreach task_id $created_task_ids {
            db_1row task_info "select task_id, task_name, task_units, task_uom_id, target_language_id,task_type_id, to_char(end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as task_deadline, billable_units from im_trans_tasks where task_id = :task_id"
            
            lappend trans_tasks [cog_rest::json_object]
        }
        
        return [cog_rest::json_response]
    }

    ad_proc -public trans_quote {
        -trans_tasks:required
        -rest_user_id:required
        { -quote_per_language_p 0 }
    } {

        @param trans_quote request_body Task information for creating or updating the quote
        @param_trans_quote trans_tasks object_array im_trans_task::read Task Id for which we want to create the tasks.
        @param_trans_quote quote_per_language_p boolean Should we create the quote per language? defaults to no.

        @return quotes json_array invoice Array of newly created quotes
    } {

        set quotes [list]

        set quote_ids [list]
        if {$quote_per_language_p} {
            # Get the distinct target languages
            set target_language_ids [db_list target_language_ids_from_tasks "select distinct target_language_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $trans_tasks])"]

            foreach target_language_id $target_language_ids {
                set trans_task_ids [db_list target_language_ids_from_tasks "select distinct task_id from im_trans_tasks where target_language_id = :target_language_id and task_id in ([template::util::tcl_to_sql_list $trans_tasks])"]
                set quote_id [im_trans_invoice_create_from_tasks -task_ids $trans_task_ids -current_user_id $rest_user_id]
                if {$quote_id ne ""} {
                    lappend quote_ids $quote_id
                }
            }
        } else {
            # Generate the quote for the task across all languages
            set quote_id [im_trans_invoice_create_from_tasks -task_ids $trans_tasks -current_user_id $rest_user_id]            
            if {$quote_id ne ""} {
                lappend quote_ids $quote_id
            }
        }
        
        if {$quote_ids ne ""} {
            foreach quote_id $quote_ids {
                cog::invoice::update_rounded_amount -invoice_id $quote_id
            }
            return [cog_rest::get::invoice -invoice_ids $quote_ids -rest_user_id $rest_user_id]
        } else {
            cog_rest::error -http_status 500 -message "Could not created quotes for the trans_tasks"
        }
    }
}

#---------------------------------------------------------------
# PUT Endpoints to update
#---------------------------------------------------------------


namespace eval cog_rest::put {

    ad_proc -public trans_quote {
        -quote_id:required
        -trans_tasks:required
        -rest_user_id:required
    } {

        Overwrite an existing quote with the tasks

        @param quote_id object im_cost::write Quote for which we will update the line items
        
        @param trans_quote request_body Task information for creating or updating the quote
        @param_trans_quote trans_tasks object_array im_trans_task::read Task Id for which we want to create the tasks.

        @return quotes json_array invoice Array with a single object with the newly created quote

    } {

        set new_quote_id [im_trans_invoice_create_from_tasks -invoice_id $quote_id -task_ids $trans_tasks -current_user_id $rest_user_id]
        cog::invoice::update_rounded_amount -invoice_id $new_quote_id

        return [cog_rest::get::invoices -invoice_ids $new_quote_id -rest_user_id $rest_user_id]
    }
}

#---------------------------------------------------------------
# Helper procs for translation
#---------------------------------------------------------------

namespace eval webix::trans {

    ad_proc project_url {
        -project_id:required
    } {
        Returns the deep url to directly access a project

        @param project_id Id of the project we want to reach
    } {
        set portal_url [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
        return [export_vars -base "${portal_url}/#!/project-info" -url {project_id}]
    }

    ad_proc quality_rating {
        -freelancer_id:required
        {-assignment_id ""}
        {-subject_area_id ""}
        {-quality_type_id ""}
    } {
        Returns the quality rating for a freelancer

        @param freelancer_id Freelancer in whos quality we are interested
        @param assignment_id If we only want the rating for a specific assignment
        @param subject_area_id All ratings for this subject_area
        @param quality_type_id All ratings for a specific quality type
    } {
        set query_sql "select round(avg(c.aux_int1),2) 
            from im_categories c, im_assignment_quality_reports r, im_assignment_quality_report_ratings rr, im_freelance_assignments f 
            where rr.report_id = r.report_id 
                and c.category_id = rr.quality_level_id 
                and r.assignment_id = f.assignment_id 
                and f.assignee_id = :freelancer_id"

        if {$assignment_id ne ""} {
            append query_sql " and f.assignment_id = :assignment_id"
        }

        if {$subject_area_id ne ""} {
            append query_sql " and r.subject_area_id = :subject_area_id"
        }

        if {$quality_type_id ne ""} {
            append query_sql " and r.quality_type_id = :quality_type_id"
        }

        set rating [db_string rating $query_sql -default ""]
    }

    ad_proc -public project_valid_language_ids {
        -project_id:required
        { -language_ids "" }
        { -type "source" }
    } {
        Returns the list of filtered Languages from the given language_ids checked against valid ones for the project

        Also checks for parent language groups and dialects (so fuzzy match)

        @param project_id Project for which we want to check languages
        @param language_ids List of languages which we want to check if they are valid for the project
        @param type Type of language we check against, might be "source" or "target"
    } {
        switch $type {
            "source" {
                db_1row source_langs "select  source_language_id, ch.parent_id
                    from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
                    where   project_id = :project_id"

                if {[exists_and_not_null parent_id]} {
                    set project_language_ids [im_sub_categories $parent_id]
                } else {
                    set project_language_ids [im_sub_categories $source_language_id]
                }
            }
            "target" {
                set project_language_ids [list]
                db_foreach target_langs "
                        select  language_id as target_language_id, parent_id
                        from	im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
                        where	project_id = :project_id
                " {
                    if {[exists_and_not_null parent_id]} {
                        set target_ids [im_sub_categories $parent_id]
                    } else {
                        set target_ids [im_sub_categories $target_language_id]
                    }
                    foreach target_id $target_ids {
                        if {[lsearch $project_language_ids $target_id]<0} {
                            lappend project_language_ids $target_id
                        }
                    }
                }
            }
        }


        if {$language_ids ne ""} {
            # Check that they are valid
            foreach language_id $language_ids {
                if {[lsearch $project_language_ids $language_id]<0} {
                    set language_ids [lsearch -inline -all -not -exact $language_ids $language_id]
                }
            }
        } else {
            set language_ids $project_language_ids
        }

        return [im_sub_categories $language_ids]
    }

    ad_proc -public main_translator_ids {
        -project_id:required
    } {
        Return the  list of main translators for a specific project. 

        Will look at final_company before company_id for the customer

        @param project_id Project for which we look up main translators

        @return main_translator_ids List of main translators for the customer.
    } {
    	return [util_memoize [list webix::trans::main_translator_ids_helper -project_id $project_id] 600]
    }

    ad_proc -public main_translator_ids_helper {
        -project_id:required
    } {
        Return the  list of main translators for a specific project. 

        Will look at final_company before company_id for the customer

        @param project_id Project for which we look up main translators

        @return main_translator_ids List of main translators for the customer.
    } {
        set source_language_ids [webix::trans::project_valid_language_ids -project_id $project_id]
        set target_language_ids [webix::trans::project_valid_language_ids -project_id $project_id -type "target"]
        set customer_id [db_string final_company_or_customer "select coalesce(final_company_id,company_id) from im_projects where project_id = :project_id"]

        set main_translator_ids [db_list main_translator_ids "select distinct freelancer_id 
            from im_trans_main_freelancer 
            where customer_id = :customer_id 
            and source_language_id in ([template::util::tcl_to_sql_list $source_language_ids]) 
            and target_language_id in ([template::util::tcl_to_sql_list $target_language_ids])"]

        return $main_translator_ids
    }

    ad_proc -public task_new {
        -project_id:required
        -task_name:required
        {-task_units ""}
        {-billable_units ""}
        -task_uom_id:required
        -task_type_id:required
        -target_language_ids:required
        {-user_id ""}
        {-end_date ""}
    } {
        @param project_id Project in which we create the tasks
        @param task_name Name of the task to create (will duplicate per target language)
        @param task_uom_id category_id for the unit of measure
        @param task_type_id category_id for the task, derived from Intranet Project Types
        @param user_id User generating the task, defaults to currently logged in user
        @param task_units Number of units for the task
        @param billable_units Number of billable units of the task
        @param end_date Task end_date

        @return List of created task_ids
    } {

        # Check user
        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }
    
        # Clean up target_language_ids to make them unique
        set target_language_ids [lsearch -all -inline -not -exact [lsort -unique $target_language_ids] {}]
        
        set source_language_id [db_string source_language_id "select source_language_id from im_projects where project_id = :project_id"]
        
        # Created status id
        set task_status_id 340
        set ip_address [ad_conn peeraddr]        

        # Set default for billable units
        if {$billable_units eq ""} {set billable_units $task_units}
        set task_ids [list]
        if {[llength $target_language_ids] <1} {
            ns_log Notice "$project_id does not have target Languages"
        }
        # Add a new task for every project target language
        foreach target_language_id $target_language_ids {
            # Check for duplicated task names
            if {![webix::trans::task_name_exists_p -project_id $project_id -task_name $task_name -target_language_id $target_language_id]} {
                # Keep track of the created tasks
                set new_task_id [im_exec_dml new_task "im_trans_task__new (
                    null,			-- task_id
                    'im_trans_task',	-- object_type
                    now(),			-- creation_date
                    :user_id,		-- creation_user
                    :ip_address,		-- creation_ip
                    null,			-- context_id
            
                    :project_id,		-- project_id
                    :task_type_id,		-- task_type_id
                    :task_status_id,	-- task_status_id
                    :source_language_id,	-- source_language_id
                    :target_language_id,	-- target_language_id
                    :task_uom_id		-- task_uom_id
                    
                    )"]

                db_dml update_task "
                    UPDATE im_trans_tasks SET
                        task_name = :task_name,
                        task_units = :task_units,
                        billable_units = :billable_units,
                        end_date = :end_date
                    WHERE
                        task_id = :new_task_id
                "
            
                # Successfully created translation task
                # Call user_exit to let TM know about the event
                im_user_exit_call trans_task_create $new_task_id
                im_audit -object_type "im_trans_task" -action after_create -object_id $new_task_id -status_id $task_status_id -type_id $task_type_id
            } else {
                set new_task_id [db_string get_existing_task "select task_id
                from im_trans_tasks
                where
                    lower(task_name) = lower(:task_name)
                    and project_id = :project_id
                    and target_language_id = :target_language_id" ]
            }
            lappend task_ids $new_task_id
        }

        return $task_ids        
    }

    ad_proc -public task_update {
        -task_id:required
        -task_name:required
        {-task_units ""}
        {-billable_units ""}
        -task_uom_id:required
        -task_type_id:required
        -target_language_id:required
        {-end_date ""}
    } {
        @param task_id Task which we want to update
        @param task_name Name of the task to create (will duplicate per target language)
        @param task_uom_id category_id for the unit of measure
        @param task_type_id category_id for the task, derived from Intranet Project Types
        @param target_language_id category for the target language
        @param task_units Number of units for the task
        @param billable_units Number of billable units of the task
        @param end_date Task end_date

        @return List of created task_ids
    } {

        # Set default for billable units
        if {$billable_units eq ""} {set billable_units $task_units}

        # Check for duplicated task names
        if {[webix::trans::task_name_exists_p -task_id $task_id -task_name $task_name -target_language_id $target_language_id]} {
            cog_rest::error -http_status 400 -message "[_ intranet-translation.Database_Error] - [_ intranet-translation.lt_Did_you_enter_the_sam]"
        }
        
        db_dml update_task "
            UPDATE im_trans_tasks SET
                task_name = :task_name,
                task_units = :task_units,
                task_type_id = :task_type_id,
                task_uom_id = :task_uom_id,
                target_language_id = :target_language_id,
                billable_units = :billable_units,
                end_date = :end_date
            WHERE
                task_id = :task_id
        "
    
        im_audit -object_type "im_trans_task" -action after_update -object_id $task_id
        
        return $task_id
        
    }

    ad_proc -public task_removeable_p {
        -task_id:required
    } {
        Procedure to determine if a task can be delete.

        You can't delete a task if it has active assignments

        @param task_id Id of the task

        @return boolean 0/1 depending if the task can be deleted
    } {

        # Check if the task is in packages at all
        set task_in_package_p [db_string package_p "select 1 from im_freelance_packages_trans_tasks where trans_task_id = :task_id limit 1" -default 0]

        if {$task_in_package_p} {
            # Assignments that are not Denied, Assignment Deleted, Assignment Closed
            set deleted_assignment_status_ids [list 4223 4230 4231]

            set task_not_removeable_p [db_string removable "select 1 from im_trans_tasks tt, im_freelance_assignments fa, im_freelance_packages_trans_tasks fp
                where tt.task_id = fp.trans_task_id
                and fp.freelance_package_id = fa.freelance_package_id
                and tt.task_id = :task_id
                and fa.assignment_status_id not in ([template::util::tcl_to_sql_list $deleted_assignment_status_ids])
                limit 1
                " -default 0]
            
            if {$task_not_removeable_p} {
                return 0
            } else {
                return 1
            }
        } else {
            return 1
        }
    }

    ad_proc -public task_name_exists_p {
        -task_name:required
        {-task_id ""}
        {-project_id ""}
        {-target_language_id ""}
    } {

    } {
        if {$project_id eq "" && $task_id eq ""} {
            ns_log Error "task_name_exists_p: you need to provide at least project_id or task_id"
            return 0
        }

        if {$project_id eq ""} {
            set project_id [db_string project_from_task "select project_id from im_trans_tasks where task_id = :task_id" -default ""]
            if {$project_id eq ""} {
                ns_log Error "task_name_exists_p: task_id does not seem to exist. you need to provide at least project_id or task_id"
            }
        }

        if {$target_language_id eq ""} {
            # Need both project and task in this case
            if {$task_id ne ""} {
                set target_language_id [db_string target_language_from_task "select target_language_id from im_trans_tasks where task_id = :task_id" -default ""]
            }
        }


        if {$task_id ne ""} {
            set task_name_exists_p [db_string task_name_count "
                select 1
                from im_trans_tasks
                where
                    lower(task_name) = lower(:task_name)
                    and project_id = :project_id
                    and target_language_id = :target_language_id
                    and task_id != :task_id                    
            " -default 0]
        } else {
            
            set task_name_exists_p [db_string task_name_count "
                select 1
                from im_trans_tasks
                where
                    lower(task_name) = lower(:task_name)
                    and project_id = :project_id
                    and target_language_id = :target_language_id
            " -default 0]
        }

        ns_log Notice "Webix task exists  $task_name_exists_p $task_id ... $task_name $project_id $target_language_id"

        return $task_name_exists_p
    }

    ad_proc -public trados_tasks_new { 	
         -project_id:required
         -trados_analysis_xml:required
         -target_language_id:required
         -user_id:required
         -only_new_p:required
    } { 	
        Create translation tasks from trados analysis 
    } {     
        set created_task_ids ""
        
        # Setup the elements to search for and how to map them
        keylset element_keyl tag "perfect"
        keylset element_keyl variable "match_perf"
        set single_elements [list $element_keyl]
        
        keylset element_keyl tag "inContextExact"
        keylset element_keyl variable "match_x"
        lappend single_elements $element_keyl
        
        keylset element_keyl tag "exact"
        keylset element_keyl variable "match100"
        lappend single_elements $element_keyl

        keylset element_keyl tag "new"
        keylset element_keyl variable "match0"
        lappend single_elements $element_keyl	

        keylset element_keyl tag "locked"
        keylset element_keyl variable "locked"
        lappend single_elements $element_keyl
        
        keylset element_keyl tag "repeated"
        keylset element_keyl variable "match_rep"
        lappend single_elements $element_keyl
        
        keylset element_keyl tag "crossFileRepeated"
        keylset element_keyl variable "match_cfr"
        lappend single_elements $element_keyl
        
        # ---------------------------------------------------------------
        # Get the XML Analysis
        # ---------------------------------------------------------------

        set xml [new_CkXml]
        
        set success [CkXml_LoadXmlFile $xml $trados_analysis_xml]
        if {[expr $success != 1]} then {
            ns_log Error [CkXml_lastErrorText $xml]
            delete_CkXml $xml
            exit
        }
        CkXml_put_Encoding $xml "utf-8"
        CkXml_put_Utf8 $xml 1

        # ---------------------------------------------------------------
        # Loop through all files which were analysed
        # ---------------------------------------------------------------
        
        set numFiles [CkXml_NumChildrenHavingTag $xml "file"]
        set variables_for_array [list match_x match_rep match100 match95 match85 match75 match50 match0  match_perf match_cfr match_f95 match_f85 match_f75 match_f50 locked]
        set file_names [list]
        
        for {set i 0} {$i <= [expr $numFiles - 1]} {incr i} {
            set success [CkXml_GetNthChildWithTag2 $xml "file" $i]
            set file_name [CkXml_getAttrValue $xml name]
        
            # Skip already processed files
            if {[lsearch $file_names $file_name]<0} {
                lappend file_names $file_name
            } else {
                continue
            }
            
            # Go into the analysis
            CkXml_FindChild2 $xml "analyse"
            
            # Loop through all single elements to get the words
            foreach element_keyl $single_elements {
                CkXml_FindChild2 $xml [keylget element_keyl tag]
                set [keylget element_keyl variable] [CkXml_getAttrValue $xml words]
                CkXml_GetParent2 $xml ; # element
                
                set variable [keylget element_keyl variable]
            }
            
            # Loop through all the fuzzy elements and get the words
            set numFuzzy [CkXml_NumChildrenHavingTag $xml "fuzzy"]
            for {set k 0} {$k <= [expr $numFuzzy - 1]} {incr k} {
                CkXml_GetNthChildWithTag2 $xml "fuzzy" $k
                set min [CkXml_getAttrValue $xml min]
                set match$min [CkXml_getAttrValue $xml words]
                CkXml_GetParent2 $xml ; # fuzzy
            }

            # Loop through all the internalfuzzy elements and get the words
            # they are optional, set the variables just in case
            foreach percent [list 50 75 85 95] {
                set match_f$percent 0
            }
            set numFuzzy [CkXml_NumChildrenHavingTag $xml "internalfuzzy"]
            for {set k 0} {$k <= [expr $numFuzzy - 1]} {incr k} {
                CkXml_GetNthChildWithTag2 $xml "internalfuzzy" $k
                set min [CkXml_getAttrValue $xml min]
                set match_f$min [CkXml_getAttrValue $xml words]
                CkXml_GetParent2 $xml ; # fuzzy
            }
            
            CkXml_GetParent2 $xml ; # analyse
            CkXml_GetParent2 $xml ; # file
            
            # Set the variables in the array for later reuse
            foreach variable $variables_for_array {
                if {[info exists $variable]} {
                    set ${variable}_arr($file_name) [set $variable]
                } else {
                    set ${variable}_arr($file_name) ""
                }
            }
        }


        db_1row project_info "select company_id,project_type_id as task_type_id from im_projects where project_id = :project_id"

        foreach file_name $file_names {
            
            # Take the array variables back :-)
            foreach variable $variables_for_array {
                set $variable [set  ${variable}_arr($file_name)]
            }
            
            # ---------------------------------------------------------------
            # Insert / Update the task
            # ---------------------------------------------------------------

            # Remove the sdlxliff
            set file_name [string trimright $file_name "sdlxliff"]
            set file_name [string range $file_name 0 end-1]
            set task_name $file_name
            set task_type "trans"
            set task_units [im_trans_trados_matrix_calculate [im_company_freelance]  $match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0  $match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]
            
            # Determine the "billable_units" form the project's customer:
            set billable_units [im_trans_trados_matrix_calculate $company_id  $match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0  $match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]
                        
            set task_id [db_string task "select task_id from im_trans_tasks where task_name = :task_name and project_id = :project_id and target_language_id = :target_language_id" -default ""]
            
            if {$task_id eq ""} {
                
                set task_status_id 340
                
                set task_id [webix::trans::task_new \
                    -project_id $project_id \
                    -task_name $task_name \
                    -task_type_id $task_type_id \
                    -user_id $user_id \
                    -task_uom_id 324 \
                    -task_units $task_units \
                    -billable_units $billable_units \
                    -target_language_ids $target_language_id]
                
                set update_match_p 1
            } else {
                if {$only_new_p} {
                    set update_match_p 0
                } else {
                    set update_match_p 1
                    db_dml update_units "update im_trans_tasks set task_units = :task_units, billable_units = :billable_units where task_id = :task_id"
                    im_audit -object_type "im_trans_task" -action after_update -object_id $task_id
                }
            }
        
            if {$update_match_p} {
                db_dml update_task "
                    UPDATE im_trans_tasks SET
                    tm_integration_type_id = [im_trans_tm_integration_type_external],
                    match_x = :match_x,
                    match_rep = :match_rep,
                    match100 = :match100, 
                    match95 = :match95,
                    match85 = :match85,
                    match75 = :match75, 
                    match50 = :match50,
                    match0 = :match0,
                    match_perf = :match_perf,
                    match_cfr = :match_cfr,
                    match_f95 = :match_f95,
                    match_f85 = :match_f85,
                    match_f75 = :match_f75,
                    match_f50 = :match_f50,
                    locked = :locked
                    WHERE 
                    task_id = :task_id
                    "
            }    

            lappend created_task_ids $task_id
        }

        # Generate batches for the provided task_ids
        webix::packages::create -trans_task_ids $created_task_ids
        
        return $created_task_ids
    }

    ad_proc -public best_rate {
        -company_id:required
        -task_type_id:required
        { -subject_area_id ""}
        -target_language_id:required
        -source_language_id:required
        -uom_id:required
        { -currency "" }
        { -task_units "1"}
    } {
        Calculate the baest rate for a company

        @param company_id Company for which we are looking. If not provided, default to internal / freelancer_internal company
        @param task_type_id Project Type for which we want to calculate the price. This is also true for freelancers.
        @param subject_area_id Subject Area for which we try to get the price
        @param source_language_id Source Language which we look for. Relevancy assumes the "main" language to be in front with two characters, so does not match over category hierarchy.
        @param target_language_id Target Language which we look for. Will be matched as per source_language (e.g. de-CH will also look at de for a price, through with reduced matching)
        @param task_units Will return the rate for the amount of units. Assumes 1 unit as default (so you get the rate per unit)
    } {

        set price_id [webix::trans::best_price_id -company_id $company_id \
            -uom_id $uom_id \
            -task_type_id $task_type_id \
            -subject_area_id $subject_area_id \
            -target_language_id $target_language_id \
            -source_language_id $source_language_id \
            -currency $currency]

        if {$price_id eq ""} {
            # No price found
            return ""
        } 

        db_1row price_info "select price, min_price, (price * :task_units) as task_price from im_trans_prices where price_id = :price_id"
        
        if {$task_price < $min_price} {
            return $min_price
        } else {
            return $task_price
        }

    }

    ad_proc -public best_price_id {
        -company_id:required
        -task_type_id:required
        { -subject_area_id "" }
        { -target_language_ids "" }
        { -source_language_id "" }
        { -currency "" }
        -uom_id:required
    } {
        Calculate the best rate for a company - defaults

        Default matching for source/target language:
            "de" <-> "de_DE" = + 1           
            "de_DE" <-> "de_DE" = +3 (same for de-DE)
            "es" <-> "de_DE" = -20


        @param company_id Company for which we are looking. If not provided, default to internal / freelancer_internal company
        @param task_type_id Project Type for which we want to calculate the price. This is also true for freelancers.
        @param subject_area_id Subject Area for which we try to get the price
        @param source_language_id Source Language which we look for. Relevancy assumes the "main" language to be in front with two characters, so does not match over category hierarchy.
        @param target_language_ids Target Languages which we look for. Will be matched as per source_language (e.g. de-CH will also look at de for a price, through with reduced matching)

        @return price_id of the best matched price for that company. Does not default to internal companies. Will return nothing if the relevancy is negative (e.g. wrong languages)
    } {

        if {$currency eq ""} {
            set currency [im_default_currency]
        }

        foreach target_language_id $target_language_ids {
            set price_id [webix::trans::best_price_id_helper -company_id $company_id \
                -uom_id $uom_id \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -currency $currency]
            if {$price_id ne ""} { break }
        }

        if {$price_id eq ""} {
            # Check for internal / freelancer
            set provider_p [db_string provide_p "select 1 from im_companies where company_id = :company_id and company_type_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_company_type_provider]]])" -default 0]
            if {$provider_p} {
                set internal_company_id [im_company_freelance] 
            } else {
                set internal_company_id [im_company_internal]
            }

            set price_id [webix::trans::best_price_id_helper -company_id $internal_company_id \
                -uom_id $uom_id \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -currency $currency]
        }
        return $price_id
    }

    ad_proc -public best_price_id_helper {
        -company_id:required
        -task_type_id:required
        { -subject_area_id "" }
        { -target_language_id "" }
        { -source_language_id "" }
        -currency:required
        -uom_id:required
    } {
        Calculate the best rate for a company

        Default matching for source/target language:
            "de" <-> "de_DE" = + 1           
            "de_DE" <-> "de_DE" = +3 (same for de-DE)
            "es" <-> "de_DE" = -20


        @param company_id Company for which we calculate the price
        @param task_type_id Project Type for which we want to calculate the price. This is also true for freelancers.
        @param subject_area_id Subject Area for which we try to get the price
        @param source_language_id Source Language which we look for. Relevancy assumes the "main" language to be in front with two characters, so does not match over category hierarchy.
        @param target_language_id Target Language which we look for. Will be matched as per source_language (e.g. de-CH will also look at de for a price, through with reduced matching)

        @return price_id of the best matched price for that company. Does not default to internal companies. Will return nothing if the relevancy is negative (e.g. wrong languages)
    } {

        set references_prices_sql "

            select im_trans_prices_calc_relevancy (
                    p.company_id, :company_id,
                    p.task_type_id, :task_type_id,
                    p.subject_area_id, :subject_area_id,
                    p.target_language_id, :target_language_id,
                    p.source_language_id, :source_language_id
                    ) as relevancy,
                p.price_id,
                p.price,
                p.min_price,
                p.company_id,
                p.uom_id,
                p.task_type_id,
                p.target_language_id,
                p.source_language_id,
                p.subject_area_id,
                p.file_type_id,
                p.valid_from,
                p.valid_through,
                p.note as price_note
            from im_trans_prices p
            where
                uom_id= :uom_id
                and currency= :currency
                and company_id = :company_id
            order by
                relevancy desc
            limit 1"

        if {[db_0or1row reference_prices $references_prices_sql]} {
            if {$relevancy >0} {
                return $price_id
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
}

namespace eval webix::trans::project {

    ad_proc -public new { 
        -company_id:required
        -source_language_id:required
        -target_language_ids:required
        -creation_user:required
        { -project_type_id "" }
        { -project_status_id "" }
        { -subject_area_id ""}
        { -final_company_id ""}
        { -customer_contact_id ""}
        { -project_id ""}
        { -project_name ""}
        { -project_nr ""}
        { -processing_time "" }
        { -company_project_nr ""}
        {-project_lead_id ""}
        {-final_company_id ""}
        {-subject_area_id ""}
        {-project_source_id ""}
        {-description ""}

    } {
        Create a new translation project
        
        @param project_status_id Status of the project, defaults to potential
        @param project_type_id Type of the project, defaults to trans + edit
    }  {
        # Auto get the project_nr if missing
        if {$project_nr eq ""} {
            set project_nr [im_next_project_nr -customer_id $company_id]
        }
        
        # Auto set the project_name
        if {$project_name eq ""} {
            set project_name $project_nr
        }


        set dup_sql "
            select	count(*)
            from	im_projects
            where	upper(trim(project_nr)) = upper(trim(:project_nr))"

        set project_id 0
    
        if {[db_string duplicates $dup_sql]} {
            set project_id [db_string duplicate_project_id "select max(project_id)
                from	im_projects
                where	upper(trim(project_nr)) = upper(trim(:project_nr))" -default "0"]
        }

        # Try to create the project
        if {$project_id eq "0"} {
            set project_path [string tolower [string trim $project_nr]]

            # Use sensible defaults
            if {$project_type_id eq ""} {set project_type_id [im_project_type_trans_edit]}
            if {$project_status_id eq ""} {set project_status_id [im_project_status_potential]}

            set creation_ip [ns_conn peeraddr]
            set project_id [db_exec_plsql create_new_project {
                select im_project__new (
                    NULL,         
                    'im_project',
                    now(),
                    :creation_user,
                    :creation_ip,
                    null,
                    :project_name,
                    :project_nr,
                    :project_path,
                    null,
                    :company_id,
                    :project_type_id,
                    :project_status_id
                );

            }]
        }

        if {$project_id eq 0} {
            ns_log Error "Creation of project $project_name , $project_nr for [im_name_from_id $company_id] failed"
        } else {

            # Add the project Manager
            if {$project_lead_id eq ""} {
                set project_lead_id $creation_user
            }

            set role_id [im_biz_object_role_project_manager]
            im_biz_object_add_role $project_lead_id $project_id $role_id
            
            # Project source id
            if {$project_source_id eq ""} {
                set project_source_id [im_project_source_normal]
            }
            db_dml update_project "update im_projects set company_contact_id = :customer_contact_id, source_language_id = :source_language_id, 
                project_lead_id = :project_lead_id, final_company_id = :final_company_id, processing_time =:processing_time, subject_area_id = :subject_area_id, 
                project_source_id =:project_source_id, company_project_nr = :company_project_nr, description =:description
                where project_id = :project_id"
        
            # Save the information about the project target languages
            # in the im_target_languages table
            db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
    
            foreach lang $target_language_ids {
                set sql "insert into im_target_languages values ($project_id, $lang)"
                db_dml insert_im_target_language $sql
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $lang
            }
        
            # Add the source language as a skill
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
            intranet_fs::create_project_folder -project_id $project_id -user_id $creation_user

            callback webix::trans::project::after_create -project_id $project_id -user_id $creation_user
        
            # -----------------------------------------------------------------
            # Flush caches related to the project's information
            
            util_memoize_flush_regexp "im_project_has_type_helper.*"
            util_memoize_flush_regexp "db_list_of_lists company_info.*"
        }
                
        return $project_id
    }
}