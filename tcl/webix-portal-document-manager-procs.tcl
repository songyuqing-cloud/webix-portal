ad_library {
    Rest Procedures for providing the backend server for docmanager by webix.

    https://docs.webix.com/docmanager__backend.html#addcomment

    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc webix_file {} {
        @return value string name of the file/folder
        @return id string item_id of the file/folder
        @return date integer UNIX Time When was the file last updated
        @return size number Filesize of the file in bytes.
        @return type string type of file
    } -
}

# /cognovis-rest/docmanager/files
ad_proc -public cog_rest::get::docmanager::files {
    {-id ""}
    -rest_user_id:required
} {
    Returns a list of files within a folder or a project

    @param id string object fs_folder::read ID of the folder we looking at 

    @return files json_array webix_file Array of files and folders within the folder.

} {
    set files [list]
    
    # look for  the last three days as new
    # has to change later on to support views package
    set n_past_days 3

    db_foreach file "select object_id as id,
            type,
            to_char(last_modified, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as date,
            content_size as size,
            name as value
        from fs_objects
        where parent_id = :id
            and exists (select 1
                    from acs_object_party_privilege_map m
                   where m.object_id = fs_objects.object_id
                     and m.party_id = :rest_user_id
                     and m.privilege = 'read')
            and live_revision is not null
    " {
        lappend files [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::docmanager::folders {
    {-id ""}
    -rest_user_id:required
} {
    Returns a list of files within a folder or a project

    @param id string object fs_folder::read ID of the folder we looking at 

    @return folders json_array webix_file Array of folders within the folder.

} {
    set folders [list]

    db_foreach folder "WITH RECURSIVE folders(id,type,parent_id,date,size,value) AS (
        select f1.object_id as id, f1.type, f1.parent_id, extract(epoch from f1.last_modified) as date,
            f1.content_size as size, f1.name as value
        from fs_objects f1
        where parent_id = :id
            and f1.type = 'folder'
        UNION
        select f2.object_id as id, f2.type, f2.parent_id, extract(epoch from f2.last_modified) as date,
            f2.content_size as size, f2.name as value
        from fs_objects f2, folders f
        where f2.parent_id = f.id
            and f2.type = 'folder'
        )
        SELECT * FROM folders 
            where exists (select 1
                    from acs_object_party_privilege_map m
                   where m.object_id = folders.id
                     and m.party_id = :rest_user_id
                     and m.privilege = 'read')
    " {
        set parent_name [im_name_from_id $parent_id]
        lappend folders [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}