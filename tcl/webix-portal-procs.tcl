
ad_library {
    Helper procedures for webix-portal package

    @author michaldrn@wp.pl

}

namespace eval webix {}

ad_proc -public im_user_main_company_id {
    -user_id
} {

    Return the main company id for given user_id.
    This should return single id or empty string
  
    @return company_id

} {
    # First we check if user is not primary_contact_id for 
    set company_id [db_string company_id_sql "select company_id from im_companies where primary_contact_id =:user_id limit 1" -default ""]
    # If company_id is still null, we keep on searching, this time with acs_rels
    if {$company_id eq ""} {
        set company_id [db_string company_id_sql "select object_id_one as company_id from acs_rels where object_id_two =:user_id and rel_type = 'im_company_employee_rel' limit 1" -default ""]
    }
    return $company_id

}


ad_proc -public im_company_key_account_id {
    -company_id
} {

    Return the key account id for given company_id
    This should return single id or empty string
  
    @return key_account_id

} {
    # First we check if user is not manager_id
    set key_account_id [db_string key_account_id_sql "select manager_id from im_companies where company_id =:company_id" -default ""]
    # If company id is null, we check acs_rels
    if {$company_id eq ""} {
        set key_account_id [db_string key_account_id_sql "select object_id_two as key_account_id from acs_rels where object_id_one =:company and rel_type = 'im_key_account_rel' limit 1" -default ""]
    }
    return $key_account_id

}


ad_proc -public im_category_ids_to_names {
    -category_ids
} {

    Converts list of category ids to list of category names

} {
    set category_names [list]
    foreach category_id $category_ids {
        lappend category_names [im_name_from_id $category_id]
    }

    return $category_names
}


ad_proc -public im_verify_user_token {
    -email
    -token
} {

    Procedure used to verify user token used for password reset

} {
    
    set password_token_matched_p [db_string user_id_from_email "select 1 from cc_users where email =:email and password =:token" -default 0]

    return $password_token_matched_p
    
}


ad_proc -public im_get_column_name_of_category {
    -column_name
} {
    Procedure which returns coressponding column name.
    So in case we have project_status_id it returns project_status_name (and opposite)
} {

    set value_to_return ""
    set obj_ctr 0
    set temp_list [list]

    set splitted_column_name [split $column_name "_"]
    set last_one [lindex $splitted_column_name [expr [llength $splitted_column_name] -1]]
    foreach column_part $splitted_column_name {
        if {[expr [llength $splitted_column_name] -1] > $obj_ctr} {
            lappend temp_list $column_part
        }
        incr obj_ctr
    }

    if {$last_one eq "name"} {
        lappend temp_list "id"
    }
    if {$last_one eq "id"} {
        lappend temp_list "name"
    }

    return [join $temp_list "_"]

}