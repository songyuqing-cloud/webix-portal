ad_library {
    Procedures for working with freelancers
    @author malte.sussdorff@cognovis.de
}
namespace eval cog_rest::json_object {
    ad_proc freelancer_language {} {
        @return language category "Intranet Translation Language" Target language for which this freelancer can work on
        @return type string Type of language (source or target)
    } -
}


ad_proc -public cog_rest::get::freelancer_project_languages {
    -freelancer_ids:required
    -project_id:required
    -rest_user_id:required
} {
    @param freelancer_ids object_array person::read IDs of freelancers we want to check the project language for
    @param project_id object im_project::read ID of the project for which we look up the freelancer languages

    @return project_languages json_array freelancer_language Array of languages a freelancer can work on in this project
} {
    set project_languages [list]
    set target_language_ids [webix::freelancer::project_target_languages -freelancer_ids $freelancer_ids -project_id $project_id] 
    foreach language_id $target_language_ids {
        set type "target"
        lappend project_languages [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Object Skills
#---------------------------------------------------------------

ad_proc -public cog_rest::json_object::skill {
} {
    Skills as they are used for attaching skills (special category) to an object or freelancer

    @return skill category Skill which is required
    @return skill_type category "Intranet Skill Type" Skill type of the required skill
} -

ad_proc -public cog_rest::get::object_skill {
    -object_id:required
    { -skill_type_id "" }
    -rest_user_id:required
} {
    Return a list of required skills for an object

    @param object_id integer Object ID of the object (typically project, RFQ, other) for which we want to see required skills
    @param skill_type_id category "Intranet Skill Type" Skill type we want to filter for

    @return skills json_array skill Array of object Skills
} {
    set skills [list]
    set where_clauses [list]
    lappend where_clauses "object_id = :object_id"
    if {$skill_type_id ne ""} {
        lappend where_clauses "skill_type_id = :skill_type_id"
    }

    db_foreach skill "
        select skill_id, skill_type_id from im_object_freelance_skill_map where [join $where_clauses " and "]
    " {
        lappend skills [cog_rest::json_object]
    }

    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::object_skill {
    -object_id:required
    -skill_ids:required
    -rest_user_id:required
} {
    Return a list of required skills for an object

    @param object_id object *::read ID of the object (typically project, RFQ, other) for which we want to see required skills
    @param skill_ids category_array "*" Skills of the skill type we want to filter for

    @return skills json_array skill Array of object_skill
} {

    set skill_type_ids [list]
    foreach skill_id $skill_ids {
        set skill_type_id [webix::freelancer::get_skill_type_id -skill_id $skill_id]
        if {$skill_type_id ne ""} {
            lappend skill_type_ids $skill_type_id
            im_freelance_add_required_skills -object_id $object_id -skill_type_id $skill_type_id -skill_ids $skill_id
        }
    }

    set skills [list]
    set where_clauses [list]
    lappend where_clauses "object_id = :object_id"
    if {[llength $skill_type_ids]>0} {
        lappend where_clauses "skill_type_id in ([template::util::tcl_to_sql_list $skill_type_ids])"
    }

    db_foreach skill "select skill_id, skill_type_id from im_object_freelance_skill_map 
        where [join $where_clauses " and "]" {
        lappend skills [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::object_skill {
    -object_id:required
    -skill_ids:required
    -rest_user_id:required
} {
    Deletes a skill from an object

    @param object_id object *::read ID of the object (typically project, RFQ, other) from which we want to delete the skill
    @param skill_ids category_array "*" Skills we want to delete

    @return skills json_array skill Array of skills after deletion. Only return those of the skill_type associated with the skill_ids
} {

    set skill_type_ids [list]
    foreach skill_id $skill_ids {
        set skill_type_id [webix::freelancer::get_skill_type_id -skill_id $skill_id]
        if {$skill_type_id ne ""} {
            lappend skill_type_ids $skill_type_id
            		db_dml delete "
		delete from im_object_freelance_skill_map
		where
			object_id = :object_id
			and skill_type_id = :skill_type_id
			and skill_id = :skill_id
		"
        }
    }

    set skills [list]
    set where_clauses [list]
    lappend where_clauses "object_id = :object_id"
    if {[llength $skill_type_ids]>0} {
        lappend where_clauses "skill_type_id in ([template::util::tcl_to_sql_list $skill_type_ids])"
    }

    db_foreach skill "select skill_id, skill_type_id from im_object_freelance_skill_map 
        where [join $where_clauses " and "]" {
        lappend skills [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Freelancer Skills
#---------------------------------------------------------------

ad_proc -public cog_rest::get::freelancer_skill {
    -freelancer_id:required
    { -skill_type_id "" }
    -rest_user_id:required
} {
    Return a list of skills for the freelancer or project.

    @param project_id object im_project::read Project to which we would like to get the skills for
    @param freelancer_id object person::read Freelancer we would like to get the skills for
    @param skill_type_id category "Intranet Skill Type" Skill type we want to filter for

    @return skills json_array skill
} {
    set skills [list]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::freelancer_skill {
    -freelancer_id:required
    -skill_type_id:required
    -skill_ids:required
    -rest_user_id
} {
    Add skills to an object (typically a project or a freelancer)

    @param freelancer_id object person::read Freelancer we would like to record the skills for
    @param skills request_body Skills we would like to add for the freelancer
    @param_skills skill_type_id category "Intranet Skill Type" Skill type we want to add
    @param_skills skill_ids object_array im_category Skill is a category_id, depending on the skill type

    @return skills json_array skill    
} {
    set skills [list]
    return [cog_rest::json_response]
}


namespace eval webix::freelancer {
    ad_proc -public project_target_languages {
        -freelancer_ids:required
        -project_id:required
    } {
        @param freelancer_ids IDs of freelancers we want to check the project language for
        @param project_id ID of the project for which we look up the freelancer languages

        @return  List of language_ids the freelancer can translate into in this project
    } {
        set project_lang_ids [list]
        
        # Get the list of possible target languages in the project
        set language_sql "select  language_id as target_language_id, parent_id
            from    im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
            where   project_id = :project_id"
        
        db_foreach language $language_sql {
            if {[exists_and_not_null parent_id]} {
                set project_lang_ids [concat $project_lang_ids [im_sub_categories $parent_id]]
            } else {
                set project_lang_ids [concat $project_lang_ids [im_sub_categories $target_language_id]]
            }
	    }
	
	    set skill_type_id 2002

        set freelancer_lang_ids [db_list fl_languages "select distinct skill_id from im_freelance_skills 
            where skill_type_id=:skill_type_id 
            and skill_id in ([template::util::tcl_to_sql_list $project_lang_ids])
            and user_id in ([template::util::tcl_to_sql_list $freelancer_ids])"] 

        set freelancer_lang_ids [lsort -unique $freelancer_lang_ids]

        # Append the parent lang_ids
        if {[llength $freelancer_lang_ids]>0} {
            set parent_ids [db_list freelancer_parent_langs "select distinct parent_id from im_category_hierarchy where child_id in ([template::util::tcl_to_sql_list $freelancer_lang_ids])"]
            set freelancer_lang_ids [concat $parent_ids $freelancer_lang_ids]
        }
        
        return [lsort -unique $freelancer_lang_ids]
    }

    ad_proc -public get_skill_type_id {
        -skill_id:required
    } {
        Return the skill_type_id for a given skill

        @param skill_id Skill we look for the type_id
    } {
        set category_type [db_string type "select category_type from im_categories where category_id = :skill_id" -default ""]
        if {$category_type eq ""} {
            return ""
        }

        set skill_type_id [db_string get_type_id "select category_id from im_categories where category_type = 'Intranet Skill Type' and aux_string1 = :category_type" -default ""]
        return $skill_type_id
    }

}
