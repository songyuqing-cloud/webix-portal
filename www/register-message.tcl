ad_page_contract {

    Displays the form for the creation of a new localized message.

    @author Bruno Mattarollo <bruno.mattarollo@ams.greenpeace.org>
    @author Christian Hvid
    @creation-date 15 April 2002
    @cvs-id $Id: localized-message-new.tcl,v 1.4 2010/10/19 20:11:56 po34demo Exp $

} {
    package_key
    message_key
    { message "" }
    {return_url "" }
}

if {$message eq ""} {
    regsub -all {_} $message_key { } message
}

set current_locale [lang::user::locale -user_id [auth::get_user_id]]

set portal_url [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
if {$return_url eq ""} {
    set return_url $portal_url
} else {
    set return_url "${portal_url}/#!$return_url"
}


set page_title "Create New Message"
set context ""

form create message_new

element create message_new package_key_display -label "Package" -datatype text \
    -widget inform -value $package_key

element create message_new message_key -label "Message key" -datatype text -widget inform -html { size 50 }
if {$current_locale ne "en_US"} {
    # Check if we have the main locale registered.
    set key "${package_key}.$message_key"
    if {[lang::message::message_exists_p "en_US" $key]} {
        element create message_new us_message -label "Message in US English" -datatype text -widget inform -html { size 50 }
    } else {
        element create message_new us_message -label "Message in US English" -datatype text -widget textarea -html { rows 6 cols 40 }
    }
}
element create message_new message -label "Message" -datatype text \
    -widget textarea -html { rows 6 cols 40 }

element create message_new package_key -datatype text -widget hidden

element create message_new return_url -datatype text -widget inform -optional

# The two hidden tags that we need to pass on the key and language to the
# processing of the form
element create message_new locale -label "locale" -datatype text -widget hidden

if { [form is_request message_new] } {

    element set_value message_new package_key $package_key
    element set_value message_new locale $current_locale
    element set_value message_new message_key $message_key
    element set_value message_new return_url $return_url
    if {$current_locale ne "en_US"} {
        element set_value message_new us_message [lang::message::lookup "en_US" $key]
    }

} else {

    # We are not getting a request, so it's a post. Get and validate
    # the values

    form get_values message_new

    # We have to check the format of the key submitted by the user,
    # We can't accept whitespaces or tabs, only alphanumerical and "-",
    # "_" or "." characters. The 1st character can't be a "."
    if { [regexp {[^[:alnum:]\_\-\.\?]} $message_key] } {
        # We matched for a forbidden character
        element set_error message_new message_key \
            "Key can only have alphanumeric or \"-\", \"_\", \".\" or \"?\" characters"

    }

    if { [string length $message_key] >= 200 } {

        # Oops. The length of the key is too high.
        element set_error message_new key \
            "Key can only have less than 200 characters"

    }
}

if { [form is_valid message_new] } {

    # We get the values from the form
    form get_values message_new package_key message_key locale message 

    if {$locale ne "en_US" && ![lang::message::message_exists_p "en_US" $key]} {
        form get_values us_message
        lang::message::register "en_US" $package_key $message_key $us_message
    }
    
    # We use the acs-lang registration of a translation. Simple, eh?

    lang::message::register $locale $package_key $message_key $message

    set escaped_locale [ns_urlencode $locale]

    ns_returnredirect $return_url

}

ad_return_template