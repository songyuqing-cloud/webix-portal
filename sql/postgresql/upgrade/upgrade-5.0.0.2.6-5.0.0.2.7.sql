--
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2021-02-28
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.2.6-5.0.0.2.7.sql','');

update im_freelance_packages fp set package_comment = (select assignment_comment from im_freelance_assignments fa where fa.freelance_package_id = fp.freelance_package_id and fa.assignment_status_id not in (4230,4228,4223) limit 1);

-- Add the freelancer biz object role
SELECT im_category_new (1310,'Freelancer','Intranet Biz Object Role');