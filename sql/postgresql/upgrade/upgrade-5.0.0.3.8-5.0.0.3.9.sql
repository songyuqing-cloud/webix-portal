SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.8-5.0.0.3.9.sql','');

update im_menus set enabled_p = 'f' where label = 'webix_freelancers_db';
update im_menus set enabled_p = 'f' where label = 'webix_companies_db';
update im_menus set enabled_p = 'f' where label = 'webix_reports';
update im_menus set enabled_p = 'f' where label = 'webix_invoices';
update im_menus set enabled_p = 'f' where label = 'webix_project_files';
update im_menus set url = 'assignments.overview' where label = 'webix_project_assignments';