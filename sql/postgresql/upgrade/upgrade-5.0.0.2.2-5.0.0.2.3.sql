SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.2.2-5.0.0.2.3.sql','');

create or replace function im_freelance_package__delete (integer) returns integer as '
DECLARE
    p_freelance_package_id   alias for $1;
    v_count integer;
    v_file record;
BEGIN

    -- Check if have an assignment, then we cannot delete the package
    select count(*) into v_count from im_freelance_assignments
        where freelance_package_id = p_freelance_package_id;
    
    IF 0 != v_count THEN
        raise notice ''Assignment still exists for % '', p_freelance_package_id;
    return 0; END IF;

    -- Erase the tasks associated with the package
    delete from im_freelance_packages_trans_tasks where freelance_package_id = p_freelance_package_id;

    -- Erase all files associated with the package
    for v_file in select freelance_package_file_id from im_freelance_package_files
        where freelance_package_id = p_freelance_package_id
    loop
        perform im_freelance_package_file__delete(v_file.freelance_package_file_id);
    end loop;

    -- Erase all the priviledges
    delete from     acs_permissions
        where          object_id = p_freelance_package_id;

    -- Erase the im_trans_packages item associated with the id
    delete from     im_freelance_packages
        where freelance_package_id = p_freelance_package_id;
    
    PERFORM acs_object__delete(p_freelance_package_id);

    return 0;
end;' language 'plpgsql';

create or replace function im_freelance_package__name (integer) returns varchar as '
DECLARE
    v_freelance_package_id    alias for $1;
    v_name        varchar;
BEGIN
    select  freelance_package_name
    into    v_name
    from    im_freelance_packages
    where   freelance_package_id = v_freelance_package_id;

    return v_name;
end;' language 'plpgsql';