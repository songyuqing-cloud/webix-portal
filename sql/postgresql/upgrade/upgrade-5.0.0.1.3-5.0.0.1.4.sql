SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.1.3-5.0.0.1.4.sql','');

-- Exchange name and label for menus of webix package key
update im_menus set menu_gif_large = label where package_name = 'webix-portal';
update im_menus set label = name where package_name = 'webix-portal';
update im_menus set name = menu_gif_large where package_name = 'webix-portal';
update im_menus set menu_gif_large = null where package_name = 'webix-portal';