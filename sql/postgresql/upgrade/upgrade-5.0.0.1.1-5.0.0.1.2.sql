SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.1.1-5.0.0.1.2.sql','');

-- Financial Overview Menu

-- Project Info Menu
select create_or_update_menu (2001024, 'webix-portal', 'webix_project_financial_overview', 'Financial Overview', 'financial-overview.financial-overview', 105, 'webix_projects', '', 'fa fa-pie-chart', 't');
-- admins
select acs_permission__grant_permission(2001024, 459, 'read');
-- po's
select acs_permission__grant_permission(2001024, 467, 'read');
-- customers
select acs_permission__grant_permission(2001024, 461, 'read');