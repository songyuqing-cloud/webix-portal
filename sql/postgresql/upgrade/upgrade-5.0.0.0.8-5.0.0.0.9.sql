SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.0.8-5.0.0.0.9.sql','');

-- Financial Overview Menu

create or replace function inline_0 ()
returns integer as $body$
declare
        v_menu_id                 integer;
begin

    select create_or_update_menu(2001023, 'webix-portal', 'webix_project_financial_documents', 'Financial Documents', 'project-assignments.project-finances', 95, 'webix_projects', '', 'fa fa-laptop', 't') into v_menu_id;
    -- admins
    perform acs_permission__grant_permission(v_menu_id, 459, 'read');
    -- po's
    perform acs_permission__grant_permission(v_menu_id, 467, 'read');
    return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();