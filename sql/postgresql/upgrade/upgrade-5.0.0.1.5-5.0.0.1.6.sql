SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.1.5-5.0.0.1.6.sql','');

-- Cleanup menus.



select im_menu__delete_by_name('webix_my_assignments');


select im_menu__delete_by_name('webix_project_financial_overview');

select im_menu__delete_by_name('invoice_go_to_project');
select im_menu__delete_by_name('invoice_download_pdf');
select im_menu__delete_by_name('invoice_preview_pdf');
select im_menu__delete_by_name('invoice_preview_word');
select im_menu__delete_by_name('invoice_convert_to_invoice');
select im_menu__delete_by_name('invoice_delete_invoice');
select im_menu__delete_by_name('invoice_mail_invoice');
select im_menu__delete_by_name('webix_invoices');
select im_menu__delete_by_name('webix_freelancers_db');
select im_menu__delete_by_name('webix_companies_db');
select im_menu__delete_by_name('webix_reports');
select im_menu__delete_by_name('webix_action_invoice');




select im_menu__delete_by_name('webix_project_assignments');
select im_menu__delete_by_name('webix_project_select_freelancers');
select im_menu__delete_by_name('webix_project_tasks');
select im_menu__delete_by_name('webix_project_files');
select im_menu__delete_by_name('webix_project_messages');
select im_menu__delete_by_name('webix_project_info');
select im_menu__delete_by_name('webix_project_financial_overview');
select im_menu__delete_by_name('webix_project_financial_documents');
select im_menu__delete_by_name('webix_project_ratings');
select im_menu__delete_by_name('webix_projects');


select im_menu__delete_by_name('webix_actions');

select im_menu__delete_by_name('webix_search');
select im_menu__delete_by_name('webix_notifications');
select im_menu__delete_by_name('webix_account_info');
select im_menu__delete_by_name('webix_new_offer');
select im_menu__delete_by_name('webix_my_projects');

select im_menu__delete_by_name('webix_main_menu');

select im_menu__delete_by_label('webix_my_assignments');


select im_menu__delete_by_label('webix_project_financial_overview');

select im_menu__delete_by_label('invoice_go_to_project');
select im_menu__delete_by_label('invoice_download_pdf');
select im_menu__delete_by_label('invoice_preview_pdf');
select im_menu__delete_by_label('invoice_preview_word');
select im_menu__delete_by_label('invoice_convert_to_invoice');
select im_menu__delete_by_label('invoice_delete_invoice');
select im_menu__delete_by_label('invoice_mail_invoice');
select im_menu__delete_by_label('webix_invoices');
select im_menu__delete_by_label('webix_freelancers_db');
select im_menu__delete_by_label('webix_companies_db');
select im_menu__delete_by_label('webix_reports');
select im_menu__delete_by_label('webix_action_invoice');




select im_menu__delete_by_label('webix_project_assignments');
select im_menu__delete_by_label('webix_project_select_freelancers');
select im_menu__delete_by_label('webix_project_tasks');
select im_menu__delete_by_label('webix_project_files');
select im_menu__delete_by_label('webix_project_messages');
select im_menu__delete_by_label('webix_project_info');
select im_menu__delete_by_label('webix_project_financial_overview');
select im_menu__delete_by_label('webix_project_financial_documents');
select im_menu__delete_by_label('webix_project_ratings');
select im_menu__delete_by_label('webix_projects');


select im_menu__delete_by_label('webix_actions');

select im_menu__delete_by_label('webix_search');
select im_menu__delete_by_label('webix_notifications');
select im_menu__delete_by_label('webix_account_info');
select im_menu__delete_by_label('webix_new_offer');
select im_menu__delete_by_label('webix_my_projects');

select im_menu__delete_by_label('webix_main_menu');

-- upgrade-5.0.0.0.5-5.0.0.0.6.sql
SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.0.5-5.0.0.0.6.sql','');


-- Delete a single menu by a name
-- Webix Main Menu
select create_or_update_menu (2001000, 'webix-portal', 'webix_main_menu', 'Webix main menu', '', 0, 'Top Menu', '', '', 't');
-- admins
select acs_permission__grant_permission((select menu_id from im_menus where label='webix_main_menu'), 459, 'read');
-- po's
select acs_permission__grant_permission((select menu_id from im_menus where label='webix_main_menu'), 467, 'read');
-- customers
select acs_permission__grant_permission((select menu_id from im_menus where label='webix_main_menu'), 461, 'read');
-- freelanccers
select acs_permission__grant_permission((select menu_id from im_menus where label='webix_main_menu'), 465, 'read');

-- Search Menu
select create_menu ('webix-portal', 'webix_search', 'Search', 'search.search', 5, 'webix_main_menu', '', 'fas fa-search', 'f', 463);

-- Notifications
select create_menu ('webix-portal', 'webix_notifications', 'Notifications', 'notifications.notifications', 10, 'webix_main_menu', '', 'fas fa-bell', 'f',467);

-- Account Info Menu
select create_menu ('webix-portal', 'webix_account_info', 'Account Info', 'account-info.account-info', 15, 'webix_main_menu', '', 'fas fa-info-circle', 't',467);
    -- customers
    select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_account_info'), 461, 'read');

-- New Project Menu
select create_menu ('webix-portal', 'webix_new_offer', 'New Offer', 'new-offer.new-offer', 20, 'webix_main_menu', '', 'fas fa-plus-square', 't',467);
    -- customers
    select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_new_offer'), 461, 'read');

-- Invoices Menu
select create_menu ('webix-portal', 'webix_invoices', 'Invoices', 'invoices.invoices', 45, 'webix_main_menu', '', 'fas fa-dollar-sign', 't',461);
    select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_invoices'), 465, 'read');

-- Freelancer DB Menu
select create_menu ('webix-portal', 'webix_freelancers_db', 'Freelancers DB', 'freelancers.freelancers', 50, 'webix_main_menu', '', 'fa fa-database', 't',467);


-- Companies DB Menu
select create_menu ('webix-portal', 'webix_companies_db', 'Companies DB', 'companies.companies', 55, 'webix_main_menu', '', 'fa fa-id-card', 't',467);


-- Reports Menu
select create_menu ('webix-portal', 'webix_reports', 'Reports', 'reports.reports', 60, 'webix_main_menu', '', 'fa fa-pie-chart', 't',467);

-- Projects Menu
select create_menu ('webix-portal', 'webix_projects', 'Projects', 'projects.projects', 40, 'webix_main_menu', '', 'fas fa-copy', 't',467);
    select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_projects'), 461, 'read');
    select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_projects'), 465, 'read');

    -- Childs of Projects Menu
    -- Project Info Menu
    select create_menu ('webix-portal', 'webix_project_info', 'Info', 'project-info.project-info', 65, 'webix_projects', '', 'fa fa-info', 't',467);
        select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_project_info'), 461, 'read');

    -- Assignments Menu
    select create_menu ('webix-portal', 'webix_project_assignments', 'Assignments', 'project-assignments.project-assignments', 70, 'webix_projects', '', 'fa fa-laptop', 't',467);

    -- Freelancer Select Menu
    select create_menu ('webix-portal', 'webix_project_select_freelancers', 'Select Freelancers', 'project_select_freelancers.project_select_freelancers', 75, 'webix_projects', '', 'fa fa-users', 't',467);

    -- Tasks Menu
    select create_menu ('webix-portal', 'webix_project_tasks', 'Tasks', 'project-tasks.project-tasks', 80, 'webix_projects', '', 'fa fa-pencil', 't',467);

    -- Files Menu
    select create_menu ('webix-portal', 'webix_project_files', 'Files', 'project_files.project_files', 85, 'webix_projects', '', 'fa fa-download', 't',467);
        select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_project_files'), 461, 'read');
        select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_project_files'), 465, 'read');


    -- Messages Menu
    select create_menu ('webix-portal', 'webix_project_messages', 'Messages', 'project_messages.project_messages', 90, 'webix_projects', '', 'fa fa-envelope', 'f',467);
        select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_project_messages'), 461, 'read');
        select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_project_messages'), 465, 'read');

    select create_menu ('webix-portal', 'webix_project_info', 'Info', 'project-info.project-info', 65, 'webix_projects', '', 'fa fa-info', 't', 467);
    select create_menu('webix-portal', 'webix_project_financial_overview', 'Financial Overview', 'financial-overview.financial-overview', 105, 'webix_projects', '', 'fa fa-pie-chart', 't', 467);

    -- Financial Documents Menu
    select create_menu ('webix-portal', 'webix_project_financial_documents', 'Financial Documents', 'project_financial_documents.project_financial_documents', 95, 'webix_projects', '', 'fa fa-usd', 'f',461);
        select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_project_financial_documents'), 465, 'read');

    -- Ratings Menu
    select create_menu ('webix-portal', 'webix_project_ratings', 'Ratings', 'project_ratings.project_ratings', 100, 'webix_projects', '', 'fa fa-star', 'f',467);


-- My Assignments
select create_menu('webix-portal', 'webix_my_assignments', 'My Assignments', 'my_assignments.my_assignments', 110, 'webix_my_projects', '', 'fa fa-user', 'f',465);

-- Project Info Menu

select create_menu('webix-portal', 'webix_actions', 'Webix Action Buttons', '', 10, 'top', '', '', 't', 463);
select create_menu('webix-portal', 'webix_action_invoice', 'Webix Invoice Buttons', '', 10, 'webix_actions', '', 'fa fa-file-invoice', 't', 463);

select create_menu('webix-portal', 'invoice_go_to_project', 'Go back to the project', '', 1, 'webix_action_invoice', '', 'fa fa-arrow-left', 't', 463);
select create_menu('webix-portal', 'invoice_download_pdf', 'Download PDF of financial document and mark it as send', '', 2, 'webix_action_invoice', '', 'fa fa-download blue-icon', 't', 463);
select create_menu('webix-portal', 'invoice_preview_pdf', 'Preview a version of the PDF', '', 3, 'webix_action_invoice', '', 'fa fa-eye blue-icon', 't', 463);
select create_menu('webix-portal', 'invoice_preview_word', 'Preview a LibreOffice version for editing', '', 4, 'webix_action_invoice', '', 'fa fa-file-word blue-icon', 't', 463);
select create_menu('webix-portal', 'invoice_convert_to_invoice', 'Convert a quote to an invoice', '', 5, 'webix_action_invoice', '[db_string quote_p "select 1 from im_costs where cost_type_id = [im_cost_type_quote] and cost_id = :object_id" -default 0]', 'fa fa-euro-sign blue-icon', 't', 467);
select create_menu('webix-portal', 'invoice_delete_invoice', 'Delete the financial document (CAREFUL!) and all generated PDFs', '', 6, 'webix_action_invoice', '[im_object_permission -object_id $object_id -user_id $user_id -privilege "admin"]', 'fa fa-trash-alt blue-icon', 't', 471);
select create_menu('webix-portal', 'invoice_mail_invoice', 'Mail the financial document to the recipient', '', 7, 'webix_action_invoice', '', 'fa fa-paper-plane blue-icon', 't', 463);
