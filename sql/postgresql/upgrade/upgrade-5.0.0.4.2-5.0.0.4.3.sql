SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.4.2-5.0.0.4.3.sql','');


-- My Assignments menu changes

-- update parent menu
update im_menus set parent_menu_id = (select menu_id from im_menus where label='webix_main_menu') where label = 'webix_my_assignments';
-- grant permissions to employees
select acs_permission__grant_permission((select menu_id from im_menus where label='webix_my_assignments'), 463, 'read');
-- update url
update im_menus set url = 'assignments.my-assignments' where label = 'webix_my_assignments';
-- finally enable item
update im_menus set enabled_p = 't' where label = 'webix_my_assignments';


-- Projects menu changes

-- disable permission for freelancers
select acs_permission__revoke_permission((select menu_id from im_menus where label='webix_projects'), 465, 'read');