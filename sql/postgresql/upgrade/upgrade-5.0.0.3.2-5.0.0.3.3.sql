SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.2-5.0.0.3.3.sql','');

SELECT im_category_new ('4304', 'Open Company', 'Webix Notification Action Type');
SELECT im_category_new ('4305', 'Open Contact', 'Webix Notification Action Type');
SELECT im_category_new ('4306', 'Freelancer Selection', 'Webix Notification Action Type');

update im_categories set aux_string1 = 'Open the corresponding company profile' where category_id = 4304;
update im_categories set aux_string1 = 'Open the corresponding contact profile' where category_id = 4305;
update im_categories set aux_string1 = 'Select a freelancer for the package' where category_id = 4306;

alter table webix_notification_actions add column action_help text;
alter table webix_notification_actions drop constraint webix_notif_action_notif_fk;
alter table webix_notification_actions add constraint webix_notif_action_notif_fk foreign key (notification_id) references webix_notifications on delete cascade;

alter table webix_notification_actions drop constraint webix_notif_action_object_fk;
alter table webix_notification_actions add constraint webix_notif_action_object_fk foreign key (action_object_id) references acs_objects on delete cascade;


alter table webix_notifications drop constraint webix_notif_object_fk;
alter table webix_notifications add constraint webix_notif_context_fk foreign key (context_id) references acs_objects on delete cascade;

alter table webix_notifications drop constraint webix_notif_user_fk;
alter table webix_notifications add constraint webix_notif_user_fk foreign key (recipient_id) references users on delete cascade;

alter table acs_mail_log drop constraint acs_mail_log_owner_id_fk;
alter table acs_mail_log add constraint acs_mail_log_context_id_fk foreign key (context_id) references acs_objects on delete cascade;

alter table acs_mail_log drop constraint acs_mail_log_sender_id_fk;
alter table acs_mail_log add constraint acs_mail_log_sender_id_fk foreign key (sender_id) references parties on delete cascade;

alter table acs_mail_log_attachment_map drop constraint acs_mail_log_file_id_fk;
alter table acs_mail_log_attachment_map add constraint acs_mail_log_file_id_fk foreign key (file_id) references cr_revisions on delete cascade;

alter table acs_mail_log_attachment_map drop constraint acs_mail_log_log_id2_fk;
alter table acs_mail_log_attachment_map add constraint acs_mail_log_id_fk foreign key (log_id) references acs_mail_log on delete cascade;

alter table acs_mail_log_recipient_map drop constraint acs_mail_log_log_id_fk;
alter table acs_mail_log_recipient_map add constraint acs_mail_log_log_id_fk foreign key (log_id) references acs_mail_log on delete cascade;