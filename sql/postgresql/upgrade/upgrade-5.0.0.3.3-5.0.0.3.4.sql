SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.3-5.0.0.3.4.sql','');

SELECT im_category_new ('4307', 'Download Files', 'Webix Notification Action Type');
update im_categories set category = 'Upload Files' where category_id = 4301;
update im_categories set aux_string1 = 'Download files based on this notification' where category_id = 4307;
update im_categories set aux_string1 = 'Select a freelancer for the package' where category_id = 4306;
update im_categories set aux_string1 = 'Upload file based on this notification' where category_id = 4301;

update im_categories set aux_html2 = 'fa fa-check' where category_id = 4300;
update im_categories set aux_html2 = 'fas fa-file-upload' where category_id = 4301;
update im_categories set aux_html2 = 'fas fa-paper-plane' where category_id = 4302;
update im_categories set aux_html2 = 'fas fa-external-link-square-alt' where category_id = 4303;
update im_categories set aux_html2 = 'fas fa-building' where category_id = 4304;
update im_categories set aux_html2 = 'fas fa-user' where category_id = 4305;
update im_categories set aux_html2 = 'fas fa-users' where category_id = 4306;
update im_categories set aux_html2 = 'fas fa-file-download' where category_id = 4307;

update im_categories set aux_html2 = 'fas fa-language' where category_id = 4210;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 4211;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 4212;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 4213;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 4214;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 4218;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 4219;

update im_categories set aux_html2 = '#008000' where category_id = 4222;

update im_categories set aux_html2 = '#FFA500' where category_id = 4241;
update im_categories set aux_html2 = '#FF0000' where category_id = 4242;