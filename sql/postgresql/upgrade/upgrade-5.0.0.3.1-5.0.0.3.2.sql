SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.1-5.0.0.3.2.sql','');

-- Actions
delete from im_categories where category_type = 'Webix Notification Action Type';
SELECT im_category_new ('4300', 'Mark as read', 'Webix Notification Action Type');
SELECT im_category_new ('4301', 'Up/Download Files', 'Webix Notification Action Type');
SELECT im_category_new ('4302', 'Send E-Mail', 'Webix Notification Action Type');
SELECT im_category_new ('4303', 'Open Object URL', 'Webix Notification Action Type');

update im_categories set aux_string1 = 'Mark the current notification as read' where category_id = 4300;
update im_categories set aux_string1 = 'Up / Download file based on this notification' where category_id = 4301;
update im_categories set aux_string1 = 'Send a mail (typically to the user causing this notification to occur)' where category_id = 4302;
update im_categories set aux_string1 = 'Open an object_url' where category_id = 4303;